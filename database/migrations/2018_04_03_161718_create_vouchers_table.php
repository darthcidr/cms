<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fund_id');
            $table->date('voucher_date');
            $table->string('dv_num');
            $table->string('ors_burs_num')->nullable();
            $table->string('payee');
            $table->string('address');
            $table->mediumText('particular');
            $table->string('uacs_code');
            $table->decimal('total',17,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
