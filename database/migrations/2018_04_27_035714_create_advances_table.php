<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('voucher');
            $table->string('check_number',10);
            $table->integer('signatory');
            $table->decimal('true_total',17,2)->nullable();
            $table->decimal('amount_reimbursed',17,2)->nullable();
            $table->mediumText('remarks')->nullable();
            $table->string('receiver_name')->nullable();
            $table->string('receiver_position')->nullable();
            $table->string('status',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advances');
    }
}
