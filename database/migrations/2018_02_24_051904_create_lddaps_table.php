<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLddapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lddaps', function (Blueprint $table) {
            /*
            cmbAddLDDAPFundCluster
            txtAddLDDAPSliiaeNumber
            dtpAddLDDAPDate
            txtAddLDDAPLDDAPADANumber
            nudAddLDDAPTotal
            */
            $table->increments('id');
            $table->string('lddap_ada_number')->unique();
            $table->string('sliiae_number');
            $table->string('voucher');
            $table->string('status',9);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lddaps');
    }
}
