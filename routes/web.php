<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::group(['middleware'=>'admin_guest'], function(){
	Route::get('/admin/login', 'AdminAuth\LoginController@showLoginForm')->name('admin.login');
	Route::post('/admin/login', 'AdminAuth\LoginController@login')->name('admin.login');
});
Route::group(['middleware'=>'admin_auth'], function(){
	//cash unit registration
	Route::get('/accounts/cash-unit/register', function(){
		return redirect('/users/cash-unit');
	})->name('register');
	Route::post('/accounts/cash-unit/register', 'Auth\RegisterController@register')->name('register');
	// Registration Routes...
	Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');

	//auth functions and user management functions
	Route::post('/admin/logout', 'AdminAuth\LoginController@logout')->name('admin.logout');
	Route::get('/admin/logout', 'AdminAuth\LoginController@logout')->name('admin.logout');
	Route::get('/admin/home', 'AdminHomeController@index')->name('admin.home');
	Route::get('/admin/account/settings', 'AdminController@showAccountSettingsForm');
	Route::put('/admin/account/settings/update/{user}', [
		'as'	=>	'admin.account.update',
		'uses' 	=> 	'AdminController@update'
	]);

	//cash unit users management
	Route::get('/users/cash-unit', 'UserController@index')->name('admin.cash-unit-users');
	Route::post('/users/cash-unit', 'UserController@store')->name('admin.create.cash-unit-user');
	Route::get('/users/cash-unit/view/{user}','UserController@show');
	Route::get('/users/cash-unit/list/all', 'UserController@list_all');
	Route::put('/users/update/{user}', [
		'as'	=>	'admin.update.cash-unit-user',
		'uses' 	=> 	'UserController@updateAsAdmin'
	]);
	Route::delete('/users/destroy/{user}', [
		'as'	=>	'admin.delete.cash-unit-user',
		'uses' 	=> 	'UserController@destroy'
	]);

	//database management
	Route::get('/settings/database', 'DatabaseController@index');
	Route::post('/settings/database/backup', 'DatabaseController@backup')->name('admin.settings.database.backup');
	Route::post('/settings/database/restore', 'DatabaseController@restore')->name('admin.settings.database.restore');
});

// Auth::routes();
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
// Password Reset Routes...
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware'=>'auth'], function(){
	Route::get('/account/settings', 'UserController@showAccountSettingsForm');
	Route::put('/account/settings/update/{user}', [
		'as'	=>	'account.update',
		'uses' 	=> 	'UserController@update'
	]);

	Route::get('/home', 'HomeController@index')->name('home');

	//VOUCHERS
	Route::get('/vouchers', 'VouchersController@index');
	Route::post('/vouchers', 'VouchersController@store')->name('vouchers.store');
	Route::get('/vouchers/view/{voucher}','VouchersController@show');
	Route::put('/vouchers/update/{id}', [
		'as'	=>	'vouchers.update',
		'uses'	=>	'VouchersController@update'
	]);
	// Route::put('/vouchers/update/{id}','VouchersController@update')->name('vouchers.update');
	Route::get('/vouchers/all', 'VouchersController@list_all_index');
	Route::get('/vouchers/list/all','VouchersController@list_all');

	//CHECKS
	Route::get('/checks/view/{check}', 'ChecksController@show');
	Route::put('/checks/update/{id}', [
		'as'	=>	'checks.update',
		'uses'	=>	'ChecksController@update'
	]);
	// Route::put('/checks/update/{id}','ChecksController@update')->name('checks.update');
	Route::get('/checks/print/signed/{check_number}', 'ChecksController@print_signed');
	Route::get('/checks/print/unsigned/{check_number}', 'ChecksController@print_unsigned');
	Route::get('/checks/list/all', 'ChecksController@list_all_table');
	Route::get('/checks/all', 'ChecksController@list_all_index');
	Route::get('/checks/stale', 'ChecksController@list_stale_index');
	Route::post('/reports/rci', 'ChecksController@report_checks_issued');
	Route::get('/checks/advice/{check}', 'ChecksController@advice');
	Route::get('/checks/report_num', 'ChecksController@report_num_option');
	Route::get('/checks/list/stale','ChecksController@list_stale');

	//LDDAPS
	Route::get('/lddap/view/{lddap}', 'LDDAPsController@show');
	Route::put('/lddap/update/{lddap}', [
		'as'	=>	'lddap.update',
		'uses'	=>	'LDDAPsController@update'
	]);
	Route::delete('/lddap/destroy/{lddap}', [
		'as'	=>	'lddap.destroy',
		'uses'	=>	'LDDAPsController@destroy'
	]);
	Route::get('/lddap/list/all', 'LDDAPsController@list_all');
	Route::get('/reports/lddap/{lddap}', 'LDDAPsController@print_summary');
	Route::get('/lddap/list/report_num', 'LDDAPsController@report_num_option');
	Route::post('/reports/lddap', 'LDDAPsController@report_LDDAP');
	Route::get('/lddap/all', 'LDDAPsController@list_all_index');
	Route::get('/lddap/list/all', 'LDDAPsController@list_all_table');
	Route::post('/reports/sliiae','LDDAPsController@sliiae');

	//CASH ADVANCES/LIQUIDATION
	Route::get('/advances/all', 'AdvancesController@list_all_index');
	Route::get('/advances/view/{advance}','AdvancesController@show');
	Route::get('/advances/list/all', 'AdvancesController@list_all');
	Route::get('/advances/list/no-liquidation', 'AdvancesController@list_no_liquidation');
	Route::get('/advances/list/existing', 'AdvancesController@list_existing');
	Route::put('/reports/liquidation/{voucher}','AdvancesController@liquidate');
	Route::get('/reports/liquidation/{voucher}','AdvancesController@liquidation');
	Route::post('/reports/liquidation','AdvancesController@liquidation_existing')->name('advances.liquidation.existing');
	Route::put('/advances/update/{advance}', [
		'as'	=>	'advance.update',
		'uses'	=>	'AdvancesController@update'
	]);

	//COLLECTIONS
	Route::get('/collections', 'CollectionsController@index')->name('collections.index');
	Route::post('/collections/create', 'CollectionsController@store')->name('collections.store');
	Route::get('/collections/create/{or_number}/successful', 'CollectionsController@store_successful');
	Route::get('/collections/create/{or_number}/failed', 'CollectionsController@store_failed');
	Route::get('/collections/view/{collection}', 'CollectionsController@show');
	Route::get('/collections/list/all', 'CollectionsController@list_all');
	Route::get('/reports/collections/or/{or_number}','CollectionsController@print_or');
	Route::post('/reports/collections/all','CollectionsController@report_collections');

	//FUNDS
	Route::get('/settings/funds', 'FundsController@index')->name('funds.index');
	Route::post('/settings/funds', 'FundsController@store')->name('funds.store');
	Route::get('/settings/funds/view/{fund}', 'FundsController@show');
	Route::put('/settings/funds/update/{fund}', [
		'as'	=>	'funds.update',
		'uses'	=>	'FundsController@update'
	]);
	Route::delete('/settings/funds/destroy/{fund}', [
		'as'	=>	'funds.destroy',
		'uses'	=>	'FundsController@destroy'
	]);
	Route::get('/funds/list/all', 'FundsController@list_all');
	Route::get('/funds/list/collectibles', 'FundsController@list_collectibles');

	//SIGNATORIES
	Route::get('/settings/signatories', 'SignatoriesController@index');
	Route::post('/settings/signatories', 'SignatoriesController@store')->name('signatories.store');
	Route::get('/settings/signatories/view/{signatory}', 'SignatoriesController@show');
	Route::put('/settings/signatories/update/{signatory}', [
		'as'	=>	'signatories.update',
		'uses'	=>	'SignatoriesController@update'
	]);
	Route::delete('/settings/signatories/destroy/{signatory}', [
		'as'	=>	'signatories.destroy',
		'uses'	=>	'SignatoriesController@destroy'
	]);
	Route::get('/signatories/list/all', 'SignatoriesController@list_all');

	//ACTIVITY LOG
	Route::get('/settings/activities', 'ActivitiesController@index');
	Route::get('/settings/activities/list/all', 'ActivitiesController@list_all');

	Route::get('/test',function(){
		return view('reports.liquidation');
	});
});
