/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100125
 Source Host           : localhost:3306
 Source Schema         : cdrms

 Target Server Type    : MySQL
 Target Server Version : 100125
 File Encoding         : 65001

 Date: 01/05/2018 14:43:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for activity_log
-- ----------------------------
DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE `activity_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of activity_log
-- ----------------------------
INSERT INTO `activity_log` VALUES (1, 'darthcaedus78@gmail.com', 'Fund save successful.', '2018-04-04 02:12:14', '2018-04-04 02:12:14');
INSERT INTO `activity_log` VALUES (2, 'darthcaedus78@gmail.com', 'Check save successful.', '2018-04-04 02:19:14', '2018-04-04 02:19:14');
INSERT INTO `activity_log` VALUES (3, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-04 08:33:54', '2018-04-04 08:33:54');
INSERT INTO `activity_log` VALUES (4, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-04 20:02:15', '2018-04-04 20:02:15');
INSERT INTO `activity_log` VALUES (5, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-04 20:08:43', '2018-04-04 20:08:43');
INSERT INTO `activity_log` VALUES (6, 'darthcaedus78@gmail.com', 'Signatory save successful.', '2018-04-05 00:16:08', '2018-04-05 00:16:08');
INSERT INTO `activity_log` VALUES (7, 'darthcaedus78@gmail.com', 'Signatory save successful.', '2018-04-05 00:16:22', '2018-04-05 00:16:22');
INSERT INTO `activity_log` VALUES (8, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-05 01:05:39', '2018-04-05 01:05:39');
INSERT INTO `activity_log` VALUES (9, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-05 01:40:23', '2018-04-05 01:40:23');
INSERT INTO `activity_log` VALUES (10, 'darthcaedus78@gmail.com', 'Fund save successful.', '2018-04-07 05:37:55', '2018-04-07 05:37:55');
INSERT INTO `activity_log` VALUES (11, 'darthcaedus78@gmail.com', 'Fund save successful.', '2018-04-07 05:38:30', '2018-04-07 05:38:30');
INSERT INTO `activity_log` VALUES (12, 'darthcaedus78@gmail.com', 'Fund save successful.', '2018-04-07 05:39:27', '2018-04-07 05:39:27');
INSERT INTO `activity_log` VALUES (13, 'darthcaedus78@gmail.com', 'Fund save successful.', '2018-04-07 05:39:46', '2018-04-07 05:39:46');
INSERT INTO `activity_log` VALUES (14, 'darthcaedus78@gmail.com', 'Fund save successful.', '2018-04-07 05:40:01', '2018-04-07 05:40:01');
INSERT INTO `activity_log` VALUES (15, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-07 05:42:14', '2018-04-07 05:42:14');
INSERT INTO `activity_log` VALUES (16, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-07 12:21:12', '2018-04-07 12:21:12');
INSERT INTO `activity_log` VALUES (17, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-07 12:22:17', '2018-04-07 12:22:17');
INSERT INTO `activity_log` VALUES (18, 'darthcaedus78@gmail.com', 'Signatory save successful.', '2018-04-07 14:28:34', '2018-04-07 14:28:34');
INSERT INTO `activity_log` VALUES (19, 'darthcaedus78@gmail.com', 'Signatory delete successful.', '2018-04-07 14:28:48', '2018-04-07 14:28:48');
INSERT INTO `activity_log` VALUES (20, 'darthcaedus78@gmail.com', 'Collection save successful.', '2018-04-07 15:29:36', '2018-04-07 15:29:36');
INSERT INTO `activity_log` VALUES (21, 'darthcaedus78@gmail.com', 'Fund edit successful.', '2018-04-08 02:52:31', '2018-04-08 02:52:31');
INSERT INTO `activity_log` VALUES (22, 'darthcaedus78@gmail.com', 'Fund edit successful.', '2018-04-08 02:52:53', '2018-04-08 02:52:53');
INSERT INTO `activity_log` VALUES (23, 'darthcaedus78@gmail.com', 'Fund save successful.', '2018-04-08 02:53:21', '2018-04-08 02:53:21');
INSERT INTO `activity_log` VALUES (24, 'darthcaedus78@gmail.com', 'Collection save successful.', '2018-04-10 05:56:09', '2018-04-10 05:56:09');
INSERT INTO `activity_log` VALUES (25, 'darthcaedus78@gmail.com', 'Collection save successful.', '2018-04-13 07:10:20', '2018-04-13 07:10:20');
INSERT INTO `activity_log` VALUES (26, 'darthcaedus78@gmail.com', 'Collection save successful.', '2018-04-13 09:27:06', '2018-04-13 09:27:06');
INSERT INTO `activity_log` VALUES (27, 'darthcaedus78@gmail.com', 'Collection save successful.', '2018-04-13 09:27:38', '2018-04-13 09:27:38');
INSERT INTO `activity_log` VALUES (28, 'darthcaedus78@gmail.com', 'Collection save successful.', '2018-04-13 09:36:54', '2018-04-13 09:36:54');
INSERT INTO `activity_log` VALUES (29, 'darthcaedus78@gmail.com', 'Collection save successful.', '2018-04-13 09:42:57', '2018-04-13 09:42:57');
INSERT INTO `activity_log` VALUES (30, 'darthcaedus78@gmail.com', 'Collection save successful.', '2018-04-13 09:44:30', '2018-04-13 09:44:30');
INSERT INTO `activity_log` VALUES (31, 'darthcaedus78@gmail.com', 'Fund delete successful.', '2018-04-13 10:14:59', '2018-04-13 10:14:59');
INSERT INTO `activity_log` VALUES (32, 'darthcaedus78@gmail.com', 'Fund save successful.', '2018-04-13 10:16:16', '2018-04-13 10:16:16');
INSERT INTO `activity_log` VALUES (33, 'darthcaedus78@gmail.com', 'Fund edit successful.', '2018-04-13 10:20:00', '2018-04-13 10:20:00');
INSERT INTO `activity_log` VALUES (34, 'darthcaedus78@gmail.com', 'Fund edit successful.', '2018-04-13 10:20:15', '2018-04-13 10:20:15');
INSERT INTO `activity_log` VALUES (35, 'darthcaedus78@gmail.com', 'Signatory edit successful.', '2018-04-13 13:18:04', '2018-04-13 13:18:04');
INSERT INTO `activity_log` VALUES (36, 'darthcaedus78@gmail.com', 'Signatory edit successful.', '2018-04-13 13:44:14', '2018-04-13 13:44:14');
INSERT INTO `activity_log` VALUES (37, 'darthcaedus78@gmail.com', 'Signatory edit successful.', '2018-04-13 13:47:32', '2018-04-13 13:47:32');
INSERT INTO `activity_log` VALUES (38, 'darthcaedus78@gmail.com', 'Signatory edit successful.', '2018-04-13 13:58:42', '2018-04-13 13:58:42');
INSERT INTO `activity_log` VALUES (39, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:44:27', '2018-04-16 07:44:27');
INSERT INTO `activity_log` VALUES (40, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:44:27', '2018-04-16 07:44:27');
INSERT INTO `activity_log` VALUES (41, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:44:27', '2018-04-16 07:44:27');
INSERT INTO `activity_log` VALUES (42, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:44:27', '2018-04-16 07:44:27');
INSERT INTO `activity_log` VALUES (43, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:40', '2018-04-16 07:50:40');
INSERT INTO `activity_log` VALUES (44, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:40', '2018-04-16 07:50:40');
INSERT INTO `activity_log` VALUES (45, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:40', '2018-04-16 07:50:40');
INSERT INTO `activity_log` VALUES (46, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:41', '2018-04-16 07:50:41');
INSERT INTO `activity_log` VALUES (47, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:45', '2018-04-16 07:50:45');
INSERT INTO `activity_log` VALUES (48, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:45', '2018-04-16 07:50:45');
INSERT INTO `activity_log` VALUES (49, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:45', '2018-04-16 07:50:45');
INSERT INTO `activity_log` VALUES (50, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:45', '2018-04-16 07:50:45');
INSERT INTO `activity_log` VALUES (51, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:53', '2018-04-16 07:50:53');
INSERT INTO `activity_log` VALUES (52, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:53', '2018-04-16 07:50:53');
INSERT INTO `activity_log` VALUES (53, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:53', '2018-04-16 07:50:53');
INSERT INTO `activity_log` VALUES (54, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:53', '2018-04-16 07:50:53');
INSERT INTO `activity_log` VALUES (55, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:59', '2018-04-16 07:50:59');
INSERT INTO `activity_log` VALUES (56, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:50:59', '2018-04-16 07:50:59');
INSERT INTO `activity_log` VALUES (57, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:51:00', '2018-04-16 07:51:00');
INSERT INTO `activity_log` VALUES (58, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:51:00', '2018-04-16 07:51:00');
INSERT INTO `activity_log` VALUES (59, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:51:03', '2018-04-16 07:51:03');
INSERT INTO `activity_log` VALUES (60, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:51:06', '2018-04-16 07:51:06');
INSERT INTO `activity_log` VALUES (61, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:51:06', '2018-04-16 07:51:06');
INSERT INTO `activity_log` VALUES (62, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:51:06', '2018-04-16 07:51:06');
INSERT INTO `activity_log` VALUES (63, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:51:06', '2018-04-16 07:51:06');
INSERT INTO `activity_log` VALUES (64, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:53:27', '2018-04-16 07:53:27');
INSERT INTO `activity_log` VALUES (65, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:53:27', '2018-04-16 07:53:27');
INSERT INTO `activity_log` VALUES (66, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:53:27', '2018-04-16 07:53:27');
INSERT INTO `activity_log` VALUES (67, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 07:53:27', '2018-04-16 07:53:27');
INSERT INTO `activity_log` VALUES (68, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 10:49:48', '2018-04-16 10:49:48');
INSERT INTO `activity_log` VALUES (69, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 10:49:48', '2018-04-16 10:49:48');
INSERT INTO `activity_log` VALUES (70, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 10:49:48', '2018-04-16 10:49:48');
INSERT INTO `activity_log` VALUES (71, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 10:49:48', '2018-04-16 10:49:48');
INSERT INTO `activity_log` VALUES (72, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 10:57:06', '2018-04-16 10:57:06');
INSERT INTO `activity_log` VALUES (73, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 10:57:06', '2018-04-16 10:57:06');
INSERT INTO `activity_log` VALUES (74, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 10:57:06', '2018-04-16 10:57:06');
INSERT INTO `activity_log` VALUES (75, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 10:57:06', '2018-04-16 10:57:06');
INSERT INTO `activity_log` VALUES (76, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-16 12:16:57', '2018-04-16 12:16:57');
INSERT INTO `activity_log` VALUES (77, 'darthcaedus78@gmail.com', 'Collection save successful.', '2018-04-16 13:43:48', '2018-04-16 13:43:48');
INSERT INTO `activity_log` VALUES (78, 'darthcaedus78@gmail.com', 'Collection save successful.', '2018-04-16 15:16:50', '2018-04-16 15:16:50');
INSERT INTO `activity_log` VALUES (79, 'darthcaedus78@gmail.com', 'Collection save successful.', '2018-04-16 15:54:10', '2018-04-16 15:54:10');
INSERT INTO `activity_log` VALUES (80, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-21 08:24:46', '2018-04-21 08:24:46');
INSERT INTO `activity_log` VALUES (81, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-21 08:44:52', '2018-04-21 08:44:52');
INSERT INTO `activity_log` VALUES (82, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-22 05:36:23', '2018-04-22 05:36:23');
INSERT INTO `activity_log` VALUES (83, 'darthcaedus78@gmail.com', 'Voucher edit successful.', '2018-04-22 07:37:35', '2018-04-22 07:37:35');
INSERT INTO `activity_log` VALUES (84, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-04-23 03:47:39', '2018-04-23 03:47:39');
INSERT INTO `activity_log` VALUES (85, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-23 04:06:23', '2018-04-23 04:06:23');
INSERT INTO `activity_log` VALUES (86, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-27 04:16:14', '2018-04-27 04:16:14');
INSERT INTO `activity_log` VALUES (87, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-27 04:24:32', '2018-04-27 04:24:32');
INSERT INTO `activity_log` VALUES (88, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-28 05:28:48', '2018-04-28 05:28:48');
INSERT INTO `activity_log` VALUES (89, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-28 06:55:21', '2018-04-28 06:55:21');
INSERT INTO `activity_log` VALUES (90, 'darthcaedus78@gmail.com', 'Collection save successful.', '2018-04-28 07:01:48', '2018-04-28 07:01:48');
INSERT INTO `activity_log` VALUES (91, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-28 07:58:24', '2018-04-28 07:58:24');
INSERT INTO `activity_log` VALUES (92, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-28 08:32:00', '2018-04-28 08:32:00');
INSERT INTO `activity_log` VALUES (93, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-28 10:23:05', '2018-04-28 10:23:05');
INSERT INTO `activity_log` VALUES (94, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-30 03:18:24', '2018-04-30 03:18:24');
INSERT INTO `activity_log` VALUES (95, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-30 04:26:19', '2018-04-30 04:26:19');
INSERT INTO `activity_log` VALUES (96, 'darthcaedus78@gmail.com', 'Signatory edit successful.', '2018-04-30 07:36:30', '2018-04-30 07:36:30');
INSERT INTO `activity_log` VALUES (97, 'darthcaedus78@gmail.com', 'Signatory edit successful.', '2018-04-30 07:36:59', '2018-04-30 07:36:59');
INSERT INTO `activity_log` VALUES (98, 'darthcaedus78@gmail.com', 'Signatory edit successful.', '2018-04-30 07:37:05', '2018-04-30 07:37:05');
INSERT INTO `activity_log` VALUES (99, 'darthcaedus78@gmail.com', 'Voucher save successful.', '2018-04-30 07:39:44', '2018-04-30 07:39:44');
INSERT INTO `activity_log` VALUES (100, 'darthcaedus78@gmail.com', 'Check edit successful.', '2018-05-01 02:34:39', '2018-05-01 02:34:39');
INSERT INTO `activity_log` VALUES (101, 'cashunit@test.com', 'Collection save successful.', '2018-05-01 04:59:57', '2018-05-01 04:59:57');
INSERT INTO `activity_log` VALUES (102, 'darthcaedus78@gmail.com', 'Collection save successful.', '2018-05-01 05:09:39', '2018-05-01 05:09:39');
INSERT INTO `activity_log` VALUES (103, 'darthcaedus78@gmail.com', 'LDDAP edit successful.', '2018-05-01 06:07:25', '2018-05-01 06:07:25');
INSERT INTO `activity_log` VALUES (104, 'darthcaedus78@gmail.com', 'LDDAP edit successful.', '2018-05-01 06:09:13', '2018-05-01 06:09:13');

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `lname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admins_email_unique`(`email`) USING BTREE,
  UNIQUE INDEX `admins_username_unique`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES (1, 'janarnoldc@gmail.com', 'janarnoldc', '$2y$10$Cq4SNarl6jvnv7Vp4u7VQ.I5Oiju1Y324C.XXOzypUXJdQv99DjT6', 'Jacen', 'Organa', 'Solo', NULL, 'ijQdySsAab7dWSdEN26VYBqKN0T0yGRKLAi3eE8SkdW5f6NNex5ehuPwkOoy', '2018-02-13 13:43:03', '2018-04-22 03:10:51');

-- ----------------------------
-- Table structure for advance_particulars
-- ----------------------------
DROP TABLE IF EXISTS `advance_particulars`;
CREATE TABLE `advance_particulars`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `advance` int(11) NOT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(17, 2) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for advances
-- ----------------------------
DROP TABLE IF EXISTS `advances`;
CREATE TABLE `advances`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `voucher` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `check_number` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `signatory` int(11) NOT NULL,
  `true_total` decimal(17, 2) NULL DEFAULT NULL,
  `amount_reimbursed` decimal(17, 2) NULL DEFAULT NULL,
  `remarks` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for checks
-- ----------------------------
DROP TABLE IF EXISTS `checks`;
CREATE TABLE `checks`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `check_number` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(9) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `checks_check_number_unique`(`check_number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of checks
-- ----------------------------
INSERT INTO `checks` VALUES (1, '0000000002', '18-04-002', 'CANCELLED', '2018-04-30 07:39:44', '2018-05-01 02:34:38');

-- ----------------------------
-- Table structure for collections
-- ----------------------------
DROP TABLE IF EXISTS `collections`;
CREATE TABLE `collections`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `payor` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `agency` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fund` int(11) NOT NULL,
  `or_number` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` decimal(17, 2) NOT NULL,
  `collecting_officer` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `collections_or_number_unique`(`or_number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of collections
-- ----------------------------
INSERT INTO `collections` VALUES (1, 'Ariel L. Rodriguez', 'LUELCO', 1, '0000001', 1500.00, 'cashunit', '2018-05-01 04:59:56', '2018-05-01 04:59:56');
INSERT INTO `collections` VALUES (4, 'John Doe', 'Doon', 6, '0000002', 615.00, 'darthcidr', '2018-05-01 05:09:38', '2018-05-01 05:09:38');

-- ----------------------------
-- Table structure for funds
-- ----------------------------
DROP TABLE IF EXISTS `funds`;
CREATE TABLE `funds`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cluster_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `collectible` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of funds
-- ----------------------------
INSERT INTO `funds` VALUES (1, '2020-90285-1', '01101101', 'Current Operating Expenses', 'COE', 'true', '2018-04-07 05:37:55', '2018-04-13 10:20:15');
INSERT INTO `funds` VALUES (2, '2020-90286-9', '01101101', 'Retirement Gratuity and Terminal Leave', 'RGTL', 'false', '2018-04-07 05:38:30', '2018-04-07 05:38:30');
INSERT INTO `funds` VALUES (3, '0202-0241-06', '06', 'National Educators Academy of the Philippines', 'NEAP', 'false', '2018-04-07 05:39:27', '2018-04-07 05:39:27');
INSERT INTO `funds` VALUES (4, '0202-0246-88', '07', 'Provident Fund Loans', 'PROV', 'false', '2018-04-07 05:39:46', '2018-04-07 05:39:46');
INSERT INTO `funds` VALUES (5, '0202-0256-41', '01', 'Remittances and Other Funds', 'ROF', 'false', '2018-04-07 05:40:01', '2018-04-08 02:52:53');
INSERT INTO `funds` VALUES (6, '3402-2485-04', '99', 'General Funds', 'GF', 'true', '2018-04-13 10:16:16', '2018-04-13 10:16:16');

-- ----------------------------
-- Table structure for lddaps
-- ----------------------------
DROP TABLE IF EXISTS `lddaps`;
CREATE TABLE `lddaps`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lddap_ada_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sliiae_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(9) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `lddaps_lddap_ada_number_unique`(`lddap_ada_number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lddaps
-- ----------------------------
INSERT INTO `lddaps` VALUES (1, '0 1101101-01-051-2018', '051', '18-04-475', 'ACTIVE', '2018-04-30 04:26:19', '2018-05-01 06:09:13');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2018_02_03_051016_create_funds_table', 1);
INSERT INTO `migrations` VALUES (4, '2018_02_03_070638_create_signatories_table', 1);
INSERT INTO `migrations` VALUES (5, '2018_02_03_102003_create_checks_table', 1);
INSERT INTO `migrations` VALUES (6, '2018_02_13_134346_create_admins_table', 1);
INSERT INTO `migrations` VALUES (8, '2018_02_26_123626_create_activity_log_table', 1);
INSERT INTO `migrations` VALUES (9, '2018_02_28_054644_create_collections_table', 1);
INSERT INTO `migrations` VALUES (10, '2018_02_28_080858_create_particulars_table', 1);
INSERT INTO `migrations` VALUES (11, '2018_04_03_161718_create_vouchers_table', 1);
INSERT INTO `migrations` VALUES (13, '2018_02_24_051904_create_lddaps_table', 2);
INSERT INTO `migrations` VALUES (14, '2018_04_07_151705_add_payor_agency_fund_to_collections', 3);
INSERT INTO `migrations` VALUES (16, '2018_04_13_095608_add_collectible_to_funds_table', 4);
INSERT INTO `migrations` VALUES (17, '2018_04_22_025259_add_username_to_users', 5);
INSERT INTO `migrations` VALUES (18, '2018_04_22_025553_add_username_to_admins', 5);
INSERT INTO `migrations` VALUES (19, '2018_04_27_035714_create_advances_table', 6);
INSERT INTO `migrations` VALUES (20, '2018_04_27_190152_add_true_total_and_amount_reimbursed_to_advances_table', 7);
INSERT INTO `migrations` VALUES (21, '2018_04_28_070638_add_remarks_to_advances_table', 8);
INSERT INTO `migrations` VALUES (22, '2018_04_28_072221_create_advance_particulars_table', 9);

-- ----------------------------
-- Table structure for particulars
-- ----------------------------
DROP TABLE IF EXISTS `particulars`;
CREATE TABLE `particulars`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `collection` int(11) NOT NULL,
  `desc` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(17, 2) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of particulars
-- ----------------------------
INSERT INTO `particulars` VALUES (1, 1, 'Electric Bill', 1500.00, '2018-05-01 04:59:57', '2018-05-01 04:59:57');
INSERT INTO `particulars` VALUES (2, 2, 'qwe', 123.00, '2018-05-01 05:09:38', '2018-05-01 05:09:38');
INSERT INTO `particulars` VALUES (3, 2, 'asd', 123.00, '2018-05-01 05:09:39', '2018-05-01 05:09:39');
INSERT INTO `particulars` VALUES (4, 2, 'zxc', 123.00, '2018-05-01 05:09:39', '2018-05-01 05:09:39');
INSERT INTO `particulars` VALUES (5, 2, 'fgh', 123.00, '2018-05-01 05:09:39', '2018-05-01 05:09:39');
INSERT INTO `particulars` VALUES (6, 2, 'rty', 123.00, '2018-05-01 05:09:39', '2018-05-01 05:09:39');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for signatories
-- ----------------------------
DROP TABLE IF EXISTS `signatories`;
CREATE TABLE `signatories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of signatories
-- ----------------------------
INSERT INTO `signatories` VALUES (1, 'Cristeta', 'M.', 'Oineza', 'Cash Unit', '2018-04-05 00:16:08', '2018-04-30 07:36:59');
INSERT INTO `signatories` VALUES (2, 'Alma Ruby', 'C.', 'Torio', 'Regional Director', '2018-04-05 00:16:22', '2018-04-05 00:16:22');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `lname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'darthcaedus78@gmail.com', 'darthcidr', '$2y$10$Cq4SNarl6jvnv7Vp4u7VQ.I5Oiju1Y324C.XXOzypUXJdQv99DjT6', 'Jacen', 'Organa', 'Solo', NULL, 'K0mfF0BbC1bxIcQozMTPf7E8rgDigL28zs7iMzVYYxxs8XeR4i736TNRPR8c', '2018-02-13 13:43:03', '2018-04-22 03:10:51');
INSERT INTO `users` VALUES (2, 'cashunit@test.com', 'cashunit', '$2y$10$TRkuV2tZoZacaPBjz8C34OvFelVaR/OPQydEnFVx/a9ftRWdKXSeK', 'Cash', 'Unit', 'User', NULL, 'xAbpB8SW3HNTBROA63x4BLRKdt9np3TNS31vkKKFrTNNS1clVvhRTPDujiAV', '2018-05-01 04:45:58', '2018-05-01 04:45:58');

-- ----------------------------
-- Table structure for vouchers
-- ----------------------------
DROP TABLE IF EXISTS `vouchers`;
CREATE TABLE `vouchers`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fund_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher_date` date NOT NULL,
  `dv_num` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ors_burs_num` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `payee` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `particular` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `uacs_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` decimal(17, 2) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of vouchers
-- ----------------------------
INSERT INTO `vouchers` VALUES (1, '5', '2018-04-30', '18-04-475', '18-04-036', 'CRISTETA M. OINEZA', 'DepEd Regional Office No. 1, San Fernando City, La Union', 'To payment of fifteen (15) days monetization computed as follows:\r\n\r\nPhp.38,085.00 X 15 X .0481927 = P27531.28', '5010403000', 27531.28, '2018-04-30 04:26:18', '2018-04-30 04:26:18');
INSERT INTO `vouchers` VALUES (2, '1', '2018-04-30', '18-04-002', '18-04-002', 'John Doe', 'Banda dito', 'Para doon', '0000000002', 20000.00, '2018-04-30 07:39:43', '2018-04-30 07:39:43');

SET FOREIGN_KEY_CHECKS = 1;
