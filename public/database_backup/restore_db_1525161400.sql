-- MySQL dump 10.16  Distrib 10.1.25-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: cdrms
-- ------------------------------------------------------
-- Server version	10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (1,'darthcaedus78@gmail.com','Fund save successful.','2018-04-03 18:12:14','2018-04-03 18:12:14'),(2,'darthcaedus78@gmail.com','Check save successful.','2018-04-03 18:19:14','2018-04-03 18:19:14'),(3,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-04 00:33:54','2018-04-04 00:33:54'),(4,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-04 12:02:15','2018-04-04 12:02:15'),(5,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-04 12:08:43','2018-04-04 12:08:43'),(6,'darthcaedus78@gmail.com','Signatory save successful.','2018-04-04 16:16:08','2018-04-04 16:16:08'),(7,'darthcaedus78@gmail.com','Signatory save successful.','2018-04-04 16:16:22','2018-04-04 16:16:22'),(8,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-04 17:05:39','2018-04-04 17:05:39'),(9,'darthcaedus78@gmail.com','Check edit successful.','2018-04-04 17:40:23','2018-04-04 17:40:23'),(10,'darthcaedus78@gmail.com','Fund save successful.','2018-04-06 21:37:55','2018-04-06 21:37:55'),(11,'darthcaedus78@gmail.com','Fund save successful.','2018-04-06 21:38:30','2018-04-06 21:38:30'),(12,'darthcaedus78@gmail.com','Fund save successful.','2018-04-06 21:39:27','2018-04-06 21:39:27'),(13,'darthcaedus78@gmail.com','Fund save successful.','2018-04-06 21:39:46','2018-04-06 21:39:46'),(14,'darthcaedus78@gmail.com','Fund save successful.','2018-04-06 21:40:01','2018-04-06 21:40:01'),(15,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-06 21:42:14','2018-04-06 21:42:14'),(16,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-07 04:21:12','2018-04-07 04:21:12'),(17,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-07 04:22:17','2018-04-07 04:22:17'),(18,'darthcaedus78@gmail.com','Signatory save successful.','2018-04-07 06:28:34','2018-04-07 06:28:34'),(19,'darthcaedus78@gmail.com','Signatory delete successful.','2018-04-07 06:28:48','2018-04-07 06:28:48'),(20,'darthcaedus78@gmail.com','Collection save successful.','2018-04-07 07:29:36','2018-04-07 07:29:36'),(21,'darthcaedus78@gmail.com','Fund edit successful.','2018-04-07 18:52:31','2018-04-07 18:52:31'),(22,'darthcaedus78@gmail.com','Fund edit successful.','2018-04-07 18:52:53','2018-04-07 18:52:53'),(23,'darthcaedus78@gmail.com','Fund save successful.','2018-04-07 18:53:21','2018-04-07 18:53:21'),(24,'darthcaedus78@gmail.com','Collection save successful.','2018-04-09 21:56:09','2018-04-09 21:56:09'),(25,'darthcaedus78@gmail.com','Collection save successful.','2018-04-12 23:10:20','2018-04-12 23:10:20'),(26,'darthcaedus78@gmail.com','Collection save successful.','2018-04-13 01:27:06','2018-04-13 01:27:06'),(27,'darthcaedus78@gmail.com','Collection save successful.','2018-04-13 01:27:38','2018-04-13 01:27:38'),(28,'darthcaedus78@gmail.com','Collection save successful.','2018-04-13 01:36:54','2018-04-13 01:36:54'),(29,'darthcaedus78@gmail.com','Collection save successful.','2018-04-13 01:42:57','2018-04-13 01:42:57'),(30,'darthcaedus78@gmail.com','Collection save successful.','2018-04-13 01:44:30','2018-04-13 01:44:30'),(31,'darthcaedus78@gmail.com','Fund delete successful.','2018-04-13 02:14:59','2018-04-13 02:14:59'),(32,'darthcaedus78@gmail.com','Fund save successful.','2018-04-13 02:16:16','2018-04-13 02:16:16'),(33,'darthcaedus78@gmail.com','Fund edit successful.','2018-04-13 02:20:00','2018-04-13 02:20:00'),(34,'darthcaedus78@gmail.com','Fund edit successful.','2018-04-13 02:20:15','2018-04-13 02:20:15'),(35,'darthcaedus78@gmail.com','Signatory edit successful.','2018-04-13 05:18:04','2018-04-13 05:18:04'),(36,'darthcaedus78@gmail.com','Signatory edit successful.','2018-04-13 05:44:14','2018-04-13 05:44:14'),(37,'darthcaedus78@gmail.com','Signatory edit successful.','2018-04-13 05:47:32','2018-04-13 05:47:32'),(38,'darthcaedus78@gmail.com','Signatory edit successful.','2018-04-13 05:58:42','2018-04-13 05:58:42'),(39,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:44:27','2018-04-15 23:44:27'),(40,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:44:27','2018-04-15 23:44:27'),(41,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:44:27','2018-04-15 23:44:27'),(42,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:44:27','2018-04-15 23:44:27'),(43,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:40','2018-04-15 23:50:40'),(44,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:40','2018-04-15 23:50:40'),(45,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:40','2018-04-15 23:50:40'),(46,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:41','2018-04-15 23:50:41'),(47,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:45','2018-04-15 23:50:45'),(48,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:45','2018-04-15 23:50:45'),(49,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:45','2018-04-15 23:50:45'),(50,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:45','2018-04-15 23:50:45'),(51,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:53','2018-04-15 23:50:53'),(52,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:53','2018-04-15 23:50:53'),(53,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:53','2018-04-15 23:50:53'),(54,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:53','2018-04-15 23:50:53'),(55,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:59','2018-04-15 23:50:59'),(56,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:50:59','2018-04-15 23:50:59'),(57,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:51:00','2018-04-15 23:51:00'),(58,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:51:00','2018-04-15 23:51:00'),(59,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:51:03','2018-04-15 23:51:03'),(60,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:51:06','2018-04-15 23:51:06'),(61,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:51:06','2018-04-15 23:51:06'),(62,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:51:06','2018-04-15 23:51:06'),(63,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:51:06','2018-04-15 23:51:06'),(64,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:53:27','2018-04-15 23:53:27'),(65,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:53:27','2018-04-15 23:53:27'),(66,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:53:27','2018-04-15 23:53:27'),(67,'darthcaedus78@gmail.com','Check edit successful.','2018-04-15 23:53:27','2018-04-15 23:53:27'),(68,'darthcaedus78@gmail.com','Check edit successful.','2018-04-16 02:49:48','2018-04-16 02:49:48'),(69,'darthcaedus78@gmail.com','Check edit successful.','2018-04-16 02:49:48','2018-04-16 02:49:48'),(70,'darthcaedus78@gmail.com','Check edit successful.','2018-04-16 02:49:48','2018-04-16 02:49:48'),(71,'darthcaedus78@gmail.com','Check edit successful.','2018-04-16 02:49:48','2018-04-16 02:49:48'),(72,'darthcaedus78@gmail.com','Check edit successful.','2018-04-16 02:57:06','2018-04-16 02:57:06'),(73,'darthcaedus78@gmail.com','Check edit successful.','2018-04-16 02:57:06','2018-04-16 02:57:06'),(74,'darthcaedus78@gmail.com','Check edit successful.','2018-04-16 02:57:06','2018-04-16 02:57:06'),(75,'darthcaedus78@gmail.com','Check edit successful.','2018-04-16 02:57:06','2018-04-16 02:57:06'),(76,'darthcaedus78@gmail.com','Check edit successful.','2018-04-16 04:16:57','2018-04-16 04:16:57'),(77,'darthcaedus78@gmail.com','Collection save successful.','2018-04-16 05:43:48','2018-04-16 05:43:48'),(78,'darthcaedus78@gmail.com','Collection save successful.','2018-04-16 07:16:50','2018-04-16 07:16:50'),(79,'darthcaedus78@gmail.com','Collection save successful.','2018-04-16 07:54:10','2018-04-16 07:54:10'),(80,'darthcaedus78@gmail.com','Check edit successful.','2018-04-21 00:24:46','2018-04-21 00:24:46'),(81,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-21 00:44:52','2018-04-21 00:44:52'),(82,'darthcaedus78@gmail.com','Check edit successful.','2018-04-21 21:36:23','2018-04-21 21:36:23'),(83,'darthcaedus78@gmail.com','Voucher edit successful.','2018-04-21 23:37:35','2018-04-21 23:37:35'),(84,'darthcaedus78@gmail.com','Check edit successful.','2018-04-22 19:47:39','2018-04-22 19:47:39'),(85,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-22 20:06:23','2018-04-22 20:06:23'),(86,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-26 20:16:14','2018-04-26 20:16:14'),(87,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-26 20:24:32','2018-04-26 20:24:32'),(88,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-27 21:28:48','2018-04-27 21:28:48'),(89,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-27 22:55:21','2018-04-27 22:55:21'),(90,'darthcaedus78@gmail.com','Collection save successful.','2018-04-27 23:01:48','2018-04-27 23:01:48'),(91,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-27 23:58:24','2018-04-27 23:58:24'),(92,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-28 00:32:00','2018-04-28 00:32:00'),(93,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-28 02:23:05','2018-04-28 02:23:05'),(94,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-29 19:18:24','2018-04-29 19:18:24'),(95,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-29 20:26:19','2018-04-29 20:26:19'),(96,'darthcaedus78@gmail.com','Signatory edit successful.','2018-04-29 23:36:30','2018-04-29 23:36:30'),(97,'darthcaedus78@gmail.com','Signatory edit successful.','2018-04-29 23:36:59','2018-04-29 23:36:59'),(98,'darthcaedus78@gmail.com','Signatory edit successful.','2018-04-29 23:37:05','2018-04-29 23:37:05'),(99,'darthcaedus78@gmail.com','Voucher save successful.','2018-04-29 23:39:44','2018-04-29 23:39:44'),(100,'darthcaedus78@gmail.com','Check edit successful.','2018-04-30 18:34:39','2018-04-30 18:34:39'),(101,'cashunit@test.com','Collection save successful.','2018-04-30 20:59:57','2018-04-30 20:59:57'),(102,'darthcaedus78@gmail.com','Collection save successful.','2018-04-30 21:09:39','2018-04-30 21:09:39'),(103,'darthcaedus78@gmail.com','LDDAP edit successful.','2018-04-30 22:07:25','2018-04-30 22:07:25'),(104,'darthcaedus78@gmail.com','LDDAP edit successful.','2018-04-30 22:09:13','2018-04-30 22:09:13');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`),
  UNIQUE KEY `admins_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'janarnoldc@gmail.com','janarnoldc','$2y$10$Cq4SNarl6jvnv7Vp4u7VQ.I5Oiju1Y324C.XXOzypUXJdQv99DjT6','Jacen','Organa','Solo',NULL,'ijQdySsAab7dWSdEN26VYBqKN0T0yGRKLAi3eE8SkdW5f6NNex5ehuPwkOoy','2018-02-13 05:43:03','2018-04-21 19:10:51');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `advance_particulars`
--

DROP TABLE IF EXISTS `advance_particulars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advance_particulars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `advance` int(11) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(17,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advance_particulars`
--

LOCK TABLES `advance_particulars` WRITE;
/*!40000 ALTER TABLE `advance_particulars` DISABLE KEYS */;
/*!40000 ALTER TABLE `advance_particulars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `advances`
--

DROP TABLE IF EXISTS `advances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `voucher` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `check_number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `signatory` int(11) NOT NULL,
  `true_total` decimal(17,2) DEFAULT NULL,
  `amount_reimbursed` decimal(17,2) DEFAULT NULL,
  `remarks` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advances`
--

LOCK TABLES `advances` WRITE;
/*!40000 ALTER TABLE `advances` DISABLE KEYS */;
/*!40000 ALTER TABLE `advances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checks`
--

DROP TABLE IF EXISTS `checks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `check_number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `checks_check_number_unique` (`check_number`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checks`
--

LOCK TABLES `checks` WRITE;
/*!40000 ALTER TABLE `checks` DISABLE KEYS */;
INSERT INTO `checks` VALUES (1,'0000000002','18-04-002','CANCELLED','2018-04-29 23:39:44','2018-04-30 18:34:38');
/*!40000 ALTER TABLE `checks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collections`
--

DROP TABLE IF EXISTS `collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fund` int(11) NOT NULL,
  `or_number` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` decimal(17,2) NOT NULL,
  `collecting_officer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `collections_or_number_unique` (`or_number`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collections`
--

LOCK TABLES `collections` WRITE;
/*!40000 ALTER TABLE `collections` DISABLE KEYS */;
INSERT INTO `collections` VALUES (1,'Ariel L. Rodriguez','LUELCO',1,'0000001',1500.00,'cashunit','2018-04-30 20:59:56','2018-04-30 20:59:56'),(4,'John Doe','Doon',6,'0000002',615.00,'darthcidr','2018-04-30 21:09:38','2018-04-30 21:09:38');
/*!40000 ALTER TABLE `collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funds`
--

DROP TABLE IF EXISTS `funds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cluster_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collectible` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funds`
--

LOCK TABLES `funds` WRITE;
/*!40000 ALTER TABLE `funds` DISABLE KEYS */;
INSERT INTO `funds` VALUES (1,'2020-90285-1','01101101','Current Operating Expenses','COE','true','2018-04-06 21:37:55','2018-04-13 02:20:15'),(2,'2020-90286-9','01101101','Retirement Gratuity and Terminal Leave','RGTL','false','2018-04-06 21:38:30','2018-04-06 21:38:30'),(3,'0202-0241-06','06','National Educators Academy of the Philippines','NEAP','false','2018-04-06 21:39:27','2018-04-06 21:39:27'),(4,'0202-0246-88','07','Provident Fund Loans','PROV','false','2018-04-06 21:39:46','2018-04-06 21:39:46'),(5,'0202-0256-41','01','Remittances and Other Funds','ROF','false','2018-04-06 21:40:01','2018-04-07 18:52:53'),(6,'3402-2485-04','99','General Funds','GF','true','2018-04-13 02:16:16','2018-04-13 02:16:16');
/*!40000 ALTER TABLE `funds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lddaps`
--

DROP TABLE IF EXISTS `lddaps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lddaps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lddap_ada_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sliiae_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lddaps_lddap_ada_number_unique` (`lddap_ada_number`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lddaps`
--

LOCK TABLES `lddaps` WRITE;
/*!40000 ALTER TABLE `lddaps` DISABLE KEYS */;
INSERT INTO `lddaps` VALUES (1,'0 1101101-01-051-2018','051','18-04-475','ACTIVE','2018-04-29 20:26:19','2018-04-30 22:09:13');
/*!40000 ALTER TABLE `lddaps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_02_03_051016_create_funds_table',1),(4,'2018_02_03_070638_create_signatories_table',1),(5,'2018_02_03_102003_create_checks_table',1),(6,'2018_02_13_134346_create_admins_table',1),(8,'2018_02_26_123626_create_activity_log_table',1),(9,'2018_02_28_054644_create_collections_table',1),(10,'2018_02_28_080858_create_particulars_table',1),(11,'2018_04_03_161718_create_vouchers_table',1),(13,'2018_02_24_051904_create_lddaps_table',2),(14,'2018_04_07_151705_add_payor_agency_fund_to_collections',3),(16,'2018_04_13_095608_add_collectible_to_funds_table',4),(17,'2018_04_22_025259_add_username_to_users',5),(18,'2018_04_22_025553_add_username_to_admins',5),(19,'2018_04_27_035714_create_advances_table',6),(20,'2018_04_27_190152_add_true_total_and_amount_reimbursed_to_advances_table',7),(21,'2018_04_28_070638_add_remarks_to_advances_table',8),(22,'2018_04_28_072221_create_advance_particulars_table',9);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `particulars`
--

DROP TABLE IF EXISTS `particulars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `particulars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `collection` int(11) NOT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(17,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `particulars`
--

LOCK TABLES `particulars` WRITE;
/*!40000 ALTER TABLE `particulars` DISABLE KEYS */;
INSERT INTO `particulars` VALUES (1,1,'Electric Bill',1500.00,'2018-04-30 20:59:57','2018-04-30 20:59:57'),(2,2,'qwe',123.00,'2018-04-30 21:09:38','2018-04-30 21:09:38'),(3,2,'asd',123.00,'2018-04-30 21:09:39','2018-04-30 21:09:39'),(4,2,'zxc',123.00,'2018-04-30 21:09:39','2018-04-30 21:09:39'),(5,2,'fgh',123.00,'2018-04-30 21:09:39','2018-04-30 21:09:39'),(6,2,'rty',123.00,'2018-04-30 21:09:39','2018-04-30 21:09:39');
/*!40000 ALTER TABLE `particulars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signatories`
--

DROP TABLE IF EXISTS `signatories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signatories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signatories`
--

LOCK TABLES `signatories` WRITE;
/*!40000 ALTER TABLE `signatories` DISABLE KEYS */;
INSERT INTO `signatories` VALUES (1,'Cristeta','M.','Oineza','Cash Unit','2018-04-04 16:16:08','2018-04-29 23:36:59'),(2,'Alma Ruby','C.','Torio','Regional Director','2018-04-04 16:16:22','2018-04-04 16:16:22');
/*!40000 ALTER TABLE `signatories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'darthcaedus78@gmail.com','darthcidr','$2y$10$Cq4SNarl6jvnv7Vp4u7VQ.I5Oiju1Y324C.XXOzypUXJdQv99DjT6','Jacen','Organa','Solo',NULL,'K0mfF0BbC1bxIcQozMTPf7E8rgDigL28zs7iMzVYYxxs8XeR4i736TNRPR8c','2018-02-13 05:43:03','2018-04-21 19:10:51'),(2,'cashunit@test.com','cashunit','$2y$10$TRkuV2tZoZacaPBjz8C34OvFelVaR/OPQydEnFVx/a9ftRWdKXSeK','Cash','Unit','User',NULL,'xAbpB8SW3HNTBROA63x4BLRKdt9np3TNS31vkKKFrTNNS1clVvhRTPDujiAV','2018-04-30 20:45:58','2018-04-30 20:45:58');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vouchers`
--

DROP TABLE IF EXISTS `vouchers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vouchers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fund_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher_date` date NOT NULL,
  `dv_num` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ors_burs_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `particular` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `uacs_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` decimal(17,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vouchers`
--

LOCK TABLES `vouchers` WRITE;
/*!40000 ALTER TABLE `vouchers` DISABLE KEYS */;
INSERT INTO `vouchers` VALUES (1,'5','2018-04-30','18-04-475','18-04-036','CRISTETA M. OINEZA','DepEd Regional Office No. 1, San Fernando City, La Union','To payment of fifteen (15) days monetization computed as follows:\r\n\r\nPhp.38,085.00 X 15 X .0481927 = P27531.28','5010403000',27531.28,'2018-04-29 20:26:18','2018-04-29 20:26:18'),(2,'1','2018-04-30','18-04-002','18-04-002','John Doe','Banda dito','Para doon','0000000002',20000.00,'2018-04-29 23:39:43','2018-04-29 23:39:43');
/*!40000 ALTER TABLE `vouchers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-01 14:36:55
