-- MySQL dump 10.16  Distrib 10.1.25-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: cdrms
-- ------------------------------------------------------
-- Server version	10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (1,'darthcaedus78@gmail.com','Signatory save successful.','2018-02-26 04:57:57','2018-02-26 04:57:57'),(2,'darthcaedus78@gmail.com','Signatory delete failed.','2018-02-26 06:41:50','2018-02-26 06:41:50'),(3,'darthcaedus78@gmail.com','Signatory delete successful.','2018-02-26 06:42:09','2018-02-26 06:42:09'),(4,'darthcaedus78@gmail.com','Signatory delete successful.','2018-02-26 06:42:15','2018-02-26 06:42:15'),(5,'darthcaedus78@gmail.com','Signatory delete successful.','2018-02-26 06:45:08','2018-02-26 06:45:08'),(6,'darthcaedus78@gmail.com','Signatory delete successful.','2018-02-26 06:45:13','2018-02-26 06:45:13'),(7,'darthcaedus78@gmail.com','Signatory edit successful.','2018-02-26 06:45:18','2018-02-26 06:45:18'),(8,'darthcaedus78@gmail.com','Signatory delete successful.','2018-02-26 06:45:28','2018-02-26 06:45:28'),(9,'darthcaedus78@gmail.com','Fund edit successful.','2018-02-26 06:55:42','2018-02-26 06:55:42'),(10,'darthcaedus78@gmail.com','Fund save successful.','2018-02-26 06:58:43','2018-02-26 06:58:43'),(11,'darthcaedus78@gmail.com','Fund delete successful.','2018-02-26 06:58:51','2018-02-26 06:58:51'),(12,'darthcaedus78@gmail.com','Fund save successful.','2018-02-26 06:59:50','2018-02-26 06:59:50'),(13,'darthcaedus78@gmail.com','Fund save successful.','2018-02-26 07:00:10','2018-02-26 07:00:10'),(14,'darthcaedus78@gmail.com','Fund save successful.','2018-02-26 07:00:30','2018-02-26 07:00:30'),(15,'darthcaedus78@gmail.com','Fund save successful.','2018-02-26 07:00:44','2018-02-26 07:00:44'),(16,'darthcaedus78@gmail.com','Check save successful.','2018-02-26 07:36:24','2018-02-26 07:36:24'),(17,'darthcaedus78@gmail.com','Check edit successful.','2018-02-26 07:36:37','2018-02-26 07:36:37'),(18,'darthcaedus78@gmail.com','LDDAP save successful.','2018-02-26 20:27:29','2018-02-26 20:27:29'),(19,'darthcaedus78@gmail.com','LDDAP delete successful.','2018-03-03 05:33:46','2018-03-03 05:33:46'),(20,'darthcaedus78@gmail.com','Collection save successful.','2018-03-03 20:59:19','2018-03-03 20:59:19'),(21,'darthcaedus78@gmail.com','LDDAP delete successful.','2018-03-03 21:45:00','2018-03-03 21:45:00'),(22,'darthcaedus78@gmail.com','Collection save successful.','2018-03-03 21:45:56','2018-03-03 21:45:56'),(23,'darthcaedus78@gmail.com','Check save successful.','2018-03-03 22:42:54','2018-03-03 22:42:54'),(24,'darthcaedus78@gmail.com','Check save successful.','2018-03-03 22:48:37','2018-03-03 22:48:37'),(25,'darthcaedus78@gmail.com','Check save successful.','2018-03-03 22:58:50','2018-03-03 22:58:50'),(26,'darthcaedus78@gmail.com','Check save successful.','2018-03-03 23:00:02','2018-03-03 23:00:02'),(27,'darthcaedus78@gmail.com','Check save successful.','2018-03-04 00:26:27','2018-03-04 00:26:27'),(28,'darthcaedus78@gmail.com','Collection save successful.','2018-03-04 00:30:37','2018-03-04 00:30:37');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'janarnoldc@gmail.com','$2y$10$st25qJv83TcUm5f7c.bQju1fMNzo8PY0mMOKbnhE/bewJeTODVw.u','Jan Arnold','Abellon','Castillo','1518536229.png','g3KizgtGJlB5Ez3HBrjVgWoqxNfkRNJdkdv6EP5UGhmmzohYlwCiRCKJEsAI','2018-02-13 06:16:14','2018-02-13 07:37:09');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checks`
--

DROP TABLE IF EXISTS `checks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `check_number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fund_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher_date` date NOT NULL,
  `dv_num` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ors_burs_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `particular` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `uacs_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` decimal(17,2) NOT NULL,
  `status` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `checks_check_number_unique` (`check_number`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checks`
--

LOCK TABLES `checks` WRITE;
/*!40000 ALTER TABLE `checks` DISABLE KEYS */;
INSERT INTO `checks` VALUES (1,'0000144545','1','2018-02-19','18-02-445','18-02-445','asdasdasd','asdasdasd','asdasdasd','5141515151',1500000.00,'CANCELLED','2018-02-18 09:33:49','2018-02-25 20:39:12'),(2,'0001919191','1','2018-02-22','18-02-111','18-02-111','asd','asd','asd','4195199519',150000.00,'ACTIVE','2018-02-22 04:44:43','2018-02-22 04:44:43'),(3,'1651516165','1','2018-11-11','18-11-454','18-11-654','asd','asd','asd','5199191919',150000.00,'ACTIVE','2018-02-22 04:54:53','2018-02-22 04:54:53'),(8,'132','5','2018-01-01','18-01-123','18-01-123','123','123','123','123',132.00,'ACTIVE','2018-02-26 07:35:52','2018-02-26 07:35:52'),(9,'123','1','2018-01-01','18-01-201','18-01-201','123','asd','123','123',132.00,'ACTIVE','2018-02-26 07:36:24','2018-02-26 07:36:37'),(10,'1500000000','3','2018-02-02','18-02-123','18-02-123','asdasd','asdasd','asdasd','1213165449',1200.00,'ACTIVE','2018-03-03 22:42:54','2018-03-03 22:42:54'),(11,'2121321321','6','2018-02-02','18-02-111','18-02-111','132132132','1321321321','321321321','2121321321',1500.00,'ACTIVE','2018-03-03 22:48:37','2018-03-03 22:48:37'),(12,'11','1','2018-02-02','18-02-111','18-02-111','111','11','11','11',11.00,'ACTIVE','2018-03-03 22:58:50','2018-03-03 22:58:50'),(14,'3213211321','1','2018-02-02','18-02-111','18-02-111','11','11','11','3203210321',11.00,'ACTIVE','2018-03-03 23:00:02','2018-03-03 23:00:02'),(15,'0001432875','1','2018-01-12','18-01-005',NULL,'Simplicio A. Aborita','DepEd, Region 1, SFC','To payment of Provident fund loan in the amount of 18,194.59','1030199000',18194.59,'ACTIVE','2018-03-04 00:26:27','2018-03-04 00:26:27');
/*!40000 ALTER TABLE `checks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collections`
--

DROP TABLE IF EXISTS `collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `or_number` int(11) NOT NULL,
  `total` decimal(17,2) NOT NULL,
  `collecting_officer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `collections_or_number_unique` (`or_number`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collections`
--

LOCK TABLES `collections` WRITE;
/*!40000 ALTER TABLE `collections` DISABLE KEYS */;
INSERT INTO `collections` VALUES (1,1,600.00,'darthcaedus78@gmail.com','2018-03-03 05:28:31','2018-03-03 05:28:31'),(2,2,500.00,'darthcaedus78@gmail.com','2018-03-03 20:59:19','2018-03-03 20:59:19'),(3,3,600.00,'darthcaedus78@gmail.com','2018-03-03 21:45:56','2018-03-03 21:45:56'),(4,4,650.00,'darthcaedus78@gmail.com','2018-03-04 00:30:36','2018-03-04 00:30:36');
/*!40000 ALTER TABLE `collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funds`
--

DROP TABLE IF EXISTS `funds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cluster_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funds`
--

LOCK TABLES `funds` WRITE;
/*!40000 ALTER TABLE `funds` DISABLE KEYS */;
INSERT INTO `funds` VALUES (1,'0202-0246-88','07','Provident Fund Loan','PROV','2018-02-16 20:32:24','2018-02-26 06:54:48'),(3,'2020-90285-1','01101101','COE','COE','2018-02-26 06:59:50','2018-02-26 06:59:50'),(4,'2020-90286-9','01101101','RGTL','RGTL','2018-02-26 07:00:10','2018-02-26 07:00:10'),(5,'0202-0241-06','06','NEAP','NEAP','2018-02-26 07:00:30','2018-02-26 07:00:30'),(6,'0202-0256-41','01','ROF','ROF','2018-02-26 07:00:44','2018-02-26 07:00:44');
/*!40000 ALTER TABLE `funds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lddaps`
--

DROP TABLE IF EXISTS `lddaps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lddaps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fund_id` int(11) NOT NULL,
  `lddap_ada_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sliiae_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher_date` date NOT NULL,
  `total` decimal(17,2) NOT NULL,
  `status` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lddaps_lddap_ada_number_unique` (`lddap_ada_number`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lddaps`
--

LOCK TABLES `lddaps` WRITE;
/*!40000 ALTER TABLE `lddaps` DISABLE KEYS */;
INSERT INTO `lddaps` VALUES (1,1,'5 45449846-12-102-3165','123','2018-02-26',15000.00,'CANCELLED','2018-02-25 23:36:27','2018-02-26 04:08:55');
/*!40000 ALTER TABLE `lddaps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_02_03_051016_create_funds_table',1),(4,'2018_02_03_070638_create_signatories_table',1),(5,'2018_02_03_102003_create_checks_table',1),(6,'2018_02_13_134346_create_admins_table',2),(8,'2018_02_24_051904_create_lddaps_table',3),(9,'2018_02_26_123626_create_activity_log_table',4),(10,'2018_02_28_054644_create_collections_table',5),(11,'2018_02_28_080858_create_particulars_table',5);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `particulars`
--

DROP TABLE IF EXISTS `particulars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `particulars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `collection` int(11) NOT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(17,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `particulars`
--

LOCK TABLES `particulars` WRITE;
/*!40000 ALTER TABLE `particulars` DISABLE KEYS */;
INSERT INTO `particulars` VALUES (1,1,'Collection 1',100.00,'2018-03-03 05:28:31','2018-03-03 05:28:31'),(2,1,'Collection 2',200.00,'2018-03-03 05:28:32','2018-03-03 05:28:32'),(3,1,'Collection 3',300.00,'2018-03-03 05:28:32','2018-03-03 05:28:32'),(4,2,'Collection A',500.00,'2018-03-03 20:59:19','2018-03-03 20:59:19'),(5,3,'Collection 1',100.00,'2018-03-03 21:45:56','2018-03-03 21:45:56'),(6,3,'Collection 2',200.00,'2018-03-03 21:45:56','2018-03-03 21:45:56'),(7,3,'Collection 3',300.00,'2018-03-03 21:45:56','2018-03-03 21:45:56'),(8,4,'Permits',150.00,'2018-03-04 00:30:36','2018-03-04 00:30:36'),(9,4,'Misc',500.00,'2018-03-04 00:30:37','2018-03-04 00:30:37');
/*!40000 ALTER TABLE `particulars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signatories`
--

DROP TABLE IF EXISTS `signatories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signatories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signatories`
--

LOCK TABLES `signatories` WRITE;
/*!40000 ALTER TABLE `signatories` DISABLE KEYS */;
INSERT INTO `signatories` VALUES (1,'Cristeta','M.','Oineza','Cash Unit','2018-02-22 04:51:37','2018-02-22 04:51:37'),(2,'Alma Ruby','C.','Torio','Regional Director','2018-02-22 04:51:56','2018-02-22 04:51:56');
/*!40000 ALTER TABLE `signatories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'darthcaedus78@gmail.com','$2y$10$mBkTYW8kDgkrtjRA6Ql.bO5650549HY83L4vMGvZIOUHqPhwDQQFW','Jacen','Organa','Solo','1519203593.jpg','rfe1dYzilmnChmn6HL3rLYDajiYM6mcPBZSdNPbG4Ca8SjBDq5DJtj9J1fn4','2018-02-13 05:43:03','2018-02-21 00:59:53'),(2,'arielrodriguez@cdrms.dev','$2y$10$lng/LXMUFx7kCsTua/Pkxe62gFVrozZP6OeHymAx7l9k7TcGHOo9O','Ariel','L.','Rodriguez',NULL,NULL,'2018-02-16 19:28:26','2018-02-16 19:28:26');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-04 16:35:49
