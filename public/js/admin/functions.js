var tblListOfCashUnitUsers = $('#tblListOfCashUnitUsers').DataTable({
    processing: true,
    ajax:{
        url: '/users/cash-unit/list/all',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'id'},
        {'data':'email'},
        {'data':'fullname',
            render: function(data,type,fullname,meta){
                return fullname.fname+' '+$.trim(fullname.mname)+' '+fullname.lname;
            }
        },
        {'data':null}
    ],
    columnDefs: [{
        targets: -1,
        data: null,
        defaultContent: '\
            <button type = "button" id="btnViewUser" class="ui mini icon button"><i class = "edit icon"></i></button>\
        ',
        orderable: false
    },{
        searchable: true,
        orderable: true,
        targets: 0
    }],
    pageLength: 5,
    deferRender: true
});
$('#tblListOfCashUnitUsers tbody').on('click', '#btnViewUser', function(event){
    var dataSelect = tblListOfCashUnitUsers.row( $(this).parents('tr') ).data();
    var id = dataSelect['id'];
    self.location = ('/users/cash-unit/view/'+id);
});