//initializes semantic dropdown
$('.ui.dropdown').not('.ui.simple.dropdown.item').dropdown();
//initialize semantic ui accordion
$('.ui.accordion').accordion();
//initialize semantic ui checkbox
$('.ui.checkbox').checkbox();

//smooth scrolling...is annie okay?
$('a[href*="#"]')
	// Remove links that don't actually link to anything
	.not('[href="#"]')
	.not('[href="#0"]')
	.click(function(event) {
	// On-page links
	if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
		// Figure out element to scroll to
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		// Does a scroll target exist?
		if (target.length) {
			// Only prevent default if animation is actually gonna happen
			event.preventDefault();
			$('html, body').animate({
				scrollTop: target.offset().top
			}, 1000);
		}
	}
});

//toastr option configuration
toastr.options = {
	'closeButton': true,
	'debug': false,
	'newestOnTop': true,
	'progressBar': true,
	'positionClass': 'toast-top-right',
	'preventDuplicates': false,
	'onclick': null,
	'showDuration': '300',
	'hideDuration': '1000',
	'timeOut': '7500',
	'extendedTimeOut': '1000',
	'showEasing': 'swing',
	'hideEasing': 'linear',
	'showMethod': 'fadeIn',
	'hideMethod': 'fadeOut'
}

function arrSum(array) {
	var sum = 0;
	for (var i = 0; i < array.length; i++) {
		if (typeof array[i]['mealprice'] == 'object') {
			sum += arrSum((parseFloat(array[i]['mealprice']) * parseFloat(array[i]['mealqty'])));
		}
		else {
			sum += (parseFloat(array[i]['mealprice']) * parseFloat(array[i]['mealqty']));
		}
	}
	return sum;
}

//copied from admin/functions.js
//get server time
function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    h = checkTime(h);
    m = checkTime(m);
    s = checkTime(s);
    $('#server-time').html((h%24) + ":" + m + ":" + s + ' ' +ampm(h));
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {
    	i = "0" + i;
    }  // add zero in front of numbers < 10
    return i;
}
function ampm(h) {
	if (h < 12) {
		return 'AM';
	}
	else {
		return 'PM';
	}
}
startTime();

function zeroFill( number, width )
{
    width -= number.toString().length;
    if ( width > 0 ) {
        return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
    }
    return number + ""; // always return a string
}

//FUNDS
//load funds to dropdown in add vouchers module
$.ajax({
    url: '/funds/list/all',
    method: 'get',
    cache: false,
    dataType: 'json',
    success: function(data) {
        if (data !== undefined) {
            var array = [];
            $.each(data, function(i,val){
                array.push('<option value = "'+val.id+'" data-code = "'+val.code+'">'+val.desc+', '+val.cluster_number+'</option>');
            });
            $('#cmbAddVoucherFundCluster, #cmbRCIFundCluster, #cmbLDDAPFundCluster, #cmbEditVoucherFundCluster, #cmbAddLDDAPFundCluster, #cmbEditCheckFundCluster, #cmbEditLDDAPFundCluster, #cmbAddCollectionFundCluster, #cmbGenerateSLIIAEFundCluster').html(array);
        }
        else {
            toastr.error('Failed to load funds to dropdown');
        }
    }
});
//COLLECTIBLE FUNDS
//load collectible funds to dropdown in add collection module
$.ajax({
    url: '/funds/list/collectibles',
    method: 'get',
    cache: false,
    dataType: 'json',
    success: function(data) {
        if (data !== undefined) {
            var array = [];
            $.each(data, function(i,val){
                array.push('<option value = "'+val.id+'">'+val.desc+' - '+val.cluster_number+'</option>');
            });
            $('#cmbAddCollectionFundCluster, #cmbReportCollectionFundCluster').html(array);
        }
        else {
            toastr.error('Failed to load collectible funds to dropdown');
        }
    }
});
var tblListOfFunds = $('#tblListOfFunds').DataTable({
    processing: true,
    ajax:{
        url: '/funds/list/all',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'id'},
        {'data':'desc'},
        {'data':'account_number'},
        {'data':'cluster_number'},
        {'data':null}
    ],
    columnDefs: [{
        targets: -1,
        data: null,
        defaultContent: '\
            <button type = "button" id="btnViewFund" class="ui mini icon button"><i class = "edit icon"></i></button>\
        ',
        orderable: false
    },{
        searchable: true,
        orderable: true,
        targets: 0
    }],
    pageLength: 5,
    deferRender: true
});
$('#tblListOfFunds tbody').on('click', '#btnViewFund', function(event){
    var dataSelect = tblListOfFunds.row( $(this).parents('tr') ).data();
    var id = dataSelect['id'];
    self.location = ('/settings/funds/view/'+id);
});

//SIGNATORIES
//load signatories to dropdown in add vouchers module
$.ajax({
    url: '/signatories/list/all',
    method: 'get',
    cache: false,
    dataType: 'json',
    success: function(data) {
        if (data !== undefined) {
            var array = [];
            $.each(data, function(i,val){
                array.push('<option value = "'+val.id+'" data-nameonly = "'+$.trim(val.fname)+' '+$.trim(val.lname)+'">'+$.trim(val.fname)+' '+$.trim(val.lname)+', '+$.trim(val.position)+'</option>');
            });
            $('#cmbAddCashAdvanceSignatory').html(array);
        }
        else {
            toastr.error('Failed to load funds to dropdown');
        }
    }
});
var tblListOfSignatories = $('#tblListOfSignatories').DataTable({
    processing: true,
    ajax:{
        url: '/signatories/list/all',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'id'},
        {'data':'fullname',
            render: function(data,row,fullname,meta){
                return fullname.fname + ' ' + $.trim(fullname.mname) + ' ' + fullname.lname;
            }
        },
        {'data':'position'},
        {'data':null}
    ],
    columnDefs: [{
        targets: -1,
        data: null,
        defaultContent: '\
            <button type = "button" id="btnViewSignatory" class="ui mini icon button"><i class = "edit icon"></i></button>\
        ',
        orderable: false
    },{
        searchable: true,
        orderable: true,
        targets: 0
    }],
    pageLength: 5,
    deferRender: true
});
$('#tblListOfSignatories tbody').on('click', '#btnViewSignatory', function(event){
    var dataSelect = tblListOfSignatories.row( $(this).parents('tr') ).data();
    var id = dataSelect['id'];
    self.location = ('/settings/signatories/view/'+id);
});

//VOUCHERS
var tblListOfAllVouchers = $('#tblListOfAllVouchers').DataTable({
    processing: true,
    ajax:{
        url: '/vouchers/list/all',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'dv_num'},
        {'data':'cluster_number'},
        {'data':'payee'},
        {'data':'total'},
        {'data':'voucher_date'},
        {'data':null}
    ],
    columnDefs: [{
        targets: -1,
        data: null,
        defaultContent: '\
            <button type = "button" id="btnViewVoucher" class="ui mini icon button"><i class = "eye icon"></i></button>\
        ',
        orderable: false
    },{
        searchable: true,
        orderable: true,
        targets: 0
    }],
    pageLength: 5,
    deferRender: true
});
$(document).on('click','#btnViewVoucher', function(event){
    event.preventDefault();
    var dataSelect = tblListOfAllVouchers.row( $(this).parents('tr') ).data();
    var id = dataSelect['dv_num'];
    self.location = ('/vouchers/view/'+id);
});

//CHECKS
var tblListOfAllChecks = $('#tblListOfAllChecks').DataTable({
    processing: true,
    ajax:{
        url: '/checks/list/all',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'check_number'},
        {'data':'cluster_number'},
        {'data':'payee'},
        {'data':'total'},
        {'data':'voucher_date'},
        {'data':'status'},
        {'data':null}
    ],
    columnDefs: [{
        targets: -1,
        data: null,
        defaultContent: '\
            <button type = "button" id="btnViewCheck" class="ui mini icon button"><i class = "edit icon"></i></button>\
            <button type = "button" id="btnPrintCheck" class="ui mini icon button"><i class = "print icon"></i></button>\
        ',
        orderable: false
    },{
        searchable: true,
        orderable: true,
        targets: 0
    }],
    pageLength: 5,
    deferRender: true
});
$('#tblListOfAllChecks tbody').on('click','#btnViewCheck', function(event){
    event.preventDefault();
    var dataSelect = tblListOfAllChecks.row( $(this).parents('tr') ).data();
    var id = dataSelect['check_number'];
    self.location = ('/checks/view/'+id);
});
$('#tblListOfAllChecks tbody').on('click','#btnPrintCheck', function(event){
    event.preventDefault();
    var dataSelect = tblListOfAllChecks.row( $(this).parents('tr') ).data();
    var id = dataSelect['check_number'];

    window.open('/checks/print/signed/'+id);
    window.open('/checks/print/unsigned/'+id);
		window.open('/checks/advice/'+id);
});

$(document).on('click', '#pgRCI', function(event){
    event.preventDefault();
    $('[data-remodal-id=modalRCI]').remodal({
        closeOnOutsideClick: false
    }).open();
});
//get stale checks
var arrayStaleChecks = [];
function fnGetStaleChecks() {
    $.ajax({
        url: '/checks/list/stale',
        method: 'get',
        cache: false,
        dataType: 'json',
        success: function(data){
            if (data !== undefined) {
                if (data.length > 0) {
                    $('.notifStaleChecks').removeClass('hidden');
                    $('.notifStaleChecks').html(data.length);
                }
                else {
                    $('.notifStaleChecks').toggleClass('hidden',true);
                }
            }
            else {
                toastr.error('Unable to retrieve count of stale checks');
            }
        }
    });
}
fnGetStaleChecks();
setTimeout(fnGetStaleChecks(),360000);
var tblListOfStaleChecks = $('#tblListOfStaleChecks').DataTable({
    processing: true,
    ajax:{
        url: '/checks/list/stale',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'check_number'},
        {'data':'cluster_number'},
        {'data':'payee'},
        {'data':'total'},
        {'data':'voucher_date'},
        {'data':'status'}
    ],
    pageLength: 5,
    deferRender: true
});

//LDDAP
//load report number to dropdown in LDDAP print module
$.ajax({
    url: '/lddap/list/report_num',
    method: 'get',
    cache: false,
    dataType: 'json',
    success: function(data) {
        if (data !== undefined) {
            var array = [];
            var rep_num = [];
            $.each(data, function(i,val){
                var id = val.dv_num.substring(0,5);
                if($.inArray(id, rep_num) === -1) rep_num.push(id);
            });
            $.each(rep_num, function(i,val){
                array.push('<option value = "'+ val +'">'+ val +'-001</option>');
            });
            $('#txtLDDAPReportNum').html(array);
        }
        else {
            toastr.error('Failed to load funds to dropdown');
        }
    }
});
var tblListOfAllLDDAP = $('#tblListOfAllLDDAP').DataTable({
    processing: true,
    ajax:{
        url: '/lddap/list/all',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'lddap_ada_number'},
        {'data':'sliiae_number'},
        {'data':'cluster_number'},
        {'data':'payee'},
        {'data':'total'},
        {'data':'voucher_date'},
        {'data':'status'},
        {'data':null}
    ],
    columnDefs: [{
        targets: -1,
        data: null,
        defaultContent: '\
            <button type = "button" id="btnViewLDDAP" class="ui mini icon button"><i class = "edit icon"></i></button>\
            <button type = "button" id="btnPrintLDDAP" class="ui mini icon button"><i class = "print icon"></i></button>\
        ',
        orderable: false
    },{
        searchable: true,
        orderable: true,
        targets: 0
    }],
    pageLength: 5,
    deferRender: true
});
//LDDAPs report print
$(document).on('click', '#pgLDDAP', function(event){
    event.preventDefault();
    $('[data-remodal-id=modalLDDAP]').remodal({
        closeOnOutsideClick: false
    }).open();
});
$('#tblListOfAllLDDAP tbody').on('click', '#btnViewLDDAP', function(event){
    var dataSelect = tblListOfAllLDDAP.row( $(this).parents('tr') ).data();
    var id = dataSelect['lddap_ada_number'];
    self.location = ('/lddap/view/'+id);
});
$('#tblListOfAllLDDAP tbody').on('click', '#btnPrintLDDAP', function(event){
    var dataSelect = tblListOfAllLDDAP.row( $(this).parents('tr') ).data();
    var id = dataSelect['lddap_ada_number'];
    window.open('/reports/lddap/'+id);
});
$(document).on('click', '#pgSLIIAE', function(event){
    event.preventDefault();
    $('[data-remodal-id=modalSLIIAE]').remodal({
        closeOnOutsideClick: false
    }).open();
});

//COLLECTIONS
function arrSum(array) {
    var sum = 0;
    for (var i = 0; i < array.length; i++) {
        if (typeof array[i]['mealprice'] == 'object') {
            sum += arrSum((parseFloat(array[i]['amount'])));
        }
        else {
            sum += (parseFloat(array[i]['amount']));
        }
    }
    return sum;
}
var tocollect = [];
var tempID = 0;
$('#tblListOfToCollect tfoot').on('click', '#btnToCollectAdd', function(){
    $('#tblListOfToCollect tbody').empty();
    var description = $(this).closest('tr').find('#txtToCollectDescription').val();
    var amount = parseFloat($(this).closest('tr').find('#txtToCollectAmount').val());

    if (description != '' && amount > 0) {
        tempID++;
        selected = {
            'tempID'        : tempID,
            'description'   : description,
            'amount'        : amount
        };
        tocollect.push(selected);
        $.each(tocollect, function(i,val){
            $('#tblListOfToCollect tbody').append(
                '<tr>'+
                    '<td>'+val.tempID+'</td>'+
                    '<td>'+val.description+'</td>'+
                    '<td>'+val.amount+'</td>'+
                    '<td><button type = "button" id = "btnToCollectRemove" class = "ui mini icon button"><i class = "trash icon"></i></button></td>'+
                '</tr>'
            );
        });
        console.log(tocollect);
        console.log(arrSum(tocollect));
        $('#nudAddCollectionTotal').val(arrSum(tocollect));
    }
    else {
        toastr.warning('Please input a collection description');
        toastr.warning('Collection amount cannot be empty or zero (0)');
    }

    //reset everything
    $(this).closest('tr').find('#txtToCollectDescription').val('');
    $(this).closest('tr').find('#txtToCollectAmount').val(0);
});
//delete appended row to tblListOfToCollect
$('#tblListOfToCollect tbody').on('click','#btnToCollectRemove', function(event){
    var todelete = $(this).closest('tr').remove();
    $.each(tocollect,function(i,val){
        if (tocollect[i].tempID == val.tempID) {
            tocollect.splice(i,1);
            return false;
        }
    });
    $('#nudAddCollectionTotal').val(arrSum(tocollect));
    // toassign.splice(id,1);
});
$('#frmAddCollection').submit(function(event){
    event.preventDefault();
    // nanobar.go(80);
    $('#btnAddCollection').toggleClass('disabled loading',true);
    $.ajax({
        url: '/collections/create',
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            'token'                         : $('#token').val(),
            'txtAddCollectionPayor'         : $('#txtAddCollectionPayor').val(),
            'txtAddCollectionAgency'        : $('#txtAddCollectionAgency').val(),
            'cmbAddCollectionFundCluster'   : $('#cmbAddCollectionFundCluster').val(),
            'txtAddCollectionORNumber'      : $('#txtAddCollectionORNumber').val(),
            'txtAddCollectionDate'          : $('#txtAddCollectionDate').val(),
            'nudAddCollectionTotal'         : $('#nudAddCollectionTotal').val(),
            'tocollect'                     : tocollect
        },
        dataType: 'json',
        beforeSend: function(xhr) {
            if (tocollect.length == 0) {
                xhr.abort();
                toastr.error('Please add a collection first.');
            }
        },
        success: function(result) {
            if (result["saved"] == true) {
                self.location = '/collections/create/'+result["or_number"]+'/successful';
            }
            else {
                self.location = '/collections/create/'+result["or_number"]+'/failed';
            }
        }
    });
});
var tblListOfCollections = $('#tblListOfCollections').DataTable({
    processing: true,
    ajax:{
        url: '/collections/list/all',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'or_number'},
        {'data':'total'},
        {'data':'fullname',
            render: function(data,type,fullname,meta) {
                return $.trim(fullname.fname)+ ' ' +$.trim(fullname.mname)+ ' ' +$.trim(fullname.lname);
            }
        },
        {'data':'created_at'},
        {'data':null}
    ],
    columnDefs: [{
        targets: -1,
        data: null,
        defaultContent: '\
            <button type = "button" id="btnViewCollection" class="ui mini icon button"><i class = "eye icon"></i></button>\
            <button type = "button" id="btnPrintCollection" class="ui mini icon button"><i class = "print icon"></i></button>\
        ',
        orderable: false
    },{
        searchable: true,
        orderable: true,
        targets: 0
    }],
    pageLength: 5,
    deferRender: true
});
$(document).on('click','#btnViewCollection', function(event){
    event.preventDefault();
    var dataSelect = tblListOfCollections.row( $(this).parents('tr') ).data();
    var id = dataSelect['or_number'];
    self.location = ('/collections/view/'+id);
});
$(document).on('click','#btnPrintCollection', function(event){
    event.preventDefault();
    var dataSelect = tblListOfCollections.row( $(this).parents('tr') ).data();
    var id = dataSelect['or_number'];
    window.open('/reports/collections/or/'+id);
});
$(document).on('click', '#pgReportCollection', function(event){
    event.preventDefault();
    $('[data-remodal-id=modalReportCollection]').remodal({
        closeOnOutsideClick: false
    }).open();
});

//activity log
var tblActivityLog = $('#tblActivityLog').DataTable({
    processing: true,
    ajax:{
        url: '/settings/activities/list/all',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'id'},
        {'data':'fullname',
            render: function(data,type,fullname,meta) {
                return $.trim(fullname.fname)+ ' ' +$.trim(fullname.mname)+ ' ' +$.trim(fullname.lname);
            }
        },
        {'data':'action'},
        {'data':'created_at'}
    ],
    pageLength: 5,
    deferRender: true
});

//cash advances
//load cash advance vouchers
var tblListOfCashAdvances = $('#tblListOfCashAdvances').DataTable({
    processing: true,
    ajax:{
        url: '/advances/list/all',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'id'},
        {'data':'voucher'},
        {'data':'check_number'},
        {'data':'receiver_name'},
        {'data':'total'},
        {'data':'voucher_date'},
        {'data':'status'},
        {'data':null}
    ],
    columnDefs: [{
        targets: -1,
        data: null,
        defaultContent:
            '<button type = "button" id="btnViewAdvance" class="ui mini icon button"><i class = "eye icon"></i></button>'+
            '<button type = "button" id="btnPrintAdvanceCheck" class="ui mini icon button" title="Print check"><i class = "print icon"></i></button>'+
            '<button type = "button" id="btnPrintLiquidationReport" class="ui mini icon button" title="Print liquidation report"><i class = "print icon"></i></button>'
        ,
        orderable: false
    },{
        searchable: true,
        orderable: true,
        targets: 0
    }],
    pageLength: 5,
    deferRender: true
});
$(document).on('click','#btnViewAdvance', function(event){
    event.preventDefault();
    var dataSelect = tblListOfCashAdvances.row( $(this).parents('tr') ).data();
    var id = dataSelect['id'];
    self.location = '/advances/view/'+id;
});
$(document).on('click','#btnPrintAdvance', function(event){
    event.preventDefault();
    var dataSelect = tblListOfCashAdvances.row( $(this).parents('tr') ).data();
    var id = dataSelect['check_number'];
    window.open('/checks/print/signed/'+id);
    window.open('/checks/print/unsigned/'+id);
});
$(document).on('click','#btnPrintLiquidationReport', function(event){
    event.preventDefault();
    var dataSelect = tblListOfCashAdvances.row( $(this).parents('tr') ).data();
    var id = dataSelect['voucher'];
    window.open('/reports/liquidation/'+id);
});
$.ajax({
    url: '/advances/list/no-liquidation',
    method: 'get',
    cache: false,
    dataType: 'json',
    success: function(data){
        if (data !== undefined) {
            var array = [];
            $.each(data,function(i,val){
                array.push(
                    '<option value = "'+val.voucher+'" data-cashadvanced = "'+val.total+'">'+val.voucher+'</option>'
                );
            });
            $('#cmbLiquidationDVNum').html(array);
            $('#nudToAdvanceAdvancedCash').val(parseFloat($('#cmbLiquidationDVNum option:selected').data('cashadvanced')));
        }
        else {
            toastr.error('Error loading cash advances');
        }
    }
});
//load dv num of existing liquidation reports
$.ajax({
    url: '/advances/list/existing',
    method: 'get',
    cache: false,
    dataType: 'json',
    success: function(data){
        if (data !== undefined) {
            var array = [];
            $.each(data,function(i,val){
                array.push(
                    '<option value = "'+val.voucher+'">'+val.voucher+'</option>'
                );
            });
            $('#cmbExistingLiquidationDVNum').html(array);
        }
        else {
            toastr.error('Error loading existing cash advances');
        }
    }
});
$(document).on('click', '#pgLiquidation', function(event){
    event.preventDefault();
    $('[data-remodal-id=modalLiquidation]').remodal({
        closeOnOutsideClick: false
    }).open();
});
$(document).on('change','#cmbLiquidationDVNum',function(event){
    event.preventDefault();
    $('#nudToAdvanceAdvancedCash').val(parseFloat($('#cmbLiquidationDVNum option:selected').data('cashadvanced')));
    console.log($('#cmbLiquidationDVNum option:selected').data('cashadvanced'));
});
var toadvance = [];
var tempID = 0;
$('#tblListOfToAdvance tfoot').on('click', '#btnToAdvanceAdd', function(){
    $('#tblListOfToAdvance tbody').empty();
    var description = $(this).closest('tr').find('#txtToAdvanceDescription').val();
    var amount = parseFloat($(this).closest('tr').find('#txtToAdvanceAmount').val());

    if (description != '' && amount > 0) {
        tempID++;
        selected = {
            'tempID'        : tempID,
            'description'   : description,
            'amount'        : amount
        };
        toadvance.push(selected);
        $.each(toadvance, function(i,val){
            $('#tblListOfToAdvance tbody').append(
                '<tr>'+
                    '<td>'+val.tempID+'</td>'+
                    '<td>'+val.description+'</td>'+
                    '<td>'+val.amount+'</td>'+
                    '<td><button type = "button" id = "btnToAdvanceRemove" class = "ui mini icon button"><i class = "trash icon"></i></button></td>'+
                '</tr>'
            );
        });
        console.log(toadvance);
        console.log(arrSum(toadvance));
        $('#nudToAdvanceTotalAmountSpent').val(arrSum(toadvance));

        $('#nudToAdvanceToReimburse').val(
            parseFloat($('#nudToAdvanceAdvancedCash').val()) - parseFloat($('#nudToAdvanceTotalAmountSpent').val())
        );
    }
    else {
        toastr.warning('Please input a description');
        toastr.warning('Item amount cannot be empty or zero (0)');

        $.each(toadvance, function(i,val){
            $('#tblListOfToAdvance tbody').append(
                '<tr>'+
                    '<td>'+val.tempID+'</td>'+
                    '<td>'+val.description+'</td>'+
                    '<td>'+val.amount+'</td>'+
                    '<td><button type = "button" id = "btnToAdvanceRemove" class = "ui mini icon button"><i class = "trash icon"></i></button></td>'+
                '</tr>'
            );
        });
        console.log(toadvance);
        console.log(arrSum(toadvance));
        $('#nudToAdvanceTotalAmountSpent').val(arrSum(toadvance));

        $('#nudToAdvanceToReimburse').val(
            parseFloat($('#nudToAdvanceAdvancedCash').val()) - parseFloat($('#nudToAdvanceTotalAmountSpent').val())
        );

    }

    //reset everything
    $(this).closest('tr').find('#txtToAdvanceDescription').val('');
    $(this).closest('tr').find('#txtToAdvanceAmount').val(0);
});
//delete appended row to tblListOfToAdvance
$('#tblListOfToAdvance tbody').on('click','#btnToAdvanceRemove', function(event){
    var todelete = $(this).closest('tr').remove();
    $.each(toadvance,function(i,val){
        if (toadvance[i].tempID == val.tempID) {
            toadvance.splice(i,1);
            return false;
        }
    });
    $('#nudToAdvanceTotalAmountSpent').val(arrSum(toadvance));
    $('#nudToAdvanceToReimburse').val(
        parseFloat($('#nudToAdvanceAdvancedCash').val()) - parseFloat($('#nudToAdvanceTotalAmountSpent').val())
    );
});
$('#frmLiquidation').submit(function(event){
    event.preventDefault();
    $.ajax({
        url: '/reports/liquidation/'+$('#cmbLiquidationDVNum').val(),
        method: 'put',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            'token':$('#token').val(),
            'cmbLiquidationDVNum':$('#cmbLiquidationDVNum').val(),
            'nudToAdvanceAdvancedCash':$('#nudToAdvanceAdvancedCash').val(),
            'nudToAdvanceTotalAmountSpent':$('#nudToAdvanceTotalAmountSpent').val(),
            'nudToAdvanceToReimburse':$('#nudToAdvanceToReimburse').val(),
            'txtToAdvanceRemarks':$('#txtToAdvanceRemarks').val(),
            'txtToAdvanceReceiverName':$('#txtToAdvanceReceiverName').val(),
            'txtToAdvanceReceiverPosition':$('#txtToAdvanceReceiverPosition').val(),
            'toadvance':toadvance
        },
        dataType: 'json',
        beforeSend: function(xhr) {
            if (toadvance.length == 0) {
                xhr.abort();
                toastr.error('Please add an item first.');
            }
            else {
                $('#btnGenerateLiquidationReport').toggleClass('disabled loading',true);
            }
        },
        success: function(result) {
            if (result["saved"] == true) {
                window.open('/reports/liquidation/'+result["dv_num"]);
                $('[data-remodal-id=modalLiquidation]').remodal().close();
                toastr.success('Cash advance information update successful.');
                location.reload();
            }
            else {
                toastr.error('Cash advance information update failed.');
            }
        }
    });
});

//select first option in all dropdowns by default
$('select').val($('select option:first').val());
