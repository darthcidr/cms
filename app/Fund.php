<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fund extends Model
{
    protected $fillable = [
    	//ADD
    	'txtAddFundDesc',
		'txtAddFundAccountNumber1',
		'txtAddFundAccountNumber2',
		'txtAddFundAccountNumber3',
		'txtAddFundClusterNumber',
		//EDIT
		'txtEditFundDesc',
		'txtEditFundAccountNumber',
		'txtEditFundClusterNumber',
    ];
}
