<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Check extends Model
{
	protected $fillable = [		
		'cmbAddCheckFundCluster',
		'dtpAddCheckDate',
		'txtAddCheckDVNum',
		'txtAddCheckORSBURSNum',
		'txtAddCheckPayee',
		'txtAddCheckPayeeAddress',
		'txtAddCheckParticular',
		'txtAddCheckUACSCode',
		'txtAddCheckCheckNumber',
		'txtAddCheckAmount',
		// 'cmbAddCheckSignatories',
	];
}
