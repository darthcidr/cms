<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LDDAP extends Model
{
	public $table = 'lddaps';

    protected $fillable = [
    	'cmbAddLDDAPFundCluster',
		'txtAddLDDAPSliiaeNumber',
		'dtpAddLDDAPDate',
		'txtAddLDDAPLDDAPADANumber',
		'nudAddLDDAPTotal',
    ];
}
