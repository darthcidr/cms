<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
	// protected $primaryKey = 'dv_num';

    protected $fillable = [		
		'cmbAddVoucherFundCluster',
		'dtpAddVoucherDate',
		'txtAddVoucherDVNum',
		'txtAddVoucherORSBURSNum',
		'txtAddVoucherPayee',
		'txtAddVoucherPayeeAddress',
		'txtAddVoucherParticular',
		'txtAddVoucherUACSCode',
		'txtAddVoucherCheckNumber',
		'txtAddVoucherAmount',
		// 'cmbAddVoucherSignatories',
	];
}
