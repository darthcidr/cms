<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Particular extends Model
{
    protected $fillable = [
		'txtAddCollectionORNumber',
		'tocollect',
    ];
}
