<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Charts;

use App\User;
use App\Check;
use App\LDDAP;
use App\Collection;

class AdminHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin_auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allChecks = Check::join('vouchers','checks.voucher','=','vouchers.dv_num')->count();
        $activeChecks = Check::join('vouchers','checks.voucher','=','vouchers.dv_num')->where('status','ACTIVE')->count();
        $cancelledChecks = Check::join('vouchers','checks.voucher','=','vouchers.dv_num')->where('status','CANCELLED')->count();
        $staleChecks = Check::join('vouchers','checks.voucher','=','vouchers.dv_num')->where('status','STALE')->count();
        
        $chartAnnualChecksReleased = Charts::database(Check::join('vouchers','checks.voucher','=','vouchers.dv_num')->where('status','ACTIVE')->get(), 'line', 'highcharts')
            ->title(date('Y').' Monthly Checks Released')
            ->responsive(true)
            ->xAxisTitle('Month')
            ->yAxisTitle('Checks released')
            ->elementLabel('Checks released per month')
            ->aggregateColumn('check_number','count')
            // ->dateColumn('voucher_date')
            ->groupByMonth()
            ->labels(['January','February','March','April','May','June','July','August','September','October','November','December']);
        $chartAllChecks = Charts::database(Check::join('vouchers','checks.voucher','=','vouchers.dv_num')->get(), 'pie', 'highcharts')
            ->title(date('Y').' All Checks Released')
            ->responsive(false)
            ->dimensions(500,300)
            ->groupBy('status');

        $totalAnnualExpense = Check::join('vouchers','checks.voucher','=','vouchers.dv_num')->whereYear('checks.created_at',date('Y'))->sum('total');
        $annualExpenseChecks = Check::join('vouchers','checks.voucher','=','vouchers.dv_num')->where('status','ACTIVE')->select('total','checks.status','checks.created_at');
        $annualExpenseLDDAP = LDDAP::join('vouchers','lddaps.voucher','=','vouchers.dv_num')->where('status','ACTIVE')->select('total','lddaps.status','lddaps.created_at');
        $chartAnnualExpense = Charts::database($annualExpenseChecks->union($annualExpenseLDDAP)->get(), 'line', 'highcharts')
            ->title(date('Y').' Monthly Expense')
            ->responsive(true)
            ->xAxisTitle('Month')
            ->yAxisTitle('Monthly expense (PHP)')
            ->elementLabel('Expense per month')
            ->aggregateColumn('total','sum')
            // ->dateColumn('voucher_date')
            ->groupByMonth()
            ->labels(['January','February','March','April','May','June','July','August','September','October','November','December']);

        $chartAnnualCollection = Charts::database(Collection::get(), 'line', 'highcharts')
            ->title(date('Y').' Monthly Collection')
            ->responsive(true)
            ->xAxisTitle('Month')
            ->yAxisTitle('Amount collected (PHP)')
            ->elementLabel('Collection per month')
            ->aggregateColumn('total','sum')
            // ->dateColumn('voucher_date')
            ->groupByMonth()
            ->labels(['January','February','March','April','May','June','July','August','September','October','November','December']);
        $chartCollectionAccdgToFund = Charts::database(Collection::join('funds','collections.fund','=','funds.id')->orderBy('desc')->whereYear('collections.created_at',date('Y'))->get(), 'bar', 'highcharts')
            ->title(date('Y').' Collections According to Fund Source')
            ->responsive(true)
            ->xAxisTitle('Fund source')
            ->yAxisTitle('Amount collected (PHP)')
            ->elementLabel('Fund sources')
            ->aggregateColumn('collections.total','sum')
            ->groupBy('desc');

        $usersCount = User::all()->count();

        return view('admin.home')->with([
            'allChecks'                     =>  $allChecks,
            'activeChecks'                  =>  $activeChecks,
            'cancelledChecks'               =>  $cancelledChecks,
            'staleChecks'                   =>  $staleChecks,
            'chartAnnualExpense'            =>  $chartAnnualExpense,
            'chartAnnualChecksReleased'     =>  $chartAnnualChecksReleased,
            'chartAllChecks'                =>  $chartAllChecks,
            'chartAnnualCollection'         =>  $chartAnnualCollection,
            'chartCollectionAccdgToFund'    =>  $chartCollectionAccdgToFund,
            'usersCount'            	=>  $usersCount,
        ]);
    }
}

