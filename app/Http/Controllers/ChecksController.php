<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

use App\Check;
use App\Voucher;
use App\Advance;
use App\Fund;
use App\Signatory;
use App\Activity;

class ChecksController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($check_number)
    {
        // $check = Check::find($check_number);
        $check = Check::join('vouchers','checks.voucher','=','vouchers.dv_num')
                    ->join('funds','vouchers.fund_id','=','funds.id')
                    ->where('check_number',$check_number)
                    ->first();

        return view('checks.view')->with([
            'check' =>  $check,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = Check::where('check_number',$id)->first();
        // $check->check_number = $request["txtEditCheckCheckNumber"];
        $check->status       = $request["cmbEditCheckStatus"];
        $edited = $check->update();
        $action = "Check edit successful.";

        if ($edited == false) {
            $action = "Check edit failed.";
            $this->log($action);
            return redirect('/checks/all')->with([
                'type'          =>  'error',
                'status'        =>  $action,
            ]);
        }

        $this->log($action);
        return redirect('/checks/all')->with([
            'type'          =>  'success',
            'status'        =>  $action,
        ]);
    }

    public function list_all_index()
    {
        return view('checks.all');
    }
    public function list_all_table()
    {
        $checks = Check::join('vouchers','checks.voucher','=','vouchers.dv_num')
                    ->join('funds','vouchers.fund_id','=','funds.id')
                    ->get();

        return $checks;
    }

    public function list_stale_index()
    {
        return view('checks.stale');
    }
    public function list_stale()
    {
        $stale_checks = DB::SELECT("
            SELECT
                checks.check_number,
                checks.created_at,

                vouchers.fund_id,
                funds.id,
                funds.cluster_number,

                vouchers.payee,
                vouchers.total,
                vouchers.voucher_date,
                checks.status,

                DATEDIFF(CURRENT_TIMESTAMP,checks.created_at)
            FROM
                checks
                    INNER JOIN
                        vouchers ON checks.voucher = vouchers.dv_num
                    INNER JOIN
                        funds ON vouchers.fund_id = funds.id
            WHERE
                DATEDIFF(CURRENT_TIMESTAMP,checks.created_at) >= 180
                OR
                status = 'STALE'
        ");

        if (count($stale_checks) > 0) {
            foreach ($stale_checks as $stale_check) {
                DB::table('checks')
                ->where('check_number',$stale_check->check_number)
                ->update(['status'=>'STALE']);
            }
        }

        return $stale_checks;
    }

    public function print_signed($check_number)
    {
        $spellout = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);

        $check = Check::where('check_number',$check_number)
                        ->join('vouchers','checks.voucher','=','vouchers.dv_num')
                        ->join('funds','vouchers.fund_id','=','funds.id')
                        ->first();
        $total_whole = floor($check["total"]);
        $total_decimal = round($check["total"] - $total_whole,2);

        $signatoryCashUnit = Signatory::where('position','Cash Unit')->first();
        $signatoryRegionalDir = Signatory::where('position','Regional Director')->first();

        return view('reports.check-signed')->with([
            'check'                 =>  $check,
            'total_in_words'        =>  ($spellout->format($total_whole).' pesos & '.($total_decimal*100).'/100').' only',
            'signatoryCashUnit'     =>  $signatoryCashUnit,
            'signatoryRegionalDir'  =>  $signatoryRegionalDir,
        ]);
        // return $total_decimal*100;
    }
    public function print_unsigned($check_number)
    {
        $spellout = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);

        $check = Check::where('check_number',$check_number)
                        ->join('vouchers','checks.voucher','=','vouchers.dv_num')
                        ->join('funds','vouchers.fund_id','=','funds.id')
                        ->first();
        $total_whole = floor($check["total"]);
        $total_decimal = round($check["total"] - $total_whole,2);

        return view('reports.check-unsigned')->with([
            'check'                 =>  $check,
            'total_in_words'        =>  ($spellout->format($total_whole).' pesos & '.($total_decimal*100).'/100').' only',
            // 'signatoryCashUnit'     =>  Signatory::where('position','Cash Unit')->first(),
            // 'signatoryRegionalDir'  =>  Signatory::where('position','Regional Director')->first(),
        ]);
        // return $total_decimal*100;
    }

    public function report_checks_issued(Request $request)
    {
        $report_number = $request["txtRCIReportNum"];
        $fund_cluster = $request["cmbRCIFundCluster"];
        $fund = Fund::where('id',$fund_cluster)->first();
        $report_num = substr($report_number,0,5);

        $checks = Check::join('vouchers','checks.voucher','=','vouchers.dv_num')
                    ->join('funds','vouchers.fund_id','=','funds.id')
                    ->whereMonth('voucher_date',explode('-',$report_num)[1])
                    ->where('fund_id',$fund_cluster)
                    ->select(
                        'vouchers.created_at',
                        'checks.check_number',
                        'vouchers.dv_num',
                        'vouchers.ors_burs_num',
                        'vouchers.payee',
                        'vouchers.uacs_code',
                        'vouchers.particular',
                        'vouchers.total',
                        'funds.cluster_number',
                        'funds.account_number'
                    )
                    ->get();
        $checks_total = Check::join('vouchers','checks.voucher','=','vouchers.dv_num')
                        ->join('funds','vouchers.fund_id','=','funds.id')
                        ->where('fund_id',$fund_cluster)
                        ->where('dv_num','like',$report_num.'%')
                        ->sum('total');

        $signatory = Signatory::select('fname', 'mname', 'lname', 'position')->where('position', 'Cash Unit')->first();
        $name = "";
        if (count($signatory)>0) {
          $mi = substr($signatory["mname"], 0, 1);
          $name = $signatory["fname"] . ' ' . $mi . '. ' . $signatory["lname"];
        } else {
          $name = "Unable to retrieve name";
        }

        $first_check = "";
        $last_check = "";
        for($i = 0 ; $i < count($checks) ; $i++) {
            if ($i == 0 || count($checks)==1) {
                $first_check = $checks[$i]["check_number"];
                $last_check = $checks[$i]["check_number"];
            }
            else {
                $last_check = $checks[$i]["check_number"];
            }
        }

        return view('reports.rci')->with([
            'checks'        =>  $checks,
            'checks_total'  =>  $checks_total,
            'fund'          =>  $fund,
            'first_check'   =>  $first_check,
            'last_check'    =>  $last_check,
            'report_number' =>  $report_num,
            'name'          =>  $name,
        ]);
    }

    public function advice($check_number)
    {
        $spellout = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);

        $check = Check::where('check_number',$check_number)
                    ->join('vouchers','checks.voucher','=','vouchers.dv_num')
                    ->join('funds','vouchers.fund_id','=','funds.id')
                    ->first();
        $total_whole = floor($check["total"]);
        $total_decimal = round($check["total"] - $total_whole,2);

        return view('reports.advice')->with([
            'check'                 =>  $check,
            'total_in_words'        =>  ($spellout->format($total_whole).' & '.($total_decimal*100).'/100').' only',
            'signatoryCashUnit'     =>  Signatory::where('position','Cash Unit')->first(),
            'signatoryRegionalDir'  =>  Signatory::where('position','Regional Director')->first(),
        ]);
    }


    // public function report_num_option()
    // {
    //     // $rep_num = Check::select('dv_num')->get();
    //     $rep_num =  Check::join('vouchers','checks.voucher','=','vouchers.dv_num')
    //                 ->select('dv_num')
    //                 ->distinct()
    //                 ->get();

    //     return $rep_num;
    // }

    public function log ($action)
    {
        $log = new Activity;
        if (Auth::guard('web')->check()) {
            $log->email = Auth::guard('web')->user()->email;
        }
        else if (Auth::guard('admin')->check()) {
            $log->email = Auth::guard('admin')->user()->email;
        }
        $log->action = $action;
        $log->save();
    }
}
