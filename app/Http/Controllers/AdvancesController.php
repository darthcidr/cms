<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

use App\Advance;
use App\AdvanceParticular;
use App\Voucher;
use App\Activity;

use DB;

class AdvancesController extends Controller
{
    public function show($id)
    {
        $advance = Advance::find($id);
        $voucher = Voucher::where('dv_num',$advance->voucher)->first();
        $particulars = AdvanceParticular::where('advance',$advance->id)->get();

        return view('advances.view')->with([
            'voucher'=>$voucher,
            'advance'=>$advance,
            'particulars'=>$particulars,
        ]);
    }

    public function update(Request $request, $id)
    {
        $advance = Advance::find($id);
        $advance->receiver_name = $request["txtEditLiquidationReceiverName"];
        $advance->receiver_position = $request["txtEditLiquidationReceiverPosition"];
        $advance->remarks = $request["txtEditLiquidationRemarks"];
        $edited = $advance->update();
        $action = "Cash advance edit successful.";

        if ($edited == false) {
            $action = "Cash advance edit failed.";
            $this->log($action);
            return redirect('/advances/all')->with([
                'type'          =>  'error',
                'status'        =>  $action,
            ]);
        }

        $this->log($action);
        return redirect('/advances/all')->with([
            'type'          =>  'success',
            'status'        =>  $action,
        ]);
    }

	public function list_all()
	{
		$advances = Advance::
                    join('vouchers','advances.voucher','=','vouchers.dv_num')
                    ->join('checks','advances.check_number','=','checks.check_number')
                    ->select(
                        'advances.id',
                        'advances.voucher',
                        'advances.check_number',
                        'advances.receiver_name',
                        'advances.status',
                        'vouchers.total',
                        'vouchers.voucher_date'
                    )
                    ->get();

		return $advances;
	}
    public function list_all_index()
    {
        return view('advances.all');
    }
    public function list_no_liquidation()
    {
        $advances = Advance::
                    join('vouchers','advances.voucher','=','vouchers.dv_num')
                    ->where('status','PENDING')
                    ->select('advances.voucher','vouchers.total')
                    ->get();

        return $advances;
    }
    public function list_existing()
    {
        $advances = Advance::
                    join('advance_particulars','advances.id','=','advance_particulars.advance')
                    ->select('advances.voucher')
                    ->distinct()
                    ->get();

        return $advances;
    }

    //update advances table then add items to advance_particulars table
    public function liquidate(Request $request, $dv_num)
    {
		$advance = Advance::where('voucher',$dv_num)->first();
    	$advance->true_total = $request["nudToAdvanceTotalAmountSpent"];
    	$advance->amount_reimbursed = $request["nudToAdvanceToReimburse"];
    	$advance->remarks = $request["txtToAdvanceRemarks"];
        $advance->receiver_name = $request["txtToAdvanceReceiverName"];
        $advance->receiver_position = $request["txtToAdvanceReceiverPosition"];
        $advance->status = "LIQUIDATED";
    	$updated = $advance->update();

    	for ($i = 0 ; $i < count($request["toadvance"]) ; $i++) {
    		$updated = DB::INSERT("INSERT INTO advance_particulars (advance,description,amount) VALUES (?,?,?)",[
				$advance->id,
				$request["toadvance"][$i]["description"],
				$request["toadvance"][$i]["amount"]
    		]);
    	}

    	return response()->json([
    		'saved'=>$updated,
            'dv_num'=>$dv_num,
    	]);
    }

    public function liquidation($dv_num)
    {
        $voucher = Voucher::where('dv_num',$dv_num)->first();
        $advance = Advance::where('voucher',$dv_num)->first();
        $particulars = AdvanceParticular::where('advance',$advance->id)->get();

        return view('reports.liquidation')->with([
            'voucher'=>$voucher,
            'advance'=>$advance,
            'particulars'=>$particulars,
        ]);
        // return $particulars;
    }

    public function liquidation_existing(Request $request)
    {
        $voucher = Voucher::where('dv_num',$request["cmbExistingLiquidationDVNum"])->first();
        $advance = Advance::where('voucher',$request["cmbExistingLiquidationDVNum"])->first();
        $particulars = AdvanceParticular::where('advance',$advance->id)->get();

        return view('reports.liquidation')->with([
            'voucher'=>$voucher,
            'advance'=>$advance,
            'particulars'=>$particulars,
        ]);
        // return $particulars;
    }

    public function log ($action)
    {
        $log = new Activity;
        if (Auth::guard('web')->check()) {
            $log->email = Auth::guard('web')->user()->email;
        }
        else if (Auth::guard('admin')->check()) {
            $log->email = Auth::guard('admin')->user()->email;
        }
        $log->action = $action;
        $log->save();
    }
}
