<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Auth;

use App\Voucher;
use App\Check;
use App\LDDAP;
use App\Fund;
use App\Activity;
use App\Advance;

class VouchersController extends Controller
{
    public function index()
    {
    	return view('vouchers.add');
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $voucher = new Voucher;
        $voucher->fund_id      = $request["cmbAddVoucherFundCluster"];
        $voucher->voucher_date = $request["dtpAddVoucherDate"];
        $voucher->dv_num       = $request["DVORSval"].$request["txtAddVoucherDVNum"];
        $voucher->ors_burs_num = $request["txtAddVoucherORSBURSNum"] == null ? null : $request["DVORSval"].$request["txtAddVoucherORSBURSNum"];
        $voucher->payee        = $request["txtAddVoucherPayee"];
        $voucher->address      = $request["txtAddVoucherPayeeAddress"];
        $voucher->particular   = $request["txtAddVoucherParticular"];
        $voucher->uacs_code    = $request["txtAddVoucherUACSCode"];
        $voucher->total        = $request["txtAddVoucherAmount"];
        $saved = $voucher->save();
        $action = "Voucher save successful.";

        if ($request["rbtnAddVoucherDisbursementMode"] == "check") {
            $check = new Check;
            $check->check_number = $request["txtAddCheckCheckNumber"];
            $check->voucher = $request["DVORSval"].$request["txtAddVoucherDVNum"];
            $check->status = "ACTIVE";
            $saved = $check->save();
            $check = $request["txtAddCheckCheckNumber"];

            if ($saved == false) {
                $action = "Voucher save failed.";
                $this->log($action);
                return redirect('/vouchers')->with([
                    'check_number'  =>  $check,
                    'type'          =>  'error',
                    'status'        =>  $action,
                    'fund'          =>  Fund::find($request["cmbAddVoucherFundCluster"]),
                ]);
            }

            $this->log($action);
            return redirect('/vouchers')->with([
                'check_number'  =>  $check,
                'type'          =>  'success',
                'status'        =>  $action,
                'fund'          =>  Fund::find($request["cmbAddVoucherFundCluster"]),
            ]);
        }
        else if ($request["rbtnAddVoucherDisbursementMode"] == "lddap") {
            $lddap = new LDDAP;
            $lddap->lddap_ada_number = $request["txtAddLDDAPLDDAPADANumber"];
            $lddap->sliiae_number = $request["txtAddLDDAPSliiaeNumber"];
            $lddap->voucher = $request["DVORSval"].$request["txtAddVoucherDVNum"];
            $lddap->status = "ACTIVE";
            $saved = $lddap->save();

            if ($saved == false) {
                $action = "Voucher save failed.";
                $this->log($action);
                return redirect('/vouchers')->with([
                    'type'          =>  'error',
                    'status'        =>  $action,
                ]);
            }

            $this->log($action);
            return redirect('/vouchers')->with([
                'type'          =>  'success',
                'status'        =>  $action,
            ]);
        }
        else if ($request["rbtnAddVoucherDisbursementMode"] == "advance") {
            //save to advances table
            $advance = new Advance;
            $advance->voucher = $request["DVORSval"].$request["txtAddVoucherDVNum"];
            $advance->check_number = $request["txtAddCashAdvanceCheckNumber"];
            $advance->signatory = $request["cmbAddCashAdvanceSignatory"];
            $advance->status = "PENDING";
            $saved = $advance->save();
            //save to checks table
            $check = new Check;
            $check->check_number = $request["txtAddCashAdvanceCheckNumber"];
            $check->voucher = $request["DVORSval"].$request["txtAddVoucherDVNum"];
            $check->status = "ACTIVE";
            $saved = $check->save();

            $check = $request["txtAddCashAdvanceCheckNumber"];

            if ($saved == false) {
                $action = "Voucher save failed.";
                $this->log($action);
                return redirect('/vouchers')->with([
                    'check_number'  =>  $check,
                    'type'          =>  'error',
                    'status'        =>  $action,
                    'fund'          =>  Fund::find($request["cmbAddVoucherFundCluster"]),
                ]);
            }

            $this->log($action);
            return redirect('/vouchers')->with([
                'check_number'  =>  $check,
                'type'          =>  'success',
                'status'        =>  $action,
                'fund'          =>  Fund::find($request["cmbAddVoucherFundCluster"]),
            ]);
        }
    }

    public function show($dv_num)
    {
        $voucher = Voucher::where('dv_num',$dv_num)->first();

        return view('vouchers.view')->with([
            'voucher'=>$voucher,
        ]);
    }

    public function update(Request $request,$id)
    {
        $voucher = Voucher::find($id);
        $voucher->fund_id      = $request["cmbEditVoucherFundCluster"];
        $voucher->voucher_date = $request["dtpEditVoucherDate"];
        $voucher->dv_num       = $request["DVORSval"].$request["txtEditVoucherDVNum"];
        $voucher->ors_burs_num = $request["txtEditVoucherORSBURSNum"] == null ? null : $request["DVORSval"].$request["txtEditVoucherORSBURSNum"];
        $voucher->payee        = $request["txtEditVoucherPayee"];
        $voucher->address      = $request["txtEditVoucherPayeeAddress"];
        $voucher->particular   = $request["txtEditVoucherParticular"];
        $voucher->uacs_code    = $request["txtEditVoucherUACSCode"];
        $voucher->total        = $request["txtEditVoucherAmount"];
        $edited = $voucher->update();
        $action = "Voucher edit successful.";

        if ($edited == false) {
            $action = "Voucher save failed.";
            $this->log($action);
            return redirect('/vouchers/all')->with([
                'type'          =>  'error',
                'status'        =>  $action,
            ]);
        }

        $this->log($action);
        return redirect('/vouchers/all')->with([
            'type'          =>  'success',
            'status'        =>  $action,
        ]);
    }

    public function list_all()
    {
        $vouchers = Voucher::join('funds','vouchers.fund_id','=','funds.id')->get();

        return $vouchers;
    }
    public function list_all_index()
    {
        return view('vouchers.all');
    }

    public function log ($action)
    {
        $log = new Activity;
        if (Auth::guard('web')->check()) {
            $log->email = Auth::guard('web')->user()->email;
        }
        else if (Auth::guard('admin')->check()) {
            $log->email = Auth::guard('admin')->user()->email;
        }
        $log->action = $action;
        $log->save();
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'cmbAddVoucherFundCluster'          => 'required',
            'dtpAddVoucherDate'                 => 'required',
            'txtAddVoucherDVNum'                => 'required',
            'txtAddVoucherPayee'                => 'required|string|max:255',
            'txtAddVoucherPayeeAddress'         => 'required|string|max:255',
            'txtAddVoucherParticular'           => 'required|string|max:255',
            'txtAddVoucherUACSCode'             => 'required|string|max:10',
            'txtAddVoucherAmount'               => 'required|max:17',
            'rbtnAddVoucherDisbursementMode'    => 'required|string',
        ]);
    }
}
