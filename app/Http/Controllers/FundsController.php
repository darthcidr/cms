<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Fund;
use App\Activity;

class FundsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('funds.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fund = new Fund;
        $fund->desc = $request["txtAddFundDesc"];
        $fund->code = $request["txtAddFundCode"];
        $fund->account_number = $request["txtAddFundAccountNumber1"].'-'.$request["txtAddFundAccountNumber2"].'-'.$request["txtAddFundAccountNumber3"];
        $fund->cluster_number = $request["txtAddFundClusterNumber"];
        $fund->collectible = isset($request["chkAddFundClusterCollectible"]) ? 'true' : 'false';
        $saved = $fund->save();
        $action = 'Fund save successful.';

        if ($saved == false) {
            $action = 'Fund save failed.';
            $this->log($action);
            return redirect('/settings/funds')->with([
                'type'      =>  'error',
                'status'    =>  $action,
            ]);
        }

        $this->log($action);
        return redirect('/settings/funds')->with([
            'type'      =>  'success',
            'status'    =>  $action,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fund = Fund::find($id);

        return view('funds.view')->with([
            'fund'  =>  $fund
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fund = Fund::find($id);
        $fund->desc = $request["txtEditFundDesc"];
        $fund->account_number = $request["txtEditFundAccountNumber"];
        $fund->cluster_number = $request["txtEditFundClusterNumber"];
        $fund->collectible = isset($request["txtEditFundClusterCollectible"]) ? 'true' : 'false';
        $edited = $fund->update();
        $action = 'Fund edit successful.';

        if ($edited == false) {
            $action = 'Fund edit failed.';
            $this->log($action);
            return redirect('/settings/funds')->with([
                'type'      =>  'error',
                'status'    =>  $action,
            ]);
        }

        $this->log($action);
        return redirect('/settings/funds')->with([
            'type'      =>  'success',
            'status'    =>  $action,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fund = Fund::find($id);
        $deleted = $fund->delete();
        $action = 'Fund delete successful.';

        if ($deleted == false) {
            $action = 'Fund delete failed.';
            $this->log($action);
            return redirect('/settings/funds')->with([
                'type'      =>  'error',
                'status'    =>  $action,
            ]);
        }

        $this->log($action);
        return redirect('/settings/funds')->with([
            'type'      =>  'success',
            'status'    =>  $action,
        ]);
    }

    public function list_all()
    {
        $funds =  Fund::all();

        return $funds;
    }
    public function list_collectibles()
    {
        $funds =  Fund::where('collectible','true')->get();

        return $funds;
    }

    public function log ($action)
    {
        $log = new Activity;
        if (Auth::guard('web')->check()) {
            $log->email = Auth::guard('web')->user()->email;
        }
        else if (Auth::guard('admin')->check()) {
            $log->email = Auth::guard('admin')->user()->email;
        }
        $log->action = $action;
        $log->save();
    }
}
