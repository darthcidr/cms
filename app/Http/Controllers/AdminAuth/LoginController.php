<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Auth;

class LoginController extends Controller
{
	protected $redirectTo = '/admin/home';

    use AuthenticatesUsers;

    protected function guard()
    {
    	return Auth::guard('admin');
    }

    public function showLoginForm()
    {
    	return view('admin.auth.login');
    }

    public function username()
    {
        return 'username';
    }

    public function logout(Request $request)
    {
        $this->guard('admin')->logout();

        // $request->session()->invalidate();

        return redirect('/');
    }
}
