<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Activity;

class ActivitiesController extends Controller
{
	public function index()
	{
		return view('activities.index');
	}

    public function list_all()
    {
    	$activities = Activity::join('users','activity_log.email','=','users.email')->orderBy('activity_log.id', 'DESC')->get();

    	return $activities;
    }
}
