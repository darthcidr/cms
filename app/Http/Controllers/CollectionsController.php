<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Collection;
use App\Particular;
use App\Activity;
use App\Fund;
use App\Signatory;

class CollectionsController extends Controller
{
    public function index ()
    {
    	return view('collections.index');
    }

    public function store (Request $request) {
    	$collection = new Collection;
        $collection->payor = $request["txtAddCollectionPayor"];
        $collection->agency = $request["txtAddCollectionAgency"];
        $collection->fund = $request["cmbAddCollectionFundCluster"];
    	$collection->or_number = $request["txtAddCollectionORNumber"];
    	$collection->total = $request["nudAddCollectionTotal"];
        $collection->date = $request["txtAddCollectionDate"];
    	$collection->collecting_officer = Auth::guard('web')->user()->username;
    	$saved = $collection->save();

    	for ($i = 0 ; $i < count($request["tocollect"]) ; $i++) {
    		$particular = new Particular;
    		$particular->collection = $request["txtAddCollectionORNumber"];
    		$particular->desc = $request["tocollect"][$i]["description"];
    		$particular->amount = $request["tocollect"][$i]["amount"];
    		$saved = $particular->save();
    	}

        return response()->json([
            'saved'=>$saved,
            'or_number'=>$request["txtAddCollectionORNumber"],
        ]);
    }
    public function store_successful ($or_number)
    {
        $action = 'Collection save successful.';
        $this->log($action);
        return redirect('/collections')->with([
            'type'=>'success',
            'status'=>$action,
            'or_number'=>$or_number,
        ]);
    }
    public function store_failed ($or_number)
    {
        $action = 'Collection save failed.';
        $this->log($action);
        return redirect('/collections')->with([
            'type'=>'error',
            'status'=>$action,
            'or_number'=>$or_number,
        ]);
    }

    public function show ($or_number)
    {
        $collection = Collection::where('or_number',$or_number)->first();
        $particulars = Particular::where('collection',$or_number)->get();

        return view('collections.view')->with([
            'collection'=>$collection,
            'particulars'=>$particulars,
        ]);
    }

    public function list_all()
    {
        $collections = Collection::join('users','collections.collecting_officer','=','users.username')
                        ->select(
                            'collections.or_number',
                            'collections.total',
                            'collections.created_at',
                            'users.fname',
                            'users.mname',
                            'users.lname'
                        )->get();

        return $collections;
    }

    public function print_or($or_number)
    {
        $collection = Collection::where('or_number',$or_number)->first();
        $particulars = Particular::where('collection',$or_number)->get();

        $spellout = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
        $total_whole = floor($collection["total"]);
        $total_decimal = round($collection["total"] - $total_whole,2);

        return view('reports.af51')->with([
            'collection'        =>  $collection,
            'particulars'       =>  $particulars,
            'total_in_words'    =>  ucwords(($spellout->format($total_whole).' pesos & '.($total_decimal*100).'/100').' only'),
        ]);
    }

    public function report_collections(Request $request)
    {
        $month = explode('-',$request["txtReportCollectionReportNum"])[1];
        $collections = Collection::
                        whereMonth('created_at',$month)
                        ->where('fund',$request["cmbReportCollectionFundCluster"])
                        ->orderBy('or_number')
                        ->get();
        $particulars = Particular::
                        orderBy('collection')
                        ->get();
        $totalCollections = Collection::
                            whereMonth('created_at',$month)
                            ->where('fund',$request["cmbReportCollectionFundCluster"])
                            ->sum('total');
        $fund = Fund::where('id',$request["cmbReportCollectionFundCluster"])->first();

        $first_or = "";
        $last_or = "";
        $or_range = "";
        for($i = 0 ; $i < count($collections) ; $i++) {
            if ($i == 0 || count($collections)==1) {
                $first_or = $collections[$i]["or_number"];
                $last_or = $collections[$i]["or_number"];
            }
            else {
                $last_or = $collections[$i]["or_number"];
            }
            $or_range = trim('-' , $first_or.'-'.$last_or);
        }

        $signatory = Signatory::select('fname', 'mname', 'lname', 'position')->where('position', 'Cash Unit')->first();
        $name = "";
        if (count($signatory)>0) {
          $mi = substr($signatory["mname"], 0, 1);
          $name = $signatory["fname"] . ' ' . $mi . '. ' . $signatory["lname"];
        } else {
          $name = "Unable to retrieve name";
        }

        return view('reports.collections')->with([
            'report_number'=>$request["txtReportCollectionReportNum"],
            'collections'=>$collections,
            'totalCollections'=>$totalCollections,
            'first_or'=>$first_or,
            'last_or'=>$last_or,
            'fund'=>$fund,
            'signatory'=>$signatory,
        ]);
    }

    public function log ($action)
    {
        $log = new Activity;
        if (Auth::guard('web')->check()) {
            $log->email = Auth::guard('web')->user()->email;
        }
        else if (Auth::guard('admin')->check()) {
            $log->email = Auth::guard('admin')->user()->email;
        }
        $log->action = $action;
        $log->save();
    }
}
