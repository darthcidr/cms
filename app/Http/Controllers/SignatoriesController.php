<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Signatory;
use App\Activity;

class SignatoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('signatories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $signatory = new Signatory;
        $signatory->fname = $request["txtAddSignatoryFName"];
        $signatory->mname = $request["txtAddSignatoryMName"];
        $signatory->lname = $request["txtAddSignatoryLName"];
        $signatory->position = $request["txtAddSignatoryPosition"];
        $saved = $signatory->save();
        $action = "Signatory save successful.";

        if ($saved == false) {
            $action = "Signatory save failed.";
            $this->log($action);
            return redirect('/settings/signatories')->with([
                'type'      =>  'error',
                'status'    =>  $action,
            ]);
        }

        $this->log($action);
        return redirect('/settings/signatories')->with([
            'type'      =>  'success',
            'status'    =>  $action,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $signatory = Signatory::find($id);

        return view('signatories.view')->with([
            'signatory' =>  $signatory
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $signatory = Signatory::find($id);
        $signatory->fname = $request["txtEditSignatoryFName"];
        $signatory->mname = $request["txtEditSignatoryMName"];
        $signatory->lname = $request["txtEditSignatoryLName"];
        $signatory->position = $request["txtEditSignatoryPosition"];
        $edited = $signatory->update();
        $action = "Signatory edit successful.";

        if ($edited == false) {
            $action = "Signatory edit failed.";
            $this->log($action);
            return redirect('/settings/signatories')->with([
                'type'      =>  'error',
                'status'    =>  $action,
            ]);
        }
        
        $this->log($action);
        return redirect('/settings/signatories')->with([
            'type'      =>  'success',
            'status'    =>  $action,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $signatory = Signatory::find($id);
        $deleted = $signatory->delete();
        $action = "Signatory delete successful.";

        if ($deleted == false) {
            $action = "Signatory delete failed.";
            $this->log($action);
            return redirect('/settings/signatories')->with([
                'type'      =>  'error',
                'status'    =>  $action,
            ]);
        }

        $this->log($action);
        return redirect('/settings/signatories')->with([
            'type'      =>  'success',
            'status'    =>  $action,
        ]);
    }

    public function list_all()
    {
        $signatories = Signatory::all();

        return $signatories;
    }

    public function log ($action)
    {
        $log = new Activity;
        if (Auth::guard('web')->check()) {
            $log->email = Auth::guard('web')->user()->email;
        }
        else if (Auth::guard('admin')->check()) {
            $log->email = Auth::guard('admin')->user()->email;
        }
        $log->action = $action;
        $log->save();
    }
}
