<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Admin;

class AdminController extends Controller
{
    public function showAccountSettingsForm()
    {
        return view('admin.account-settings');
    }

    public function update(Request $request, $id)
    {
        // $image = $request->file('picture');
        // $fileName = $image->getClientOriginalName();
        // $request['picture'] = time().'.'.$image->getClientOriginalExtension();
        // $destinationPath = public_path('/images');
        // $image->move($destinationPath, $request['picture']);
        if ($request['picture'] !== null) {
            $imageName = time().'.'.$request['picture']->getClientOriginalExtension();
            request()->picture->move(public_path('/images/profile_picture'), $imageName);
        }
        else {
            $imageName = null;
        }

        $user = Admin::find($id);
        $user->fname = $request['fname'];
        $user->mname = $request['mname'];
        $user->lname = $request['lname'];
        $user->image = $imageName;
        $user->email = $request['email'];
        $user->update();

        return redirect('/admin/account/settings')->with([
            'status'    =>  'Account updated successfully.'
        ]);
    }
}
