<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DatabaseController extends Controller
{
	public function index()
	{
		return view('admin.database.index');
	}

    public function backup(Request $request)
    {
		$strBackupDB = ("mysqldump ".env('DB_DATABASE','cdrms')." -u ".env('DB_USERNAME','root')." > ".public_path()."/database_backup/".$request["txtBackupDBFileName"].".sql");

		exec($strBackupDB,$output,$return_var);

		return redirect('/settings/database')->with([
			'status'	=>	'Database backed up successfully as '.$request["txtBackupDBFileName"].'.sql',
			'type'		=>	'success',
			'file'		=>	$request["txtBackupDBFileName"].'.sql',
		]);
    }

    public function restore(Request $request)
    {
    	if ($_FILES["fileRestoreDBFile"]["tmp_name"]=="") {
			return redirect('/settings/database')->with([
			'status'	=>	'Database restore failed. PLease add a file',
			'type'		=>	'error',
		]);
		}
		
		$fileName = 'restore_db_'.time().'.'.$request['fileRestoreDBFile']->getClientOriginalExtension();
		request()->fileRestoreDBFile->move(public_path('/database_backup'), $fileName);

		$strRestoreDB = "mysql -u ".env('DB_USERNAME','root')." -h ".env('DB_HOST','127.0.0.1')." ".env('DB_DATABASE','cms')." < ".public_path('/database_backup/').$fileName;
		exec($strRestoreDB,$output,$return_var);
		return redirect('/settings/database')->with([
			'status'	=>	'Database restored successfully',
			'type'		=>	'success',
		]);
    }
}
