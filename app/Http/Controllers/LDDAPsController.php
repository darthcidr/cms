<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\LDDAP;
use App\Voucher;
use App\Fund;
use App\Activity;
use App\Signatory;


class LDDAPsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('lddap.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lddap_ada_number)
    {
        $lddap = LDDAP::join('vouchers','lddaps.voucher','=','vouchers.dv_num')
                    ->join('funds','vouchers.fund_id','=','funds.id')
                    ->where('lddap_ada_number',$lddap_ada_number)->first();

        $lddap = LDDAP::where('lddap_ada_number',$lddap_ada_number)->first();
        $voucher = Voucher::where('dv_num',$lddap->voucher)->first();

        return view('lddap.view')->with([
            'voucher'=>$voucher,
            'lddap'=>$lddap,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lddap = LDDAP::find($id);
        $lddap->lddap_ada_number = $request["txtEditLDDAPLDDAPADANumber"];
        $lddap->sliiae_number = $request["txtEditLDDAPSliiaeNumber"];
        $lddap->status = $request["cmbEditLDDAPStatus"];
        $edited = $lddap->update();
        $action = "LDDAP edit successful.";

        if ($edited == false) {
            $action = "LDDAP edit failed.";
            $this->log($action);
            return redirect('/lddap/view/'.$lddap->lddap_ada_number)->with([
                'type'      =>  'error',
                'status'    =>  $action,
            ]);
        }

        $this->log($action);
        return redirect('/lddap/view/'.$lddap->lddap_ada_number)->with([
            'type'      =>  'success',
            'status'    =>  $action,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lddap = LDDAP::find($id);
        $deleted = $lddap->delete();
        $action = "LDDAP delete successful.";

        if ($deleted == false) {
            $action = "LDDAP delete failed.";
            $this->log($action);
            return redirect('/lddap')->with([
                'type'      =>  'error',
                'status'    =>  $action,
            ]);
        }

        $this->log($action);
        return redirect('/lddap')->with([
            'type'      =>  'success',
            'status'    =>  $action,
        ]);
    }

    public function list_all()
    {
        $lddaps = LDDAP::all();

        return $lddaps;
    }
    public function list_all_index()
    {
        return view('lddap.all');
    }
    public function list_all_table()
    {
        $lddap = LDDAP::join('vouchers','lddaps.voucher','=','vouchers.dv_num')
                    ->join('funds','vouchers.fund_id','=','funds.id')
                    ->get();

        return $lddap;
    }
    //  Ferdie ends here
    public function print_summary($id)
    {
        $lddap = LDDAP::
                    join('vouchers','lddaps.voucher','=','vouchers.dv_num')
                    ->where('lddap_ada_number',$id)
                    ->first();
        $fund = Fund::where('id',$lddap->fund_id)->first();

        $spellout = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
        $total_whole = floor($lddap->total);
        $total_decimal = round($lddap->total - $total_whole,2);

        return view('reports.sliiae')->with([
            'fund'              =>  $fund->cluster_number,
            'sliiae_no'         =>  $lddap->sliiae_number,
            'date'              =>  $lddap->voucher_date,
            'lddap_ada_number'  =>  $lddap->lddap_ada_number,
            'total'             =>  $lddap->total,
            'total_in_words'    =>  ($spellout->format($total_whole).' pesos & '.($total_decimal*100).'/100').' only',
        ]);

        return view('reports.lddap')->with([
            'lddap'=>$lddap,
            'fund'=>$fund,
        ]);
    }

    public function report_LDDAP(Request $request)
    {
        $rep = $request["txtLDDAPReportNum"];
        $fund_cluster = $request["cmbLDDAPFundCluster"];
        $fund = Fund::where('id',$fund_cluster)->first();
        $rep_number = substr($rep,0,5);

        $lddap = LDDAP::join('vouchers','lddaps.voucher','=','vouchers.dv_num')
                    ->join('funds','vouchers.fund_id','=','funds.id')
                    ->where('fund_id',$fund_cluster)
                    ->where('dv_num', 'like', $rep_number.'%')
                    ->get();
        $lddap_total = LDDAP::join('vouchers','lddaps.voucher','=','vouchers.dv_num')
                        ->join('funds','vouchers.fund_id','=','funds.id')
                        ->where('fund_id',$fund_cluster)
                        ->where('dv_num','like',$rep_number.'%')
                        ->sum('total');

        $signatory = Signatory::select('fname', 'mname', 'lname', 'position')->where('position', 'Cash Unit')->first();
        $name = "";
        if (count($signatory)>0) {
          $mi = substr($signatory["mname"], 0, 1);
          $name = $signatory["fname"] . ' ' . $mi . '. ' . $signatory["lname"];
        } else {
          $name = "Unable to retrieve name";
        }

        $first_lddap = "";
        $last_lddap = "";
        for($i = 0 ; $i < count($lddap) ; $i++) {
            if ($i == 0 || count($lddap)==1) {
                $first_lddap = $lddap[$i]["lddap_ada_number"];
                $last_lddap = $lddap[$i]["lddap_ada_number"];
            }
            else {
                $last_lddap = $lddap[$i]["lddap_ada_number"];
            }
        }

        return view('reports.lddap')->with([
            'lddap'         =>  $lddap,
            'lddap_total'   =>  $lddap_total,
            'fund'          =>  $fund,
            'first_lddap'   =>  $first_lddap,
            'last_lddap'    =>  $last_lddap,
            'report_number' =>  $rep_number,
            'name'          =>  $name,
            'signatory'     =>  $signatory,
        ]);
    }

    public function report_num_option()
    {
        // $rep_num = Check::select('dv_num')->get();
        $rep_num =  LDDAP::join('vouchers','lddaps.voucher','=','vouchers.dv_num')
                    ->select('dv_num')
                    ->distinct()
                    ->get();

        return $rep_num;
    }

    public function sliiae(Request $request)
    {
        $fund               = Fund::where('id',$request["cmbGenerateSLIIAEFundCluster"])->first();
        $sliiae_no          = $request["txtGenerateSLIIAESLIIAENo"];
        $date               = $request["dtpGenerateSLIIAEDate"];
        $lddap_ada_number   = $request["txtGenerateSLIIAELDDAPADANo"];
        $total              = $request["nudGenerateSLIIAETotal"];

        $spellout = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
        $total_whole = floor($request["nudGenerateSLIIAETotal"]);
        $total_decimal = round($request["nudGenerateSLIIAETotal"] - $total_whole,2);

        return view('reports.sliiae')->with([
            'fund'              =>  $fund->cluster_number,
            'sliiae_no'         =>  $sliiae_no,
            'date'              =>  $date,
            'lddap_ada_number'  =>  $lddap_ada_number,
            'total'             =>  $total,
            'total_in_words'    =>  ($spellout->format($total_whole).' pesos & '.($total_decimal*100).'/100').' only',
        ]);
    }

    public function log ($action)
    {
        $log = new Activity;
        if (Auth::guard('web')->check()) {
            $log->email = Auth::guard('web')->user()->email;
        }
        else if (Auth::guard('admin')->check()) {
            $log->email = Auth::guard('admin')->user()->email;
        }
        $log->action = $action;
        $log->save();
    }
}
