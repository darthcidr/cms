<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    public function index()
    {
        return view('admin.users.index');
    }

    public function showAccountSettingsForm()
    {
        return view('account-settings');
    }

    public function store(Request $request)
    {
        $user = new User;
        $user->fname        = $request['fname'];
        $user->mname        = $request['mname'];
        $user->lname        = $request['lname'];
        $user->email        = $request['email'];
        $user->username     = $request['username'];
        $user->password     = bcrypt($request['password']);
        $user->save();

        return redirect('/users/cash-unit')->with([
            'type'   =>  'success',
            'status' =>  'Account created successfully.',
        ]);
    }

    public function show($id)
    {
        $user = User::find($id);

        return view('admin.users.view')->with([
            'user'=>$user,
        ]);
    }

    public function update(Request $request, $id)
    {
        // $image = $request->file('picture');
        // $fileName = $image->getClientOriginalName();
        // $request['picture'] = time().'.'.$image->getClientOriginalExtension();
        // $destinationPath = public_path('/images');
        // $image->move($destinationPath, $request['picture']);
        if ($request['picture'] !== null) {
            $imageName = time().'.'.$request['picture']->getClientOriginalExtension();
            request()->picture->move(public_path('/images/profile_picture'), $imageName);
        }
        else {
            $imageName = null;
        }

        $user = User::find($id);
        $user->fname = $request['fname'];
        $user->lname = $request['lname'];
        $user->image = $imageName;
        $user->email = $request['email'];
        $user->username = $request['username'];
        $user->update();

        return redirect('/account/settings')->with([
            'status'    =>  'Account updated successfully.'
        ]);
    }

    public function updateAsAdmin(Request $request, $id)
    {
        // $image = $request->file('picture');
        // $fileName = $image->getClientOriginalName();
        // $request['picture'] = time().'.'.$image->getClientOriginalExtension();
        // $destinationPath = public_path('/images');
        // $image->move($destinationPath, $request['picture']);
        if ($request['picture'] !== null) {
            $imageName = time().'.'.$request['picture']->getClientOriginalExtension();
            request()->picture->move(public_path('/images/profile_picture'), $imageName);
        }
        else {
            $imageName = null;
        }

        $user = User::find($id);
        $user->fname = $request['fname'];
        $user->lname = $request['lname'];
        $user->image = $imageName;
        $user->email = $request['email'];
        $user->username = $request['username'];
        $user->update();

        return redirect('/users/cash-unit')->with([
            'type'      =>  'success',
            'status'    =>  'Account updated successfully.',
        ]);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('/users/cash-unit')->with([
            'type'      =>  'success',
            'status'    =>  'Account deleted successfully.',
        ]);
    }

    public function list_all()
    {
        $users = User::all();

        return $users;
    }

    public function log ($action)
    {
        $log = new Activity;
        if (Auth::guard('web')->check()) {
            $log->email = Auth::guard('web')->user()->email;
        }
        else if (Auth::guard('admin')->check()) {
            $log->email = Auth::guard('admin')->user()->email;
        }
        $log->action = $action;
        $log->save();
    }
}
