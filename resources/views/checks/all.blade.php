@section('page-name')
    Checks | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "stretched row">
        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">List of all checks</h4>

                <table id = "tblListOfAllChecks" class = "ui small celled striped table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Check number</th>
                            <th>Fund cluster</th>
                            <th>Payee</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type = "text/javascript">
        $('#pgChecks, #pgAllChecks').toggleClass('active',true);
    </script>
@endsection
