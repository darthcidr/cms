@section('page-name')
    Check #{{ $check->check_number }} | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "stretched row">
        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Voucher information</h4>

                <div class = "ui small equal width form">
                    <div class = "fields">
                        <div class = "field">
                            <label>Fund cluster</label>
                            <select id = "cmbEditCheckFundCluster" name = "cmbEditCheckFundCluster" class = "ui small fluid search dropdown" readonly>

                            </select>
                        </div>

                        <div class = "field">
                            <label>Date</label>
                            <input type = "date" id = "dtpEditCheckDate" name = "dtpEditCheckDate" value = "{{ $check->voucher_date }}" readonly>
                        </div>

                        <div class = "field">
                            <label>DV no.</label>
                            <input type = "text" id = "txtEditCheckDVNum" name = "txtEditCheckDVNum" value = "{{ $check->dv_num }}" readonly>
                        </div>

                        <div class = "field">
                            <label>ORS/BURS no.</label>
                            <input type = "text" id = "txtEditCheckORSBURSNum" name = "txtEditCheckORSBURSNum" value = "{{ $check->ors_burs_num }}" readonly>
                        </div>
                    </div>

                    <div class = "fields">
                        <div class = "field">
                            <label>Payee</label>
                            <input type = "text" id = "txtEditCheckPayee" name = "txtEditCheckPayee" value = "{{ $check->payee }}" readonly>
                        </div>

                        <div class = "field">
                            <label>Address</label>
                            <textarea id = "txtEditCheckPayeeAddress" name = "txtEditCheckPayeeAddress" rows="3" readonly>{{ $check->address }}</textarea>
                        </div>

                        <div class = "field">
                            <label>Particular</label>
                            <textarea id = "txtEditCheckParticular" name = "txtEditCheckParticular" rows="3" readonly>{{ $check->particular }}</textarea>
                        </div>
                    </div>

                    <div class = "fields">
                        <div class = "field">
                            <label>UACS code</label>
                            <input type = "text" id = "txtEditCheckUACSCode" name = "txtEditCheckUACSCode" value = "{{ $check->uacs_code }}" data-mask = "9999999999" readonly>
                        </div>

                        <div class = "field">
                            <label>Check number</label>
                            <input type = "text" id = "txtEditCheckCheckNumber" name = "txtEditCheckCheckNumber" value = "{{ $check->check_number }}" data-mask = "9999999999" data-mask-reverse = "true" readonly>
                        </div>

                        <div class = "field">
                            <label>Amount</label>
                            <input type = "number" id = "txtEditCheckAmount" name = "txtEditCheckAmount" value = "{{ $check->total }}" min="0" step = "0.01" readonly>
                        </div>
                    </div>
                </div>
            </div>

            <div class = "ui small text segment">
                <h4 class = "ui header">Edit check</h4>

                <form id = "frmEditCheck" class = "ui small equal width form" action = "{{ route('checks.update',$check->check_number) }}" method = "POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class = "fields">
                        <div class = "field">
                                <label>Check number</label>
                                <input type = "text" id = "txtEditCheckCheckNumber" name = "txtEditCheckCheckNumber" value = "{{ $check->check_number }}" data-mask = "9999999999" data-mask-reverse = "true" readonly required>
                            </div>

                        <div class = "field">
                            <label>Status</label>
                            <select id = "cmbEditCheckStatus" name = "cmbEditCheckStatus" class = "ui small fluid search dropdown" required>
                                <option value = "ACTIVE">ACTIVE</option>
                                <option value = "CANCELLED">CANCELLED</option>
                                <option value = "STALE">STALE</option>
                            </select>
                        </div>
                    </div>

                    <a href = "/checks/all" role = "button" class = "ui small button">
                        <i class = "left arrow icon"></i>
                        Back
                    </a>
                    <button type = "submit" id = "btnEditCheck" class = "ui small positive button">
                        <i class = "send icon"></i>
                        Update
                    </button>
                </form>
            </div>
        </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgChecks').toggleClass('active',true);

        $('#cmbAddCheckFundCluster').val('{{ $check->fund_id }}');
        $('#cmbEditCheckStatus').val('{{ $check->status }}');
    </script>
@endsection
