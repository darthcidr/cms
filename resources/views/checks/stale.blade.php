@section('page-name')
    Checks | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "stretched row">
        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">List of stale checks</h4>

                <table id = "tblListOfStaleChecks" class = "ui small celled striped table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Check number</th>
                            <th>Fund cluster</th>
                            <th>Payee</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <form id = "frmUpdateStaleChecks" method = "POST" hidden>
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <input type = "text" id = "txtEditCheckCheckNumber" name = "txtEditCheckCheckNumber" required>
        <input type = "text" id = "cmbEditCheckStatus" name = "cmbEditCheckStatus" required>
    </form>
@endsection

@section('scripts')
    <script type = "text/javascript">
        $('#pgChecks, #pgStaleCheck').toggleClass('active',true);
        // console.log(arrayStaleChecks);
        // if (arrayStaleChecks.length > 0) {
        //     for(var i = 0 ; i < arrayStaleChecks.length ; i++){
        //         $('#frmUpdateStaleChecks').attr('action','/checks/update/'+arrayStaleChecks[i]);
        //         $('#txtEditCheckCheckNumber').val(arrayStaleChecks[i]);
        //         $('#cmbEditCheckStatus').val('STALE');
        //         $('#frmUpdateStaleChecks').submit();
        //     };
        // }
    </script>
@endsection
