@section('page-name')
    Disbursement Voucher #{{ $voucher->dv_num }} | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "stretched row">
        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Edit voucher #{{ $voucher->dv_num }}</h4>

                <form id = "frmEditVoucher" class = "ui small form" action = "{{ route('vouchers.update',$voucher->id) }}" method = "POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class = "fields">
                        <div class = "seven wide field">
                            <label>Fund cluster</label>
                            <select id = "cmbEditVoucherFundCluster" name = "cmbEditVoucherFundCluster" class = "ui small fluid search dropdown" required>

                            </select>
                        </div>

                        <div class = "three wide field">
                            <label>Date</label>
                            <input type = "date" id = "dtpEditVoucherDate" name = "dtpEditVoucherDate" value = "{{ $voucher->voucher_date }}" required>
                        </div>

                        <div class = "three wide field">
                            <label>DV no.</label>
                            <div class = "ui labeled input">
                                <div class = "ui label" id="DVprefix" name= "DVprefix">{{ substr($voucher->dv_num,0,6) }}</div>
                                <input type = "text" id = "txtEditVoucherDVNum" name = "txtEditVoucherDVNum" data-mask = "999" value = "{{ substr($voucher->dv_num,6) }}" required>
                            </div>
                        </div>
                        <input id="DVORSval" name="DVORSval" hidden/>
                        <div class = "three wide field">
                            <label>ORS/BURS no.</label>
                            <div class = "ui labeled input">
                                <div class = "ui label" id="ORSprefix" name= "ORSprefix">{{ substr($voucher->ors_burs_num,0,6) }}</div>
                                <input type = "text" id = "txtEditVoucherORSBURSNum" name = "txtEditVoucherORSBURSNum" data-mask = "999" value = "{{ substr($voucher->ors_burs_num,6) }}">
                            </div>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Payee</label>
                        <input type = "text" id = "txtEditVoucherPayee" name = "txtEditVoucherPayee" value = "{{ $voucher->payee }}" required>
                    </div>

                    <div class = "fields">
                        <div class = "eight wide field">
                            <label>Address</label>
                            <textarea id = "txtEditVoucherPayeeAddress" name = "txtEditVoucherPayeeAddress" rows="3" required>{{ $voucher->address }}</textarea>
                        </div>

                        <div class = "eight wide field">
                            <label>Particular</label>
                            <textarea id = "txtEditVoucherParticular" name = "txtEditVoucherParticular" rows="3" required>{{ $voucher->particular }}</textarea>
                        </div>
                    </div>

                    <div class = "fields">
                        <div class = "eight wide field">
                            <label>UACS code</label>
                            <input type = "text" id = "txtEditVoucherUACSCode" name = "txtEditVoucherUACSCode" data-mask = "9999999999" value = "{{ $voucher->uacs_code }}" required>
                        </div>

                        <div class = "eight wide field">
                            <label>Amount</label>
                            <input type = "number" id = "txtEditVoucherAmount" name = "txtEditVoucherAmount" min="0" step = "0.01" value = "{{ $voucher->total }}" required>
                        </div>
                    </div>

                    <a href = "/vouchers/all" id = "btnEditVoucher" class = "ui small button">
                        <i class = "left arrow icon"></i>
                        Back
                    </a>

                    <button type = "submit" id = "btnEditVoucher" class = "ui small positive button">
                        <i class = "send icon"></i>
                        Submit
                    </button>

                    <button type = "reset" class = "ui small negative button">
                        <i class = "refresh icon"></i>
                        Reset fields
                    </button>
                </form>
            </div>
        </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgVouchers, #pgEditVoucher').toggleClass('active',true);

        $('#dtpEditVoucherDate').change(function(){
            if ($('#dtpEditVoucherDate').val()) {
                // console.log("there is a date");
                var date = $(this).val();
                var ndate = date.substring(2,8);
                $('#DVprefix').html(ndate);
                $('#ORSprefix').html(ndate);
                $('#DVORSval').val(ndate);
            }
            else {
                $('#DVprefix').html("");
                $('#ORSprefix').html("");
                $('#DVORSval').val("");
            }
        });
    </script>
@endsection
