@section('page-name')
    Vouchers | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "stretched row">
        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Add voucher</h4>

                <form id = "frmAddVoucher" class = "ui small equal width form" action = "{{ route('vouchers.store') }}" method = "POST">
                    {{ csrf_field() }}

                    <div class = "inline fields">
                        <label>Select type of payment</label>
                        <div class = "three wide field">
                            <div class = "ui radio checkbox">
                                <input type = "radio" id = "rbtnInitialSalary" name = "rbtnAddVoucherPaymentType" value = "initialsalary">
                                <label>Payment of Initial Salary</label>
                            </div>
                        </div>

                        <div class = "three wide field">
                            <div class = "ui radio checkbox">
                                <input type = "radio" id = "rbtnTravelTraining" name = "rbtnAddVoucherPaymentType" value = "traveltraining">
                                <label>Payment for Travel/Training</label>
                            </div>
                        </div>

                        <div class = "three wide field">
                            <div class = "ui radio checkbox">
                                <input type = "radio" id = "rbtnUtilities" name = "rbtnAddVoucherPaymentType" value = "utilities">
                                <label>Utilities (e.g. LUECO)</label>
                            </div>
                        </div>
                    </div>

                    <div id = "docsInitialSalary" class = "docs grouped fields">
                        <label>Supporting document/s:</label>
                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkInitialSalaryApprovedAppointment" name = "chkInitialSalaryApprovedAppointment">
                                <label>Approved Appointment</label>
                            </div>
                        </div>

                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkInitialSalaryOathOfOffice" name = "chkInitialSalaryOathOfOffice">
                                <label>Oath of Office</label>
                            </div>
                        </div>

                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkInitialSalaryDTR" name = "chkInitialSalaryDTR">
                                <label>Daily Time Record for the Period Covered</label>
                            </div>
                        </div>

                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkInitialSalarySALN" name = "chkInitialSalarySALN">
                                <label>Statement of Assets, Liabilities, and Net Worth (SALN)</label>
                            </div>
                        </div>

                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkInitialSalaryFirstDayServiceCertificate" name = "chkInitialSalaryFirstDayServiceCertificate">
                                <label>Certification of First Day of Service</label>
                            </div>
                        </div>

                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkInitialSalaryClearance" name = "chkInitialSalaryClearance">
                                <label>Clearance (if transferred from other office)</label>
                            </div>
                        </div>
                    </div>

                    <div id = "docsTravelTraining" class = "docs grouped fields">
                        <label>Supporting document/s:</label>
                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkTravelTrainingAuthorityToTravel" name = "chkTravelTrainingAuthorityToTravel">
                                <label>Authority to Travel</label>
                            </div>
                        </div>

                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkTravelTrainingItinerary" name = "chkTravelTrainingItinerary">
                                <label>Itinerary of Travel</label>
                            </div>
                        </div>

                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkTravelTrainingParticipationCertificate" name = "chkTravelTrainingParticipationCertificate">
                                <label>Certificate of Participation & Appearance</label>
                            </div>
                        </div>

                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkTravelTrainingTravelCompletedCertificate" name = "chkTravelTrainingTravelCompletedCertificate">
                                <label>Certificate of Travel Completed</label>
                            </div>
                        </div>

                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkTravelTrainingTicket" name = "chkTravelTrainingTicket">
                                <label>Bus/Plane Ticket</label>
                            </div>
                        </div>

                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkTravelTrainingNarrativeReport" name = "chkTravelTrainingNarrativeReport">
                                <label>Narrative Report</label>
                            </div>
                        </div>

                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkTravelTrainingRegistrationOR" name = "chkTravelTrainingRegistrationOR">
                                <label>Official Receipt of Paid Registration Fee</label>
                            </div>
                        </div>

                        <div class = "field">
                            <div class = "ui checkbox">
                                <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkTravelTrainingReimbursementReceipt" name = "chkTravelTrainingReimbursementReceipt">
                                <label>Reimbursement Receipt</label>
                            </div>
                        </div>
                    </div>

                    <div id = "docsUtilities" class = "docs field">
                        <label>Supporting document/s:</label>
                        <div class = "ui checkbox">
                            <input type = "checkbox" oninvalid = "this.setCustomValidity('Please submit this supporting document')" onchange = "this.setCustomValidity('')" id = "chkUtilitiesBillingStatement" name = "chkUtilitiesBillingStatement">
                            <label>Statement of Account & Billing</label>
                        </div>
                    </div>

                    <div class="inline fields">
                        <label>Select mode of disbursement:</label>
                        <div class = "three wide field">
                            <div class = "ui radio checkbox">
                                <input type = "radio" id = "modeCheck" name = "rbtnAddVoucherDisbursementMode" value = "check" checked>
                                <label>Check</label>
                            </div>
                        </div>

                        <div class = "three wide field">
                            <div class = "ui radio checkbox">
                                <input type = "radio" id = "modeLDDAP" name = "rbtnAddVoucherDisbursementMode" value = "lddap">
                                <label>LDDAP</label>
                            </div>
                        </div>

                        <div class = "three wide field">
                            <div class = "ui radio checkbox">
                                <input type = "radio" id = "modeCashAdvance" name = "rbtnAddVoucherDisbursementMode" value = "advance">
                                <label>Cash advance</label>
                            </div>
                        </div>
                    </div>

                    <div id = "fldGenerateCheck" class = "five wide field">
                        <label>Check number</label>
                        <input type = "text" id = "txtAddCheckCheckNumber" name = "txtAddCheckCheckNumber" data-mask = "9999999999" data-mask-reverse = "true" required>
                    </div>

                    <div id = fldGenerateLDDAP class = "fields">
                        <div class = "five wide field">
                            <label>LDDAP-ADA Number</label>
                            <input type = "text" id = "txtAddLDDAPLDDAPADANumber" name = "txtAddLDDAPLDDAPADANumber" data-mask = "9 99999999-99-999-9999">
                        </div>

                        <div class = "five wide field">
                            <label>SLIIAE No.</label>
                            <input type = "text" id = "txtAddLDDAPSliiaeNumber" name = "txtAddLDDAPSliiaeNumber" data-mask = "999">
                        </div>
                    </div>

                    <div id = "fldGenerateCashAdvance" class = "fields">
                        <div class = "five wide field">
                            <label>Check number</label>
                            <input type = "text" id = "txtAddCashAdvanceCheckNumber" name = "txtAddCashAdvanceCheckNumber" data-mask = "9999999999">
                        </div>

                        <div class = "five wide field">
                            <label>Signatory</label>
                            <select id = "cmbAddCashAdvanceSignatory" name = "cmbAddCashAdvanceSignatory" class = "ui small search dropdown">

                            </select>
                        </div>
                    </div>

                    <div class = "fields">
                        <div class = "seven wide field">
                            <label>Fund cluster</label>
                            <select id = "cmbAddVoucherFundCluster" name = "cmbAddVoucherFundCluster" class = "ui small fluid search dropdown" required>

                            </select>
                        </div>

                        <div class = "three wide field">
                            <label>Date</label>
                            <input type = "date" id = "dtpAddVoucherDate" name = "dtpAddVoucherDate" required>
                        </div>
                    </div>

                    <div class = "fields">
                        <div class = "three wide field">
                            <label>DV no.</label>
                            <div class = "ui labeled input">
                                <div class = "ui label" id="DVprefix" name= "DVprefix"></div>
                                <input type = "text" id = "txtAddVoucherDVNum" name = "txtAddVoucherDVNum" data-mask = "999" required>
                            </div>
                        </div>
                        <input id="DVORSval" name="DVORSval" hidden/>
                        <div class = "three wide field">
                            <label>ORS/BURS no.</label>
                            <div class = "ui labeled input">
                                <div class = "ui label" id="ORSprefix" name= "ORSprefix"></div>
                                <input type = "text" id = "txtAddVoucherORSBURSNum" name = "txtAddVoucherORSBURSNum" data-mask = "999">
                            </div>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Payee</label>
                        <input type = "text" id = "txtAddVoucherPayee" name = "txtAddVoucherPayee" required>
                    </div>

                    <div class = "fields">
                        <div class = "eight wide field">
                            <label>Address</label>
                            <textarea id = "txtAddVoucherPayeeAddress" name = "txtAddVoucherPayeeAddress" rows="3" required></textarea>
                        </div>

                        <div class = "eight wide field">
                            <label>Particular</label>
                            <textarea id = "txtAddVoucherParticular" name = "txtAddVoucherParticular" rows="3" required></textarea>
                        </div>
                    </div>

                    <div class = "fields">
                        <div class = "eight wide field">
                            <label>UACS code</label>
                            <input type = "text" id = "txtAddVoucherUACSCode" name = "txtAddVoucherUACSCode" data-mask = "9999999999" required>
                        </div>

                        <div class = "eight wide field">
                            <label>Amount</label>
                            <input type = "number" id = "txtAddVoucherAmount" name = "txtAddVoucherAmount" min="0" step = "0.01" required>
                        </div>
                    </div>

                    <div class = "field" style = "text-align: center;">
                        <button type = "submit" id = "btnAddVoucher" class = "ui small positive button">
                            <i class = "send icon"></i>
                            Submit
                        </button>

                        <button type = "reset" class = "ui small negative button">
                            <i class = "refresh icon"></i>
                            Reset fields
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif
    @if(session()->has('check_number'))
        <script type = "text/javascript">
            window.open('/checks/print/signed/{{ session('check_number') }}');
            window.open('/checks/print/unsigned/{{ session('check_number') }}');
            window.open('/checks/advice/{{ session('check_number') }}');
        </script>
    @endif
    @if(session()->has('fund'))
        @if(session('fund') === 'COE' || session('fund') === 'RGTL')
            <script type = "text/javascript">
                window.open('/checks/advice/{{ session('check_number') }}');
            </script>
        @endif
    @endif

    @if($errors->has('cmbAddVoucherFundCluster'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('cmbAddVoucherFundCluster') }}');
        </script>
    @endif
    @if($errors->has('dtpAddVoucherDate'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('dtpAddVoucherDate') }}');
        </script>
    @endif
    @if($errors->has('txtAddVoucherDVNum'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('txtAddVoucherDVNum') }}');
        </script>
    @endif
    @if($errors->has('txtAddVoucherPayee'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('txtAddVoucherPayee') }}');
        </script>
    @endif
    @if($errors->has('txtAddVoucherPayeeAddress'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('txtAddVoucherPayeeAddress') }}');
        </script>
    @endif
    @if($errors->has('txtAddVoucherParticular'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('txtAddVoucherParticular') }}');
        </script>
    @endif
    @if($errors->has('txtAddVoucherUACSCode'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('txtAddVoucherUACSCode') }}');
        </script>
    @endif
    @if($errors->has('txtAddVoucherAmount'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('txtAddVoucherAmount') }}');
        </script>
    @endif
    @if($errors->has('rbtnAddVoucherDisbursementMode'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('rbtnAddVoucherDisbursementMode') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgVouchers, #pgAddVoucher').toggleClass('active',true);

        $('#dtpAddVoucherDate').change(function(){
            if ($('#dtpAddVoucherDate').val()) {
                // console.log("there is a date");
                var date = $(this).val();
                var ndate = date.substring(2,8);
                $('#DVprefix').html(ndate);
                $('#ORSprefix').html(ndate);
                $('#DVORSval').val(ndate);
            }
            else {
                $('#DVprefix').html("");
                $('#ORSprefix').html("");
                $('#DVORSval').val("");
            }
        });

        $('#docsInitialSalary, #docsTravelTraining, #docsUtilities').hide();
        $(document).on('change','#rbtnInitialSalary',function(event){
            event.preventDefault();
            if (this.checked) {
                $('.docs').hide();
                $('#docsInitialSalary').show();

                var docsInitialSalary = $('#docsInitialSalary').find('input');
                $.each(docsInitialSalary,function(i,element){
                    $(element).attr('required','required');
                    $(element).attr('checked','checked');
                });
                var docsTravelTraining = $('#docsTravelTraining').find('input');
                $.each(docsTravelTraining,function(i,element){
                    $(element).removeAttr('required');
                    $(element).removeAttr('checked');
                });
                var docsUtilities = $('#docsUtilities').find('input');
                $.each(docsUtilities,function(i,element){
                    $(element).removeAttr('required');
                    $(element).removeAttr('checked');
                });
            }
        });
        $(document).on('change','#rbtnTravelTraining',function(event){
            event.preventDefault();
            if (this.checked) {
                $('.docs').hide();
                $('#docsTravelTraining').show();

                var docsTravelTraining = $('#docsTravelTraining').find('input');
                $.each(docsTravelTraining,function(i,element){
                    $(element).attr('required','required');
                    $(element).attr('checked','checked');
                });
                var docsInitialSalary = $('#docsInitialSalary').find('input');
                $.each(docsInitialSalary,function(i,element){
                    $(element).removeAttr('required');
                    $(element).removeAttr('checked');
                });
                var docsUtilities = $('#docsUtilities').find('input');
                $.each(docsUtilities,function(i,element){
                    $(element).removeAttr('required');
                    $(element).removeAttr('checked');
                });
            }
        });
        $(document).on('change','#rbtnUtilities',function(event){
            event.preventDefault();
            if (this.checked) {
                $('.docs').hide();
                $('#docsUtilities').show();

                var docsUtilities = $('#docsUtilities').find('input');
                $.each(docsUtilities,function(i,element){
                    $(element).attr('required','required');
                    $(element).attr('checked','checked');
                });
                var docsInitialSalary = $('#docsInitialSalary').find('input');
                $.each(docsInitialSalary,function(i,element){
                    $(element).removeAttr('required');
                    $(element).removeAttr('checked');
                });
                var docsTravelTraining = $('#docsTravelTraining').find('input');
                $.each(docsTravelTraining,function(i,element){
                    $(element).removeAttr('required');
                    $(element).removeAttr('checked');
                });
            }
        });

        $('#fldGenerateLDDAP').hide();
        $('#fldGenerateCashAdvance').hide();
        $(document).on('change','#modeCheck',function(event){
            event.preventDefault();
            if(this.checked){
                $('#fldGenerateLDDAP').hide();
                $('#fldGenerateCashAdvance').hide();
                $('#fldGenerateCheck').show();

                $('#txtAddVoucherPayee, #txtAddVoucherPayeeAddress, #txtAddVoucherParticular').removeAttr('readonly');
                // $('#txtAddVoucherPayee, #txtAddVoucherPayeeAddress, #txtAddVoucherParticular').val('');

                $('#txtAddCheckCheckNumber').attr('required','required');
                $('#txtAddLDDAPLDDAPADANumber').removeAttr('required');
                $('#txtAddLDDAPSliiaeNumber').removeAttr('required');
                $('#txtAddCashAdvanceCheckNumber').removeAttr('required');
            }
        });
        $(document).on('change','#modeLDDAP',function(event){
            event.preventDefault();
            if(this.checked){
                $('#fldGenerateCheck').hide();
                $('#fldGenerateCashAdvance').hide();
                $('#fldGenerateLDDAP').show();

                $('#txtAddVoucherPayee, #txtAddVoucherPayeeAddress, #txtAddVoucherParticular').removeAttr('readonly');
                // $('#txtAddVoucherPayee, #txtAddVoucherPayeeAddress, #txtAddVoucherParticular').val('');

                $('#txtAddLDDAPLDDAPADANumber').attr('required','required');
                $('#txtAddLDDAPSliiaeNumber').attr('required','required');
                $('#txtAddCheckCheckNumber').removeAttr('required','required');
                $('#txtAddCashAdvanceCheckNumber').removeAttr('required','required');
            }
        });
        $(document).on('change','#modeCashAdvance',function(event){
            event.preventDefault();
            if(this.checked){
                $('#fldGenerateCheck').hide();
                $('#fldGenerateLDDAP').hide();
                $('#fldGenerateCashAdvance').show();

                $('#txtAddVoucherPayee, #txtAddVoucherPayeeAddress, #txtAddVoucherParticular').attr('readonly','readonly');
                $('#txtAddVoucherPayee, #txtAddVoucherPayeeAddress, #txtAddVoucherParticular').val('');

                $('#txtAddCashAdvanceCheckNumber').attr('required','required');
                $('#txtAddCheckCheckNumber').removeAttr('required');
                $('#txtAddLDDAPLDDAPADANumber').removeAttr('required');
                $('#txtAddLDDAPSliiaeNumber').removeAttr('required');

                $('#txtAddVoucherPayeeAddress').val('DepEd RO1, SFLU');
                $('#txtAddVoucherPayee').val($('#cmbAddCashAdvanceSignatory option:first').data('nameonly'));
                $(document).on('keyup','#txtAddCashAdvanceCheckNumber, #dtpAddVoucherDate',function(event){
                    event.preventDefault();
                    $('#txtAddVoucherParticular').val(
                        'To Liquidate Cash Advance under check #'+$('#txtAddCashAdvanceCheckNumber').val()+' dtd. '+$.format.date(new Date($('#dtpAddVoucherDate').val()),'MMMM d, yyyy')+' ('+$('#cmbAddVoucherFundCluster option:selected').data('code')+')'
                    );
                });
                $(document).on('change','#cmbAddVoucherFundCluster',function(event){
                    event.preventDefault();
                    $('#txtAddVoucherParticular').val(
                        'To Liquidate Cash Advance under check #'+$('#txtAddCashAdvanceCheckNumber').val()+' dtd. '+$.format.date(new Date($('#dtpAddVoucherDate').val()),'MMMM d, yyyy')+' ('+$('#cmbAddVoucherFundCluster option:selected').data('code')+')'
                    );
                });
                $(document).on('change','#cmbAddCashAdvanceSignatory',function(event){
                    event.preventDefault();
                    $('#txtAddVoucherPayee').val($('#cmbAddCashAdvanceSignatory option:selected').data('nameonly'));
                });
            }
        });
    </script>
@endsection
