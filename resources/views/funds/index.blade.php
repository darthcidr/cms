@section('page-name')
    System Settings - Funds | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "seven wide column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Add fund source</h4>

                <form id = "frmAddFund" class = "ui small equal width form" method = "POST" action = "{{ route('funds.store') }}">
                    {{ csrf_field() }}

                    <div class = "fields">
                        <div class = "field">
                            <label>Description</label>
                            <input type = "text" id = "txtAddFundDesc" name = "txtAddFundDesc" required autofocus>
                        </div>

                        <div class = "field">
                            <label>Code</label>
                            <input type = "text" id = "txtAddFundCode" name = "txtAddFundCode" required autofocus>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Account number</label>
                        <div class = "fields">
                            <div class = "field">
                                <input type = "text" id = "txtAddFundAccountNumber1" name = "txtAddFundAccountNumber1" data-mask = "9999" required>
                            </div>
                            <div class = "field">
                                <input type = "text" id = "txtAddFundAccountNumber2" name = "txtAddFundAccountNumber2" data-mask = "99999" required>
                            </div>
                            <div class = "five wide field">
                                <input type = "text" id = "txtAddFundAccountNumber3" name = "txtAddFundAccountNumber3" data-mask = "99" required>
                            </div>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Cluster number</label>
                        <input type = "text" id = "txtAddFundClusterNumber" name = "txtAddFundClusterNumber" data-mask = "99999999" required>
                    </div>

                    <div class = "field">
                        <div class = "ui checkbox">
                            <input type = "checkbox" id = "chkAddFundClusterCollectible" name = "chkAddFundClusterCollectible">
                            <label>Collectible? (applies to Collections module)</label>
                        </div>
                    </div>

                    <button type = "submit" id = "btnAddFund" class = "ui small positive button">
                        <i class = "send icon"></i>
                        Submit
                    </button>
                </form>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">List of fund sources</h4>

                <table id = "tblListOfFunds" class = "ui small celled striped table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Description</th>
                            <th>Account number</th>
                            <th>Cluster #</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgSettingsFunds').toggleClass('active',true);
    </script>
@endsection
