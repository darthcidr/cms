@section('page-name')
    Funds - {{ $fund->desc }} | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "seven wide column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Edit fund source</h4>

                <form id = "frmEditFund" class = "ui small equal width form" method = "POST" action = "{{ route('funds.update', $fund->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class = "fields">
                        <div class = "field">
                            <label>Description</label>
                            <input type = "text" id = "txtEditFundDesc" name = "txtEditFundDesc" value = "{{ $fund->desc }}" required autofocus>
                        </div>

                        <div class = "field">
                            <label>Code</label>
                            <input type = "text" id = "txtEditFundCode" name = "txtEditFundCode" value = "{{ $fund->code }}" required>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Account number</label>
                        <div class = "fields">
                            <div class = "field">
                                <input type = "text" id = "txtEditFundAccountNumber" name = "txtEditFundAccountNumber" value = "{{ $fund->account_number }}" required>
                            </div>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Cluster number</label>
                        <input type = "text" id = "txtEditFundClusterNumber" name = "txtEditFundClusterNumber" value = "{{ $fund->cluster_number }}" data-mask = "99999999" required>
                    </div>

                    <div class = "field">
                        <div class = "ui checkbox">
                            @if($fund->collectible == 'true')
                                <input type = "checkbox" id = "txtEditFundClusterCollectible" name = "txtEditFundClusterCollectible" checked required>
                            @elseif($fund->collectible == 'false')
                                <input type = "checkbox" id = "txtEditFundClusterCollectible" name = "txtEditFundClusterCollectible" required>
                            @endif
                            <label>Collectible? (applies to Collections module)</label>
                        </div>
                    </div>

                    <div class = "ui three small buttons">
                        <a href = "{{ route('funds.index') }}" role = "button" class = "ui small button">
                            <i class = "left arrow icon"></i>
                            Back
                        </a>
                        <button type = "submit" id = "btnEditFund" class = "ui small positive button">
                            <i class = "send icon"></i>
                            Submit
                        </button>
                        <button type = "button" id = "btnDeleteFund" class = "ui small negative button">
                            <i class = "trash icon"></i>
                            Delete
                        </button>
                    </div>
                </form>

                <form id = "frmDeleteFund" method = "POST" action = "{{ route('funds.destroy', $fund->id) }}" hidden>
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>
            </div>
        </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgSettingsFunds').toggleClass('active',true);

        $(document).on('click', '#btnDeleteFund', function(event){
            event.preventDefault();
            if (confirm('Are you sure to delete fund: {{ $fund->desc }}?') == true) {
                $('#frmDeleteFund').submit();
            }
        });
    </script>
@endsection
