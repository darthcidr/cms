<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
        <title>Login | {{ config('app.name','Cannot display app name') }}</title>

        <!-- Styles -->
        <link href="{{ asset('packages/packages.css') }}" rel="stylesheet">
    </head>

    <body style = "background: linear-gradient(#0D3265 , #16478A, #3886B0, #4E99AD);">
        <div class = "ui container" style="padding-top: 5rem;">
            <div class = "ui stackable equal width grid">
                <div class = "row">
                    <div class = "center aligned column">
                        <h2 class = "ui inverted header" style = "font-family: HelvLight">{{ config('app.name','Cannot display app name') }}</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class = "ui container" style = "padding-top: 5rem;">
            <div class = "ui stackable equal width grid">
                <div class = "row">
                    <div class = "column"></div>
                    <div class = "column"></div>

                    <div class = "five wide column">
                        <img src = "{{ asset('images/deped-logo.png') }}" style="width: 15rem; height: auto;">
                    </div>

                    <div class = "five wide column">
                        <h5 class = "ui inverted header">Login</h5>

                        <form id = "frmLogin" class = "ui small inverted equal width form" method = "POST" action = "{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class = "field{{ $errors->has('username') ? ' has-error' : '' }}">
                                <label>Username:</label>
                                <input id="username" type="text" name="username" value="{{ old('username') }}" required autofocus>
                            </div>

                            <div class = "field{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label>Password:</label>
                                <input id="password" type="password" name="password" required>
                            </div>

                            <div class = "field">
                                <div class = "ui checkbox">
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label>Keep me logged in</label>
                                </div>
                            </div>

                            <div class = "field">
                                <button type = "submit" class = "ui small primary button">
                                    <i class = "sign in icon"></i>
                                    Login
                                </button>

                                <a href = "{{ route('password.request') }}" style="color: #fff">Forgot your password?</a>
                            </div>

                            <div class = "field">

                            </div>
                        </form>
                    </div>

                    <div class = "column"></div>
                    <div class = "column"></div>
                </div>
            </div>
        </div>

        <!-- Scripts -->
        <script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
        <script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>
        <script src="{{ asset('packages/toastr/build/toastr.min.js') }}"></script>
        <script src="{{ asset('packages/dataTables/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('packages/dataTables/dataTables.semanticui.js') }}"></script>
        <script src="{{ asset('packages/jquery-mask/dist/jquery.mask.js') }}"></script>
        <script src="{{ asset('js/core.js') }}"></script>
        @if(Auth::guard('admin')->check())
            <script src="{{ asset('js/admin/functions.js') }}"></script>
        @endif
        @yield('charts-js')
        <script src="{{ asset('packages/Remodal/dist/remodal.js') }}"></script>
        @if($errors->has('username'))
            <script type = "text/javascript">
                toastr.error('{{ $errors->first('username') }}');
            </script>
        @endif

        @if($errors->has('password'))
            <script type = "text/javascript">
                toastr.error('{{ $errors->first('password') }}');
            </script>
        @endif

        <script type = "text/javascript">
            $('#pgLogin').toggleClass('active',true);
        </script>
    </body>
</html>
