@section('page-name')
    Register | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "column"></div>
        <div class = "nine wide column">
            <div class = "ui small text segment">
                <h5 class = "ui header">Register</h5>

                <form id = "frmRegister" class = "ui small equal width form" method = "POST" action = "{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class = "fields">
                        <div class = "field">
                            <label>First name:</label>
                            <input id="fname" type="text" name="fname" value="{{ old('fname') }}" required autofocus>
                        </div>

                        <div class = "field">
                            <label>Middle name:</label>
                            <input id="mname" type="text" name="mname" value="{{ old('mname') }}" required autofocus>
                        </div>

                        <div class = "field">
                            <label>Last name:</label>
                            <input id="lname" type="text" name="lname" value="{{ old('lname') }}" required>
                        </div>
                    </div>

                    <div class = "field">
                        <label>E-mail address:</label>
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                    </div>

                    <div class = "field">
                        <label>Username:</label>
                        <input id="username" type="text" name="username" value="{{ old('username') }}" required>
                    </div>

                    <div class = "fields">
                        <div class="field">
                            <label>Password:</label>
                            <input id="password" type="password" name="password" required>
                        </div>

                        <div class="field">
                            <label>Confirm password:</label>
                            <input id="password-confirm" type="password" name="password_confirmation" required>
                        </div>
                    </div>

                    <div class = "field">
                        <button type = "submit" class = "ui small positive button">
                            <i class = "add user icon"></i>
                            Register
                        </button>
                        <button type = "reset" class = "ui small negative button">
                            <i class = "repeat icon"></i>
                            Reset fields
                        </button>
                    </div>

                    <div class = "field">
                        <a href = "{{ route('login') }}">Already have an account?</a>
                    </div>
                </form>
            </div>
        </div>
        <div class = "column"></div>
    </div>
@endsection

@section('scripts')
    @if($errors->has('fname'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('fname') }}');
        </script>
    @endif
    @if($errors->has('mname'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('mname') }}');
        </script>
    @endif
    @if($errors->has('lname'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('lname') }}');
        </script>
    @endif

    @if($errors->has('email'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('email') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgRegister').toggleClass('active',true);

        $('#password, #password-confirm').on('keyup click', function(){
            if ($('#password').val() != $('#password-confirm').val()) {
                $('#password, #password-confirm').css({
                    'border-color':'red'
                });
            }
            else {
                $('#password, #password-confirm').css({
                    'border-color':''
                });
            }
        });
    </script>
@endsection