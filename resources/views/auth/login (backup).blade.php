@section('page-name')
    {{ config('app.name', 'Laravel') }} | Login
@endsection

@extends("layouts.master")

@section("content")
    <div class = "row">
        {{-- <div class = "column">
            <div class = "ui basic clearing segment">
                <h5 class = "ui header">About us</h5>

                <p>
                    <img src = "{{ asset('images/deped-logo.png') }}" style="float: left; margin: 1rem; width: 10rem; height: auto;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate mi et luctus egestas. Phasellus quis lacus commodo, vestibulum neque non, pharetra nisl. Ut odio quam, pretium at scelerisque eget, eleifend vitae eros. Mauris a mauris imperdiet lectus rutrum dignissim nec sed ipsum. Praesent pellentesque nisi eget sodales accumsan. Sed nisl libero, fermentum vitae enim eu, dictum malesuada nibh. Integer vel nunc dictum leo suscipit sollicitudin eget at orci. Nunc in imperdiet sem, non convallis nulla.
                </p>

                <p>
                    Ut dignissim eu nisi a interdum. Nulla massa velit, tempor sit amet arcu ut, bibendum condimentum lorem. Aliquam arcu lectus, placerat quis orci quis, bibendum ullamcorper massa. Suspendisse dolor metus, dictum et pretium id, lobortis vitae ex. Nullam malesuada leo non augue aliquet, vel hendrerit enim imperdiet. Nulla in nunc a sem placerat consequat eget vitae diam. Sed vel efficitur ipsum, maximus ullamcorper lorem.
                </p>

                <p>
                    Ut dignissim eu nisi a interdum. Nulla massa velit, tempor sit amet arcu ut, bibendum condimentum lorem. Aliquam arcu lectus, placerat quis orci quis, bibendum ullamcorper massa. Suspendisse dolor metus, dictum et pretium id, lobortis vitae ex. Nullam malesuada leo non augue aliquet, vel hendrerit enim imperdiet. Nulla in nunc a sem placerat consequat eget vitae diam. Sed vel efficitur ipsum, maximus ullamcorper lorem.
                </p>
            </div>
        </div> --}}
        <div class = "column"></div>
        <div class = "seven wide column">
            <div class = "ui small text segment">
                <h5 class = "ui header">Login</h5>
                
                <form id = "frmLogin" class = "ui small equal width form" method = "POST" action = "{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class = "field{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label>Email:</label>
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
                    </div>

                    <div class = "field{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label>Password:</label>
                        <input id="password" type="password" name="password" required>
                    </div>

                    <div class = "field">
                        <div class = "ui checkbox">
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label>Keep me logged in</label>
                        </div>
                    </div>

                    <div class = "field">
                        <button type = "submit" class = "ui small positive button">
                            <i class = "sign in icon"></i>
                            Login
                        </button>
                    </div>

                    <div class = "field">
                        <a href = "{{ route('password.request') }}">Forgot your password?</a>
                    </div>
                </form>
            </div>
        </div>
        <div class = "column"></div>
    </div>
@endsection

@section('scripts')
    @if($errors->has('email'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('email') }}');
        </script>
    @endif

    @if($errors->has('password'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('password') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgLogin').toggleClass('active',true);
    </script>
@endsection