@extends('layouts.errors')

@section('page-name')
Unauthorized access | {{ config('app.name','Cannot display app name') }}
@endsection

@section('content')
	<div class = "row">
		<div class = "column">
			<div class = "ui basic center aligned segment">
				<div><img src="{{ asset('images/403.png') }}" width="75%" height="auto"></div>
				<div>
					@if(Auth::guard('web')->check())
						<a href="/home" role = "button" class = "ui blue button">
							Force Burst of Speed out of here
						</a>
					@else
						<a href="/login" role = "button" class = "ui blue button">
							Force Burst of Speed out of here
						</a>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection