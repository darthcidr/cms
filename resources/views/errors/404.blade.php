@extends('layouts.errors')

@section('page-name')
Page not found | {{ config('app.name','Cannot display app name') }}
@endsection

@section('content')
	<div class = "row">
		<div class = "column">
			<div class = "ui basic center aligned segment">
				<div><img src="{{ asset('images/404.jpg') }}" width="75%" height="auto"></div>
				<div>
					<a href="/home" role = "button" class = "ui positive button">
						Move along
					</a>
				</div>
			</div>
		</div>
	</div>
@endsection