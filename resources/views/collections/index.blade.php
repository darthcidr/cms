@section('page-name')
    Collections | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "seven wide column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Add collection</h4>

                <form id = "frmAddCollection" class = "ui small equal width form" method = "POST">
                    {{ csrf_field() }}
                    <input type = "text" id = "token" value = "{{ csrf_token() }}" hidden>

                    <div class = "field">
                        <label>Payor</label>
                        <input type = "text" id = "txtAddCollectionPayor" name = "txtAddCollectionPayor" required>
                    </div>

                    <div class = "field">
                        <label>Agency</label>
                        <input type = "text" id = "txtAddCollectionAgency" name = "txtAddCollectionAgency" required>
                    </div>

                    <div class = "field">
                        <label>Fund source</label>
                        <select id = "cmbAddCollectionFundCluster" name = "cmbAddCollectionFundCluster" class = "ui small search fluid dropdown" required>

                        </select>
                    </div>

                    <table id = "tblListOfToCollect" class = "ui small table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nature of payment</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>

                        </tbody>

                        <tfoot>
                            <td></td>
                            <td>
                                <input type = "text" id = "txtToCollectDescription" placeholder="Description...">
                            </td>
                            <td>
                                <input type = "number" id = "txtToCollectAmount" min = "0" step="0.01" value = "0" placeholder="Amount...">
                            </td>
                            <td>
                                <button type = "button" id = "btnToCollectAdd" class = "ui mini icon button">
                                    <i class = "plus icon"></i>
                                </button>
                            </td>
                        </tfoot>
                    </table>

                    <div class = "fields">
                        <div class = "field">
                            <label>Official Receipt #</label>
                            <input type = "text" id = "txtAddCollectionORNumber" name = "txtAddCollectionORNumber" data-mask = "9999999" required>
                        </div>

                        <div class = "field">
                            <label>Date</label>
                            <input type = "date" id = "txtAddCollectionDate" name = "txtAddCollectionDate" value = "{{ date('Y-m-d') }}" required>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Total</label>
                        <input type = "number" id = "nudAddCollectionTotal" name = "nudAddCollectionTotal" readonly required>
                    </div>

                    <button type = "submit" id = "btnAddCollection" class = "ui small positive button">
                        <i class = "send icon"></i>
                        Submit
                    </button>
                </form>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">List of collections</h4>

                <table id = "tblListOfCollections" class = "ui small celled striped table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>OR #</th>
                            <th>Total</th>
                            <th>Collecting officer</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');

            if ('{{ session('type') }}' == 'success') {
                window.open('/reports/collections/or/{{ session('or_number') }}');
            }
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgCollections').toggleClass('active',true);
    </script>
@endsection
