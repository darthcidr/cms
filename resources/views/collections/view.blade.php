@section('page-name')
    View Collection | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "eight wide column">
            <div class = "ui small text segment">
                <h4 class = "ui header">View collection under OR #{{ $collection->or_number }}</h4>

                <h5 class = "ui header">Particulars</h5>
                <div class = "ui relaxed ordered list">
                    @if(count($particulars) > 0)
                        @foreach($particulars as $particular)
                            <div class = "item">
                                {{ $particular->desc }} = {{ $particular->amount }} PHP
                            </div>
                        @endforeach
                    @else
                        <div class = "item">No entries found</div>
                    @endif
                </div>

                <div class = "ui divider"></div>

                <div class = "ui small equal width form">
                    <div class = "field">
                        <label>Payor</label>
                        <input type = "text" value = "{{ $collection->payor }}" readonly>
                    </div>

                    <div class = "field">
                        <label>Agency</label>
                        <input type = "text" value = "{{ $collection->agency }}" readonly>
                    </div>

                    <div class = "fields">
                        <div class = "field">
                            <label>OR Number</label>
                            <input type = "text" value = "{{ $collection->or_number }}" readonly>
                        </div>

                        <div class = "field">
                            <label>Date</label>
                            <input type = "date" value = "{{ date('Y-m-d',strtotime($collection->created_at)) }}" readonly>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Total</label>
                        <input type = "number" value = "{{ $collection->total }}" readonly>
                    </div>

                    <div class = "ui three buttons">
                        <a href = "/collections" role = "button" class = "ui small button">
                            <i class = "left arrow icon"></i>
                            Back
                        </a>
                        <a href = "/reports/collections/or/{{ $collection->or_number }}" class = "ui small button" target="_new">
                            <i class = "print icon"></i>
                            Print
                        </a>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgCollections').toggleClass('active',true);
    </script>
@endsection
