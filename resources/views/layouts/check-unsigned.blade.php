<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('page-name')</title>

        <!-- Styles -->
        <link href="{{ asset('packages/packages.css') }}" rel="stylesheet">
    </head>

    <body style = "">
    	<div class = "ui fluid container" style = " margin-top: 26.5rem; width: 7in; height: 5.5in;">
			<div class = "ui stackable equal width grid">
				<div class = "row">
					<div class = "column">
                        @yield('content')
					</div>
				</div>
			</div>
    	</div>

        <!-- Scripts -->
        <script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
        <script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>
    </body>
</html>