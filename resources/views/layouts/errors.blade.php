<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
        <title>@yield('page-name')</title>

        <!-- Styles -->
        <link href="{{ asset('packages/packages.css') }}" rel="stylesheet">
    </head>

    <body>
        <nav class = "ui large top fixed inverted borderless stackable menu" style = "background-color: #04508c;">
            <a class = "header item" href = "{{ url('/') }}">
                <img src = "{{ asset('favicon.ico') }}">
                &nbsp;
                {{ config('app.name','Cannot display app name') }}
                {{-- <img src = "{{ asset('images/deped-logo-with-text.png') }}" style = "width: 100%; height: auto;"> --}}
            </a>
        </nav>


        <div class = "ui container" style = "padding-top: 7rem;">
            <div class = "ui stackable equal width grid">
                @yield('content')
            </div>
        </div>

        <!-- Scripts -->
        <script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
        <script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>
    </body>
</html>