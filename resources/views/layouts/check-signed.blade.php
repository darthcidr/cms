<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('page-name')</title>

        <!-- Styles -->
        <link href="{{ asset('packages/packages.css') }}" rel="stylesheet">
    </head>

    <body>
    	<div class = "ui fluid container" style = "font-family: Tahoma; padding-top: 26rem; height: 8.5in; width: 11in;">
			<div class = "ui stackable equal width grid">
				<div class = "row">
					<div class = "column">
						<div style = "position: absolute; right: 6rem;">{{ $check->check_number }}</div>

                        @yield('content')

                        <div style = "position: absolute; right: 6rem; top: 12rem;">
                            {{ strtoupper($signatoryCashUnit->fname) }}
                            {{ strtoupper($signatoryCashUnit->mname) }}
                            {{ strtoupper($signatoryCashUnit->lname) }}
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            {{ strtoupper($signatoryRegionalDir->fname) }}
                            {{ strtoupper($signatoryRegionalDir->mname) }}
                            {{ strtoupper($signatoryRegionalDir->lname) }}
                        </div>
						<div style = "position: absolute; right: 7rem; top: 13.5rem;">Administrative Officer V&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Regional Director</div>
						<div style = "position: absolute; right: 22rem; top: 14.75rem;">Cash Unit</div>
						<div style = "position: absolute; left: 2rem; top: 16.5rem;">{{ $check->code }}</div>
					</div>
				</div>
			</div>
    	</div>

        <!-- Scripts -->
        <script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
        <script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>
    </body>
</html>
