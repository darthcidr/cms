@section('page-name')
    Settings - Activity Log | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "stretched row">
        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Activity log</h4>

                <table id = "tblActivityLog" class = "ui small celled striped table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>User logged in</th>
                            <th>Action</th>
                            <th>Timestamp</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
@endsection

@section('scripts')
    <script type = "text/javascript">
        $('#pgSettings, #pgSettingsActivities').toggleClass('active',true);
    </script>
@endsection
