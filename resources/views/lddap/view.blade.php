@section('page-name')
    LDDAP #{{ $lddap->lddap_ada_number }} | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Voucher information</h4>

                <div id = "frmEditLDDAP" class = "ui small equal width form">
                    <div class = "fields">
                        <div class = "field">
                            <label>Fund cluster</label>
                            <select id = "cmbEditLDDAPFundCluster" name = "cmbEditLDDAPFundCluster" class = "ui small fluid search dropdown" readonly>

                            </select>
                        </div>

                        <div class = "field">
                            <label>Date</label>
                            <input type = "date" id = "dtpEditLDDAPDate" name = "dtpEditLDDAPDate" value = "{{ $voucher->voucher_date }}" readonly>
                        </div>

                        <div class = "field">
                            <label>DV no.</label>
                            <input type = "text" id = "txtEditLDDAPDVNum" name = "txtEditLDDAPDVNum" value = "{{ $voucher->dv_num }}" readonly>
                        </div>

                        <div class = "field">
                            <label>ORS/BURS no.</label>
                            <input type = "text" id = "txtEditLDDAPORSBURSNum" name = "txtEditLDDAPORSBURSNum" value = "{{ $voucher->ors_burs_num }}" readonly>
                        </div>
                    </div>

                    <div class = "fields">
                        <div class = "field">
                            <label>Payee</label>
                            <input type = "text" id = "txtEditLDDAPPayee" name = "txtEditLDDAPPayee" value = "{{ $voucher->payee }}" readonly>
                        </div>

                        <div class = "field">
                            <label>Address</label>
                            <textarea id = "txtEditLDDAPPayeeAddress" name = "txtEditLDDAPPayeeAddress" rows="3" readonly>{{ $voucher->address }}</textarea>
                        </div>

                        <div class = "field">
                            <label>Particular</label>
                            <textarea id = "txtEditLDDAPParticular" name = "txtEditLDDAPParticular" rows="3" readonly>{{ $voucher->particular }}</textarea>
                        </div>
                    </div>

                    <div class = "fields">
                        <div class = "field">
                            <label>UACS code</label>
                            <input type = "text" id = "txtEditLDDAPUACSCode" name = "txtEditLDDAPUACSCode" value = "{{ $voucher->uacs_code }}" data-mask = "9999999999" readonly>
                        </div>

                        <div class = "field">
                            <label>Amount</label>
                            <input type = "number" id = "txtEditLDDAPAmount" name = "txtEditLDDAPAmount" value = "{{ $voucher->total }}" min="0" step = "0.01" readonly>
                        </div>
                    </div>
                </div>
            </div>

            <div class = "ui small text segment">
                <h4 class = "ui header">Edit LDDAP #{{ $lddap->lddap_ada_number }}</h4>

                <form id = "frmEditLDDAP" class = "ui small equal width form" method = "POST" action = "{{ route('lddap.update',$lddap->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class = "field">
                        <label>Status</label>
                        <select id = "cmbEditLDDAPStatus" name = "cmbEditLDDAPStatus" class = "ui small search fluid dropdown" required>
                            <option value = "ACTIVE">ACTIVE</option>
                            <option value = "CANCELLED">CANCELLED</option>
                        </select>
                    </div>

                    <div class = "fields">
                        <div class = "field">
                            <label>SLIIAE No.</label>
                            <input type = "text" id = "txtEditLDDAPSliiaeNumber" name = "txtEditLDDAPSliiaeNumber" value = "{{ $lddap->sliiae_number }}" data-mask = "999" required>
                        </div>

                        <div class = "field">
                            <label>LDDAP-ADA No.</label>
                            <input type = "text" id = "txtEditLDDAPLDDAPADANumber" name = "txtEditLDDAPLDDAPADANumber" value = "{{ $lddap->lddap_ada_number }}" data-mask = "9 99999999-99-999-9999" required>
                        </div>
                    </div>

                    <div class = "field">
                        <a href = "/lddap/all" role = "button" class = "ui small button">
                            <i class = "left arrow icon"></i>
                            Back
                        </a>

                        <button type = "submit" class = "ui small positive button">
                            <i class = "send icon"></i>
                            Submit
                        </button>

                        <button type = "button" id = "btnDeleteLDDAP" class = "ui small negative button">
                            <i class = "trash icon"></i>
                            Delete
                        </button>
                    </div>
                </form>

                <form id = "frmDeleteLDDAP" method = "POST" action = "{{ route('lddap.destroy',$lddap->id) }}" hidden>
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgSettingsSignatories').toggleClass('active',true);

        $('#cmbEditLDDAPStatus').val('{{ $lddap->status }}');
        $('#cmbEditLDDAPFundCluster').val('{{ $lddap->fund_id }}');

        $(document).on('click', '#btnDeleteLDDAP', function(event){
            event.preventDefault();
            if (confirm('Are you sure to delete LDDAP #{{ $lddap->lddap_ada_number }}?')) {
                $('#frmDeleteLDDAP').submit();
            }
        });
    </script>
@endsection