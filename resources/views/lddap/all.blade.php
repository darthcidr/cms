@section('page-name')
    All LDDAP | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "stretched row">
        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">List of Due and Demandable Accounts Payable (LDDAP)</h4>

                <table id = "tblListOfAllLDDAP" class = "ui small celled striped table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>LDDAP-ADA number</th>
                            <th>SLIIAE number</th>
                            <th>Fund cluster</th>
                            <th>Payee</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
@endsection

@section('scripts')
    {{-- @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');

            window.open('/checks/print/signed/{{ session('check_number') }}');
            window.open('/checks/print/unsigned/{{ session('check_number') }}');
        </script>
    @endif --}}

    <script type = "text/javascript">
        $('#pgLDDAPs, #pgAllLDDAP').toggleClass('active',true);
    </script>
@endsection
