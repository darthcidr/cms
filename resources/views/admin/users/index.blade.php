@section('page-name')
    Users | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "seven wide column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Add cash unit user</h4>

                <form id = "frmAddUser" class = "ui small equal width form" method = "POST" action = "{{ route('admin.create.cash-unit-user') }}">
                    {{ csrf_field() }}

                    <div class = "fields">
                        <div class = "field">
                            <label>First name:</label>
                            <input id="fname" type="text" name="fname" value="{{ old('fname') }}" required autofocus>
                        </div>

                        <div class = "field">
                            <label>Middle name:</label>
                            <input id="mname" type="text" name="mname" value="{{ old('mname') }}" required autofocus>
                        </div>

                        <div class = "field">
                            <label>Last name:</label>
                            <input id="lname" type="text" name="lname" value="{{ old('lname') }}" required>
                        </div>
                    </div>

                    <div class = "field">
                        <label>E-mail address:</label>
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                    </div>

                    <div class = "field">
                        <label>Username:</label>
                        <input id="username" type="text" name="username" value="{{ old('username') }}" required>
                    </div>

                    <div class = "fields">
                        <div class="field">
                            <label>Password:</label>
                            <input id="password" type="password" name="password" required>
                        </div>

                        <div class="field">
                            <label>Confirm password:</label>
                            <input id="password-confirm" type="password" name="password_confirmation" required>
                        </div>
                    </div>

                    <div class = "field">
                        <button type = "submit" class = "ui small positive button">
                            <i class = "add user icon"></i>
                            Register
                        </button>
                        <button type = "reset" class = "ui small negative button">
                            <i class = "repeat icon"></i>
                            Reset fields
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">List of cash unit users</h4>

                <table id = "tblListOfCashUnitUsers" class = "ui small celled striped table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Email</th>
                            <th>Full name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgUsers').toggleClass('active',true);
    </script>
@endsection
