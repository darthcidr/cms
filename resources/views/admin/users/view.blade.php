@section('page-name')
    Users | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "seven wide column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Edit cash unit user</h4>

                <form id = "frmEditUser" class = "ui small equal width form" method = "POST" action = "{{ route('admin.update.cash-unit-user',$user->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class = "fields">
                        <div class = "field">
                            <label>First name:</label>
                            <input id="fname" type="text" name="fname" value="{{ $user->fname }}" required autofocus>
                        </div>

                        <div class = "field">
                            <label>Middle name:</label>
                            <input id="mname" type="text" name="mname" value="{{ $user->mname }}" required autofocus>
                        </div>

                        <div class = "field">
                            <label>Last name:</label>
                            <input id="lname" type="text" name="lname" value="{{ $user->lname }}" required>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Profile picture:</label>
                        <input type = "file" accept = "image/jpeg,image/jpg,image/png" id = "picture" name = "picture" value = "/images/profile_picture/{{ (Auth::guard('admin')->user()->image) }}">
                    </div>

                    <div class = "field">
                        <label>E-mail address:</label>
                        <input id="email" type="email" name="email" value="{{ $user->email }}" required>
                    </div>

                    <div class = "ui three small buttons">
                        <button type = "button" class = "ui small button">
                            <i class = "left arrow icon"></i>
                            Back
                        </button>
                        <button type = "submit" class = "ui small positive button">
                            <i class = "refresh icon"></i>
                            Update
                        </button>
                        <button id = "btnDeleteUser" type = "button" class = "ui small negative button">
                            <i class = "trash icon"></i>
                            Delete
                        </button>
                    </div>
                </form>

                <form id = "frmDeleteUser" action = "{{ route('admin.delete.cash-unit-user',$user->id) }}" method = "POST" hidden>
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>
            </div>
        </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgSettingsFunds').toggleClass('active',true);

        $(document).on('click', '#btnDeleteUser', function(event){
            event.preventDefault();
            if (confirm('Confirm delete user: {{ $user->fname }} {{ $user->lname }}?') == true) {
                $('#frmDeleteUser').submit();
            }
        });
    </script>
@endsection
