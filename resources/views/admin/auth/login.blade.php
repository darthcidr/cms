@section('page-name')
    {{ config('app.name', 'Laravel') }} | Administrator Login
@endsection

@extends("layouts.master")

@section("content")
    <div class = "row">
        <div class = "column"></div>
        <div class = "seven wide column">
            <div class = "ui small text segment">
                <h5 class = "ui header">Administrator Login</h5>

                <div class = "ui negative icon message">
                    <i class = "warning icon"></i>
                    This page is intended for administrators only.
                </div>
                
                <form id = "frmLogin" class = "ui small equal width form" method = "POST" action = "{{ route('admin.login') }}">
                    {{ csrf_field() }}

                    <div class = "field{{ $errors->has('username') ? ' has-error' : '' }}">
                        <label>Username:</label>
                        <input id="username" type="text" name="username" value="{{ old('username') }}" required autofocus>
                    </div>

                    <div class = "field{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label>Password:</label>
                        <input id="password" type="password" name="password" required>
                    </div>

                    <div class = "field">
                        <div class = "ui checkbox">
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label>Keep me logged in</label>
                        </div>
                    </div>

                    <div class = "field">
                        <button type = "submit" class = "ui small positive button">
                            <i class = "sign in icon"></i>
                            Login
                        </button>
                    </div>

                    <div class = "field">
                        <a href = "{{ route('password.request') }}">Forgot your password?</a> &bullet; <a href = "{{ route('register') }}">Create cash unit user</a>
                    </div>
                </form>
            </div>
        </div>
        <div class = "column"></div>
    </div>
@endsection

@section('scripts')
    @if($errors->has('username'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('username') }}');
        </script>
    @endif

    @if($errors->has('password'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('password') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgLogin').toggleClass('active',true);
    </script>
@endsection