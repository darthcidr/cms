@section('page-name')
    Administrator Dashboard | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "stretched row">
        <div class = "column">
            <div class = "ui small center aligned clearing text segment">
                <i class = "huge orange money icon" style="float: left;"></i>

                <div class="ui tiny statistic">
                    <div class="label">
                        RELEASED CHECKS
                    </div>

                    <div class="value">
                        {{ $allChecks }}
                    </div>
                </div>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small center aligned clearing text segment">
                <i class = "huge green check icon" style="float: left;"></i>

                <div class="ui tiny statistic">
                    <div class="label">
                        ACTIVE CHECKS
                    </div>

                    <div class="value">
                        {{ $activeChecks }}
                    </div>
                </div>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small center aligned clearing text segment">
                <i class = "huge red x icon" style="float: left;"></i>

                <div class="ui tiny statistic">
                    <div class="label">
                        CANCELLED CHECKS
                    </div>

                    <div class="value">
                        {{ $cancelledChecks }}
                    </div>
                </div>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small center aligned clearing text segment">
                <i class = "huge gray x icon" style="float: left;"></i>

                <div class="ui tiny statistic">
                    <div class="label">
                        STALE CHECKS
                    </div>
                    <div class="value">
                        {{ $staleChecks }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class = "row">
        <div class = "column">
            <div class = "ui small text segment">
                {!! $chartAnnualChecksReleased->html() !!}
            </div>
        </div>

        <div class = "column">
            <div class = "ui small text segment">
                {!! $chartAnnualExpense->html() !!}
            </div>
        </div>
    </div>

    <div class = "row">
        <div class = "column">
            <div class = "ui small text segment">
                {!! $chartAnnualCollection->html() !!}
            </div>
        </div>

        <div class = "column">
            <div class = "ui small text segment">
                {!! $chartCollectionAccdgToFund->html() !!}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgHome').toggleClass('active',true);
    </script>
@endsection

@section('charts-js')
    {!! $chartAnnualExpense->script() !!}
    {!! $chartAnnualChecksReleased->script() !!}
    {!! $chartAnnualCollection->script() !!}
    {!! $chartCollectionAccdgToFund->script() !!}
@endsection