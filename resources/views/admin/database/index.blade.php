@section('page-name')
    Settings - Database Management | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Backup database</h4>

                <form id = "frmBackupDB" class = "ui small equal width form" action = "{{ route('admin.settings.database.backup') }}" method = "POST">
                    {{ csrf_field() }}

                    <div class = "field">
                        <label>File name</label>
                        <input type = "text" id = "txtBackupDBFileName" name = "txtBackupDBFileName" required>
                    </div>

                    <div class = "field">
                        <button type = "submit" class = "ui small positive button">
                            <i class = "send icon"></i>
                            Backup
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Restore database</h4>

                <form id = "frmRestoreDB" class = "ui small equal width form" action = "{{ route('admin.settings.database.restore') }}" method = "POST" enctype = "multipart/form-data">
                    {{ csrf_field() }}

                    <div class = "field">
                        <label>File name</label>
                        <input type = "file" id = "fileRestoreDBFile" name = "fileRestoreDBFile" accept = ".sql" required>
                    </div>

                    <div class = "field">
                        <button type = "submit" class = "ui small positive button">
                            <i class = "send icon"></i>
                            Restore
                        </button>
                    </div>
                </form>
            </div>
        </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif
    @if(session()->has('file'))
        <script type = "text/javascript">
            window.open('{{ env('APP_URL') }}/database_backup/{{ session('file') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgSettingsDatabase').toggleClass('active',true);
    </script>
@endsection
