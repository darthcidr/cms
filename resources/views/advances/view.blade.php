@section('page-name')
    Cash advance under Disbursement Voucher #{{ $voucher->dv_num }} | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "stretched row">
        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Cash advance under Disbursement Voucher #{{ $voucher->dv_num }}</h4>

                <form id = "frmEditLiquidation" class = "ui small equal width form" action = {{ route('advance.update',$advance->id) }} method = "POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class = "field">
                        <label>Breakdown</label>
                        <div class = "ui relaxed ordered list">
                            @if(count($particulars) > 0)
                                @foreach($particulars as $particular)
                                    <div class = "item">{{ $particular->description }}, {{ $particular->amount }} PHP</div>
                                @endforeach
                            @else
                                <div class = "item">No item/s found</div>
                            @endif
                        </div>
                    </div>

                    <div class = "fields">
                        <div class = "field">
                            <label>Amount advanced</label>
                            <input type = "number" id = "txtEditLiquidationCashAdvanced" name = "txtEditLiquidationCashAdvanced" value = "{{ $voucher->total }}" step = "0.01" disabled>
                        </div>

                        <div class = "field">
                            <label>Total amount of item/s</label>
                            <input type = "number" id = "txtEditLiquidationTrueTotal" name = "txtEditLiquidationTrueTotal" value = "{{ $advance->true_total }}" step = "0.01" disabled>
                        </div>

                        @if($advance->amount_reimbursed <= 0)
                            <div class = "field">
                                <label>Amount to reimburse</label>
                                <input type = "number" id = "txtEditLiquidationAmountToReimburse" name = "txtEditLiquidationAmountToReimburse" value = "{{ abs($advance->amount_reimbursed) }}" step = "0.01" disabled>
                            </div>
                        @elseif($advance->amount_reimbursed > 0)
                            <div class = "field">
                                <label>Amount to refund</label>
                                <input type = "number" id = "txtEditLiquidationAmountToReimburse" name = "txtEditLiquidationAmountToReimburse" value = "{{ $advance->amount_reimbursed }}" step = "0.01" disabled>
                            </div>
                        @endif
                    </div>

                    <div class = "ui divider"></div>

                    <div class = "fields">
                        <div class = "field">
                            <label>Receiver's name</label>
                            <input type = "text" id = "txtEditLiquidationReceiverName" name = "txtEditLiquidationReceiverName" value = "{{ $advance->receiver_name }}" required>
                        </div>

                        <div class = "field">
                            <label>Position</label>
                            <input type = "text" id = "txtEditLiquidationReceiverPosition" name = "txtEditLiquidationReceiverPosition" value = "{{ $advance->receiver_position }}" required>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Remarks</label>
                        <textarea type = "text" id = "txtEditLiquidationRemarks" name = "txtEditLiquidationRemarks" rows = "2" required>{{ $advance->remarks }}</textarea>
                    </div>

                    <div class = "field">
                        <label>Status</label>
                        <input type = "text" id = "txtEditLiquidationStatus" name = "txtEditLiquidationStatus" value = "{{ $advance->status }}" readonly disabled>
                    </div>

                    <div class = "field">
                        <a href = "/advances/all" class = "ui small button">
                            <i class = "left arrow icon"></i>
                            Back
                        </a>

                        <button type = "submit" class = "ui small positive button">
                            <i class = "send icon"></i>
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type = "text/javascript">
        $('#pgAdvances, #pgAllAdvances').toggleClass('active',true);
    </script>
@endsection
