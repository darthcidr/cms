@section('page-name')
    Cash advances | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "stretched row">
        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">List of cash advances</h4>

                <table id = "tblListOfCashAdvances" class = "ui small celled striped table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>DV No.</th>
                            <th>Check number</th>
                            <th>Receiver</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type = "text/javascript">
        $('#pgAdvances, #pgAllAdvances').toggleClass('active',true);
    </script>

    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif
@endsection
