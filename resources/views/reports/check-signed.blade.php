@section('page-name')
    Print Check #{{ $check->check_number }} | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.check-signed')

@section('content')
	<div style = "position: absolute; right: 7rem; top: 2rem;">{{ date('F d, Y') }}</div>
	<div style = "display: inline;">
		<div style = "position: absolute; left: 5rem; top: 4rem;">* * * {{ strtoupper($check->payee) }} * * *</div>
		<div style = "position: absolute; left: 42rem; top: 4rem;">{{ number_format($check->total,2,'.',',') }}</div>
	</div>
	<div style = "position: absolute; left: 3rem; top: 6rem;">* * * {{ strtoupper(str_replace('-', ' ', $total_in_words)) }} * * *</div>
	<div style = "position: absolute; right: 5rem; top: 10rem; font-size: 12px;">101101&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;070010300001&nbsp;&nbsp;013314000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $check->uacs_code }}</div>
@endsection
