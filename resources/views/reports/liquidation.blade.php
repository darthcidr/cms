<?php


// Require composer autoload
require_once __DIR__ .'/../../../vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf(['setAutoTopMargin' => 'stretch'],['format' => 'Legal']);
// $mpdf = new \Mpdf\Mpdf('utf-8', array(190,236));
// Buffer the following html with PHP so we can store it to a variable later
ob_start();
?>
{{-- This is where your script would normally output the HTML using echo or print --}}
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Liquidation Report under Disbursement Voucher #{{ $advance->voucher }}</title>

      <style media="screen">
        /* .topBorder{
          border-top: 3px solid black;
        }
        .bottomBorder{
          border-bottom: 3px solid black;
        }
        .rightBorder{
          border-right: 3px solid black;
        }
        .leftBorder{
          border-left: 3px solid black;
        }
        table, th, td{
          padding: 10px;
        }*/
      </style>
      <!-- Styles -->
      {{-- <link href="{{ asset('packages/packages.css') }}" rel="stylesheet"> --}}
  </head>
  <body>
    <div class="ui fluid container">
      <div class = "ui stackable equal width grid">
        <div class = "row">
          <div class="column">
            <table cellspacing="0" width="100%">
              <tr>
                <th colspan="2" style="font-size: 15px;padding-bottom:0px; border-top: 3px solid ;border-left: 3px solid ;border-right: 3px solid ;">LIQUIDATION REPORT</th>
                <td style="font-size: 12px;padding-bottom:1px; border-top: 3px solid ;border-right: 3px solid;">Serial No.:________________</td>
              </tr>
            <tbody>
              <tr>
                <td colspan="2"  style="font-size: 12px;padding-top:0px; text-align:center;border-left: 3px solid; border-right: 3px solid;">{{date('M d, Y')}}</td>
                <td style="font-size: 12px;padding-bottom:3px;border-right: 3px solid;border-bottom: 3px solid;">Date: ____________________</td>
              </tr>
              <tr>
                <td colspan="2"  style="font-size: 12px;padding:2px; border-left: 3px solid;border-right: 3px solid;"><strong>Entity Name : Department of Education Region 1</strong></td>
                <td rowspan="2" style="font-size: 12px;padding:2px; border-right: 3px solid; border-bottom: 3px solid;" >Responsibility Center Code: <br>_________________________</td>
              </tr>
              <tr>
                <td colspan="2"  style="font-size: 12px;padding-top:2px;padding-left:2px; padding-right:2px;  border-left: 3px solid; border-right: 3px solid; border-bottom: 3px solid;" ><strong>Fund Cluster:</strong> 01</td>
              </tr>
              <tr>
                <td colspan="2"  style="font-size: 12px;padding-top:5px; padding-bottom:5px; padding-left:100px; border-left: 3px solid; border-right: 3px solid;  border-bottom: 3px solid;" ><strong>PARTICULARS</strong></td>
                <td style="font-size: 12px;text-align: center; border-right: 3px solid; border-bottom: 3px solid;"><strong>AMOUNT</strong></td>
              </tr>
              <tr class="centertext ">
                <td colspan="2" style="font-size: 12px;padding-top:3px; padding-bottom:5px; border-left: 3px solid; border-right: 3px solid;">{{ $voucher->particular }}</td>
                <td style="font-size: 12px;border-right: 3px solid;"></td>
              </tr>
              <tr>
                <td colspan="2" style=" border-left: 3px solid; border-right: 3px solid;"><br></td>
                <td style=" border-right: 3px solid;"></td>
              </tr>

              @if(count($particulars) > 0)
                @foreach($particulars as $particular)
                  <tr>
                    <td colspan="2" style="font-size: 12px; padding-bottom:10px; border-left: 3px solid; border-right: 3px solid;">{{ $particular->description }}</td>
                    <td style="font-size: 12px; text-align:right; padding-bottom:1px; border-right: 3px solid;">{{ $particular->amount }}</td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="2" style="font-size: 12px;padding-bottom:10px; border-left: 3px solid; border-right: 3px solid;">No entries found</td>
                  <td style="font-size: 12px;text-align:right; padding-bottom:1px; border-right: 3px solid;">N/A</td>
                </tr>
              @endif

              <tr>
                <td colspan="2" style=" border-left: 3px solid; border-right: 3px solid;"><br><br></td>
                <td style=" border-right: 3px solid;"></td>
              </tr>
              <tr>
                <td colspan="2" style="font-size: 12px; border-left: 3px solid; border-right: 3px solid;">{{ $advance->remarks }}</td>
                <td style="font-size: 12px; border-right: 3px solid;"></td>
              </tr>
              <tr>
                <td colspan="2" style=" border-left: 3px solid; border-right: 3px solid;"><br><br></td>
                <td style=" border-right: 3px solid;"></td>
              </tr>
              <tr>
                <td colspan="2" style="font-size: 12px;padding-bottom:5px; border-left: 3px solid; border-right: 3px solid; border-bottom: 3px solid;">TOTAL AMOUNT SPENT</td>
                <td style="font-size: 12px;text-align:right; border-right: 3px solid; border-bottom: 3px solid;" >{{ $advance->true_total }}</td>
              </tr>
              <tr>
                <td colspan="2" style="font-size: 12px;padding-bottom:5px; padding-top:5px; border-left: 3px solid; border-right: 3px solid; border-bottom: 3px solid;">AMOUNT OF CASH ADVANCE PER DV NO. {{ $advance->voucher }} dtd. {{ (new DateTime($voucher->voucher_date))->format('F d, Y') }}</td>
                <td  style="font-size: 12px;text-align:right; border-right: 3px solid; border-bottom: 3px solid;">{{ $voucher->total }}</td>
              </tr>

              @if($advance->amount_reimbursed < 0)
                <tr>
                  <td colspan="2"  style="font-size: 12px;padding-top:5px;padding-bottom:5px; border-left: 3px solid; border-right: 3px solid; border-bottom: 3px solid;">AMOUNT REFUNDABLE PER OR NO.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DTD</td>
                  <td  style="font-size: 12px;text-align:right;  border-right: 3px solid; border-bottom: 3px solid;"></td>
                </tr>
                <tr>
                  <td colspan="2" style="font-size: 12px;padding-top:5px;padding-bottom:5px; border-left: 3px solid; border-right: 3px solid; border-bottom: 3px solid;">AMOUNT TO BE REIMBURSED</td>
                  <td  style="font-size: 12px;text-align:right; border-right: 3px solid; border-bottom: 3px solid;">{{ number_format(abs($advance->amount_reimbursed),2,'.',',') }}</td>
                </tr>
              @else
                <tr>
                  <td colspan="2"  style="font-size: 12px;padding-top:5px;padding-bottom:5px; border-left: 3px solid; border-right: 3px solid; border-bottom: 3px solid;">AMOUNT REFUNDABLE PER OR NO.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DTD</td>
                  <td  style="font-size: 12px;text-align:right; border-right: 3px solid; border-bottom: 3px solid;">{{ number_format(abs($advance->amount_reimbursed),2,'.',',') }}</td>
                </tr>
                <tr>
                  <td colspan="2" style="font-size: 12px;padding-top:5px;padding-bottom:5px; border-left: 3px solid; border-right: 3px solid; border-bottom: 3px solid;">AMOUNT TO BE REIMBURSED</td>
                  <td  style="font-size: 12px;text-align:right; border-right: 3px solid; border-bottom: 3px solid;"></td>
                </tr>
              @endif

              <tr>
                <td style="font-size: 12px; border-left: 3px solid; border-right: 3px solid;"><span style="border-right:3px solid black; border-bottom:3px solid black;">A</span> Certified: Correctness of the above data <br><br><br><br></td>
                <td style="font-size: 12px; border-right: 3px solid;"><span style="border-right:3px solid black; border-bottom:3px solid black;">B</span>  Certified: Purpose of travel / cash &nbsp;&nbsp;&nbsp;<span style="border-left:3px solid black; border-bottom:3px solid black;">C</span>  advance duly accomplished <br><br><br><br></td>
                <td style="font-size: 12px; border-right: 3px solid;"><span style="border-right:3px solid black; border-bottom:3px solid black; margin-left: -10px;">C</span> Certified: Supporting documents complete and proper<br><br><br><br></td>
              </tr>
              <tr>
                <td style="font-size: 12px;text-align: center; padding-top:0px; padding-bottom:0px; border-left: 3px solid; border-right: 3px solid;" >CRISTETA M. OINEZA</td>
                <td style="font-size: 12px;text-align: center; padding-top:0px; padding-bottom:0px; border-right: 3px solid;" >{{ strtoupper($advance->receiver_name) }}</td>
                <td style="font-size: 12px;text-align: center; padding-top:0px; padding-bottom:0px; border-right: 3px solid;">CATHERINE N. ESQUIDA</td>
              </tr>
              <tr>
                <td style="font-size: 12px;text-align: center; padding-top:0px; padding-bottom:0px; border-left: 3px solid; border-right: 3px solid;">Administrative Officer V</td>
                <td style="font-size: 12px;text-align: center; padding-top:0px; padding-bottom:0px; border-right: 3px solid;">{{ $advance->receiver_position }}</td>
                <td style="font-size: 12px;text-align: center; padding-top:0px; padding-bottom:0px; border-right: 3px solid;">Accountant III</td>
              </tr>
              <tr>
                <td style="font-size: 12px;text-align: center; padding-top:0px; padding-bottom:0px; border-left: 3px solid; border-right: 3px solid;" >Cash Unit<br></td>
                <td style="font-size: 12px;text-align: center; padding-top:0px; padding-bottom:0px; border-right: 3px solid;" ><br></td>
                <td style="font-size: 12px;text-align: center; padding-top:0px; padding-bottom:0px; border-right: 3px solid;" ><br></td>
              </tr>
              <tr>
                <td style="font-size: 12px;padding-top:0px; padding-bottom:5px; border-left: 3px solid; border-right: 3px solid; border-bottom: 3px solid;" >Date: _________________________</td>
                <td style="font-size: 12px;padding-top:0px; padding-bottom:5px; border-right: 3px solid; border-bottom: 3px solid;" >Date: _________________________</td>
                <td style="font-size: 12px;padding-top:0px; padding-bottom:5px; border-right: 3px solid; border-bottom: 3px solid;" >Date: _________________________</td>
              </tr>
            </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
    <script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>
  </body>
</html>
<?php
$currentMonth = new DateTime(date('Y-m-d'));
$mpdf->SetHTMLHeader('
');

// Now collect the output buffer into a variable
$html = ob_get_contents();
ob_end_clean();
// send the captured HTML from the output buffer to the mPDF class for processing

// <link href="{{ asset('packages/packages.css') }}" rel="stylesheet">

// <script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
// <script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>


$mpdf->WriteHTML($html);
$mpdf->SetHTMLFooter('');

// send the captured HTML from the output buffer to the mPDF class for processing

// footer

$mpdf->Output();
exit;
?>
