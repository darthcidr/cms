@section('page-name')
	Summary of LDDAP-ADAs Issued and Invalidated ADA Entries | {{ config('app.name','Cannot display app name') }}
@endsection

@extends('layouts.sliiae')

@section('content')
	<div style = "text-align: right; font-style: italic;">Appendix 53</div>

	<br>

	<div style = "text-align: center; font-weight: bold">
		SUMMARY OF LDDAP-ADAs ISSUED AND INVALIDATED ADA ENTRIES
	</div>

	<br>

	<div style="font-weight: bold;">
		<div style = "float: left;">
			<div>Department: DEPARTMENT OF EDUCATION</div>
			<div>Entity Name: DEPARTMENT OF EDUCATION REGION 1</div>
			<div>Operating: REGIONAL OFFICE PROPER</div>
		</div>

		<div style = "float: right;">
			<div>Fund Cluster: {{ $fund }}</div>
			<div>SLIIAE No.: {{ $sliiae_no }}</div>
			<div>Date: {{ date('F d, Y',strtotime($date)) }}</div>
		</div>
	</div>

	<br>
	<br>
	<br>
	<br>

	<div style="font-weight: bold;">
		<div>
			<div>To: MS. ADELINE M. RAMOS</div>
			<div>LAND BANK OF THE PHILIPPINES-SFLU BRANCH</div>
			<div>CITY OF SAN FERNANDO LA UNION</div>
		</div>
	</div>

	<br>

	<div>
		<table border="1" cellspacing="0" width="100%">
			<thead style="text-align: center; font-weight: bold;">
				<tr>
					<td rowspan="3">LDDAP-ADA No.</td>
					<td rowspan="3">Date of Issue</td>
					<td colspan="5">Amount</td>
					<td colspan="2">For GSB Use Only</td>
				</tr>

				<tr>
					<td rowspan="2">Total</td>
					<td colspan="4">Allotment/Object Class</td>
					<td rowspan="2" colspan="2">Remarks</td>
				</tr>

				<tr>
					<td>PS</td>
					<td>MOOE</td>
					<td>CO</td>
					<td>FE</td>
				</tr>
			</thead>

			<tbody style="border-bottom: none;">
				<tr>
					<td>{{ $lddap_ada_number }}</td>
					<td>{{ date('d-M-y',strtotime($date)) }}</td>
					<td>{{ number_format($total,2,'.',',') }}</td>
					<td></td>
					<td>{{ number_format($total,2,'.',',') }}</td>
					<td></td>
					<td></td>
					<td colspan="2"></td>
				</tr>

				@for($i = 0 ; $i < 5 ; $i++)
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td colspan="2">&nbsp;</td>
					</tr>
				@endfor

				<tr>
					<td colspan="2">No. of pcs of LDDAP-ADA: 1</td>
					<td colspan="7">
						<div>Total Amount: <strong>{{ number_format($total,2,'.',',') }}</strong></div>
						<div>Amount in Words: {{ ucwords($total_in_words) }}</div>
					</td>
				</tr>

				<tr>
					<td colspan="9">&nbsp;</td>
				</tr>

				<tr>
					<td rowspan="3">LDDAP-ADA No.</td>
					<td rowspan="3">Amount</td>
					<td rowspan="3">Date Issued</td>
					<td colspan="6" style="font-weight: normal;">OF WHICH INVALIDATED ENTRIES OF PREVIOUSLY ISSUED LDDAP-ADAs</td>
				</tr>

				<tr>
					<td colspan="6">Allotment/Object Class</td>
				</tr>

				<tr>
					<td>PS</td>
					<td>MOOE</td>
					<td>CO</td>
					<td>FE</td>
					<td>TOTAL</td>
					<td>Remarks</td>
				</tr>

				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>

				<tr>
					<td colspan="9" style="padding: 1rem;">
						<div style="float: left;">
							<div>Certified Correct by:</div>
							<br>
							<br>
							<div><strong>CRISTETA M. OINEZA</strong></div>
							<div>Administrative Officer V</div>
							<div>Cash Unit</div>
						</div>

						<div style="float: right;">
							<div>Approved by:</div>
							<br>
							<br>
							<div><strong>ALMA RUBY C. TORIO</strong></div>
							<div>Regional Director</div>
						</div>
					</td>
				</tr>

				<tr>
					<td colspan="9" style="padding: 1rem;">
						<div>TRANSMITTAL INFORMATION</div>
						<br>
						<div style="float: left;">
							<div>Delivered by:</div>
							<div style="text-decoration: underline;">(Signature)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<div style="text-decoration: underline;">(Name in Print)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<div style="text-decoration: underline;">(Designation)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						</div>

						<div style="float: right;">
							<div>Received by:</div>
							<div style="text-decoration: underline;">(Signature)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<div style="text-decoration: underline;">(Name in Print)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<div style="text-decoration: underline;">(Designation)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
@endsection