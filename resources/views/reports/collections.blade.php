<?php
// Require composer autoload
require_once __DIR__ .'/../../../vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf(['setAutoTopMargin' => 'stretch'],['format' => 'A4']);
// $mpdf = new \Mpdf\Mpdf('utf-8', array(190,236));
// Buffer the following html with PHP so we can store it to a variable later
ob_start();
?>
{{-- This is where your script would normally output the HTML using echo or print --}}
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
        <title>Report of Collection and Deposit, {{ date('F Y') }} | {{ config('app.name', 'Cannot display page name') }}</title>
				<!-- Styles -->
				{{-- <link href="{{ asset('packages/packages.css') }}" rel="stylesheet"> --}}
        <style>
            .tbl_data{
            border-collapse: collapse;
            border:1px solid #000000;
            }
            .tbl_data td, th{
            padding: 5px;
            border:1px solid #000000;
            }
            .tbl_head{
              margin-top: 0px;
              border:none;
              /* border:1px solid #000000; */
            }

        </style>
		</head>
		<body>

	<div class = "ui fluid container" style = "font-size: 12px;">

		<div class = "row" style = "padding-bottom: 0rem;">
			<div class = "column">
				<table class="tbl_data" style = "font-family: Times New Roman; font-size: 12px;" cellspacing="0" cellpadding="0" width="100%" border = "1">
            <thead >
              <tr>
                <th style=" font-size:10px;" colspan="2" rowspan="2">Official Receipt/ Report<br> of Collection by Sub-<br>Collector</th>
                <th style=" font-size:10px;" rowspan="3">Resposibility <br>Center <br>Code</th>
    						<th style=" font-size:10px;" rowspan="3"></th>
    						<th style=" font-size:10px;" rowspan="3">Payor</th>
    						<th style=" font-size:10px;" rowspan="3">Particulars</th>
    						<th style=" font-size:10px;" rowspan="3">MFO/PAP</th>
    						<th style=" font-size:10px;" colspan="3" width="15%">Amount</th>
              </tr>
              <tr>
                <th style=" font-size:10px;" rowspan="2">Total per OR</th>
                <th style=" font-size:10px;">Taxes</th>
                <th style=" font-size:10px;">Fees</th>
              </tr>
              <tr>
                  <th style="padding:5px; font-size:10px;">Date</th>
                  <th style="padding:5px; font-size:10px;">Number</th>
                  <th style="padding:5px; font-size:9px;">40101010</th>
                  <th style="padding:5px; font-size:9px;">40201010</th>
              </tr>
            </thead>
					<tbody>

						@if(count($collections) > 0)
							@foreach($collections as $collection)
								<tr>
									<td>{{ (new DateTime($collection->date))->format('d-M-y') }}</td>
									<td>{{ $collection->or_number }}</td>
									<td></td>
                  <td></td>
									<td>{{ $collection->payor }}</td>
									<td></td>
									<td></td>
									<td><strong>{{ number_format($collection->total,2,'.',',') }}</strong></td>
									<td></td>
                  <td></td>
								</tr>
							@endforeach
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
                <td>Total</td>
								<td><strong>{{ $totalCollections }}</strong></td>
                <td></td>
                <td></td>
							</tr>
						@else
							<tr>
								<td colspan="10" align="center">No entries found</td>
							</tr>
						@endif

            <tr>
              {{-- <td colspan="2" style="border-right:0px; border-bottom:0px;"></td>
              <td colspan="3" style="font-size:10px;border-left:0px;border-right:0px; border-bottom:0px;">
                Summary:<br>
                Undeposited Collections per last Report<br>
                Collections per OR Nos. {{ $first_or }}-{{ $last_or }}<br>
                Deposits<br>
                &nbsp;&nbsp;&nbsp;Date: <br><br><br>
                Undeposited Collections, This Report
              </td>
              <td colspan="2" style="font-size:10px;border-left:0px; border-right: 0px; border-bottom:0px; text-align:right;"><br><br>
                24,899.10<br><br>
                24,899.10<br><br>
                ____________________<br>
                0.00
              </td>
              <td colspan="3" style="border-left:0px; border-bottom:0px;"></td> --}}
              <td colspan="10">
                <div>Summary:</div>
                <div>Undeposited Collections per last report</div>
                <div>Collections per OR Nos. {{ $first_or }}-{{ $last_or }}: {{ $totalCollections }}</div>
                <div>Deposits</div>
                @if(count($collections) > 0)
                  @foreach($collections as $collection)
                    <div>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      Date
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      {{ ( new DateTime($collection->date) )->format('d-M-y') }}
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      {{ $collection->total }}
                    </div>
                  @endforeach
                @else
                  No entries found
                @endif
                <br>
                <div>Undeposited Collections, this Report&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.00</div>
              </td>
            </tr>

            <tr>
              <td colspan="10" style="text-align:Center; border-bottom:0px; font-size:10px;"><br><strong>CERTIFICATION</strong><br></td>
            </tr>
            <tr>
              <td colspan="10" style="font-size: 10px;border-left: 0px; border-top:0px; text-align: justify;">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                I hereby certify on my official oath that the above is a true statement of all collections and deposits had by me during the period stated
                above for which Official Receipts Nos. {{ $first_or }} to {{ $last_or}} inclusive, were actually issued by me in the amounts shown thereon.
                I also certify that I have not received money from whatever source without having issued the necessary Official Receipt in acknowledgement thereof.
                Collections received by sub-collectors are recorded above in lump-sum opposite their respective collection report number. I certify further that
                the balance shown above agrees with the balance appearing in my Cash Receipts Record.
              </td>
            </tr>
            <tr>
              <td colspan="10" style="text-align:center; font-size:10px;">
                <br>
                <br>
                <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ trim($signatory->fname) }} {{ trim($signatory->mname) }} {{ trim($signatory->lname) }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u><br>
                Name and Signature of the Collecting Officer<br><br>
                Administrative Officer V <br>
                Cash Unit <br>
                Official Designation
              </td>
            </tr>
					</tbody>
					</table>
			</div>
		</div>
	</div>
  <br>
  <br>


<!-- Scripts -->
{{-- <script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
<script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script> --}}
</body>
</html>



<?php
$mpdf->SetHTMLHeader('
<div class = "ui fluid container" style = "font-family: Times New Roman; font-size: 10px;">


<div class = "ui stackable equal width grid">
<div class = "row" style="padding-bottom: 0rem;">
<div class = "column" style = "text-align: right;">
<em>Appendix 26</em>
</div>
</div>
</div>

<div class = "row" style="padding-bottom: 0rem;">
<div class = "column" style = "text-align: center;">
<div style=" font-weight: bold; padding-bottom: 0rem;">
	<div>SUMMARY REPORT OF COLLECTIONS AND DEPOSITS</div>
</div>
</div>
</div>
</div>
<br>

<table class="tbl_head" cellspacing="0" cellpadding="0" style="font-family: Times New Roman; font-size: 10px; font-weight: bold; padding-bottom: 0rem; width:100%">
      <tr>
            <td width="75%" style="text-align:left;">
            <div class = "column">
            <div>
            	<div>Entity Name : Department of Education Region 1</div>
            	<div>Fund Cluster : '.$fund->cluster_number.' ('.$fund->code.')</div>
            </div>
            </div>
            </td>
            <td></td>
            <td style="text-align:left;">
            <div class = "repos">
            <div style="">Report No. : '.$report_number .'</div>
            <div style="">Sheet No. : {PAGENO}</div>
          	<div>Date: {DATE M 1-t, Y}</div>
            </div>
            </td>
      </tr>
</table>
');

// Now collect the output buffer into a variable
$html = ob_get_contents();
ob_end_clean();
// send the captured HTML from the output buffer to the mPDF class for processing

// <link href="{{ asset('packages/packages.css') }}" rel="stylesheet">

// <script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
// <script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>


$mpdf->WriteHTML($html);
// $mpdf->SetHTMLFooter('
// <div class = "row" style = "padding-bottom: 3rem;">
//   <div class = "column" style = "text-align: center;">
//     <strong>C E R T I F I C A T I O N</strong>
//     <div>I hereby certify on my official oath that this Report of Collection in {PAGENO} sheet(s) is a full,true and correct</div>
//   </div>
// </div>
// <br>
//
// <table class="tbl_footer">
//   <tr>
//     <td style="width:402px;"></td>
//     <td style="width:402px; text-align:center;">
//     <div>
//       <strong>' . $name . '</strong>
//       <div>Administrative Officer V</div>
//       <div>Cash Unit</div>
//     </div>
//     </td>
//     <td style="width:402px; text-align:center;">
//       <div>
//         1-{DATE M-y}
//       </div>
//     </td>
// </table>
// ');

// send the captured HTML from the output buffer to the mPDF class for processing

// footer

$mpdf->Output();
exit;
?>
