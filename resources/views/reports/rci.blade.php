<?php
$cluster = $fund->cluster_number;
$acnum = $fund->account_number;
$report_num = substr($report_number,0,5);
$editdate = $report_num.'-01';
$date = date("F", strtotime($editdate)) . " 1-" . date("t", strtotime($editdate)) . ", " . date("Y", strtotime($editdate));


// Require composer autoload
require_once __DIR__ .'/../../../vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf(['setAutoTopMargin' => 'stretch'],['format' => 'Legal']);
// $mpdf = new \Mpdf\Mpdf('utf-8', array(190,236));
// Buffer the following html with PHP so we can store it to a variable later
ob_start();
?>
{{-- This is where your script would normally output the HTML using echo or print --}}
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
        <title>Report of Checks Issued, {{ date('F Y') }} | {{ config('app.name', 'Cannot display page name') }}</title>
        <!-- Styles -->
        {{-- <link href="{{ asset('packages/packages.css') }}" rel="stylesheet"> --}}
        <style>
            .tbl_data{
                border-collapse: collapse;
                border:1px solid #000000;
            }
            .tbl_data td, th{
                padding: 5px;
                border:1px solid #000000;
            }
            .tbl_head{
                margin-top: 0px;
                border:none;
                /* border:1px solid #000000; */
            }
        </style>
    </head>
    
    <body>
    	<div class = "ui fluid container" style = "font-size: 12px;">

    		<div class = "row" style = "padding-bottom: 0rem;">
    			<div class = "column">
    				<table class="tbl_data" style = "font-family: Times New Roman; font-size: 12px;" cellspacing="0" cellpadding="0" width="100%" border = "1">
    					<tr>
    						<th colspan="2">Check</th>
    						<th rowspan="2">DV/Payroll No.</th>
    						<th rowspan="2">ORS/BURS<br>No.</th>
    						<th rowspan="2">Resp. <br>CD</th>
    						<th rowspan="2">Payee</th>
    						<th rowspan="2">UACS Object Code</th>
    						<th rowspan="2">Nature of Payment</th>
    						<th rowspan="2">Amount</th>
    					</tr>
    						<tr>
    							<th style="padding:5px">Date</th>
    							<th style="padding:5px">Serial No.</th>
    						</tr>
    					<tbody>
    						@if(count($checks) > 0)
    							@foreach($checks as $check)
    								<tr>
    									<td>{{ date('d-M-y',strtotime($check->created_at)) }}</td>
    									<td>{{ $check->check_number }}</td>
    									<td>{{ $check->dv_num }}</td>
    									<td>{{ $check->ors_burs_num }}</td>
    									<td></td>
    									<td>
                                            @if($check->status === "ACTIVE")
                                                {{ $check->payee }}
                                            @elseif($check->status === "CANCELLED")
                                                CANCELLED
                                            @endif
                                        </td>
    									<td>{{ $check->uacs_code }}</td>
    									<td>{{ $check->particular }}</td>
    									<td>{{ number_format($check->total,2,'.',',') }}</td>
    								</tr>
    							@endforeach
    							<tr>
    								<td></td>
    								<td></td>
    								<td></td>
    								<td></td>
    								<td></td>
    								<td></td>
    								<td></td>
    								<td></td>
    								<td><strong>{{ number_format($checks_total,2,'.',',') }}</strong></td>
    							</tr>
    						@else
    							<tr>
    								<td colspan="9" align="center">No entries found</td>
    							</tr>
    						@endif
    					</tbody>
    				</table>
    			</div>
    		</div>
    	</div>
        <br>
        <br>
    </body>
</html>



<?php
$currentMonth = new DateTime(date('Y-m-d'));
$mpdf->SetHTMLHeader('
<div class = "ui fluid container" style = "font-family: Times New Roman; font-size: 12px;">


<div class = "ui stackable equal width grid">
<div class = "row" style="padding-bottom: 0rem;">
<div class = "column" style = "text-align: right;">
<em>Appendix 35</em>
</div>
</div>
</div>

<div class = "row" style="padding-bottom: 0rem;">
<div class = "column" style = "text-align: center;">
<div style=" font-weight: bold; padding-bottom: 0rem;">
	<div>REPORT OF CHECKS ISSUED</div>
	<div>Period Covered: '. $date .'</div>
</div>
</div>
</div>
</div>
<br>

<table class="tbl_head" cellspacing="0" cellpadding="0" style="font-family: Times New Roman; font-size: 12px; font-weight: bold; padding-bottom: 0rem; width:100%">
      <tr>
            <td>
            <div class = "column">
            <div>
            	<div>Entity Name : Department of Education Region 1</div>
            	<div>Fund Cluster : '.$cluster.'</div>
            	<div>Bank Name/Account No. : LBP/'.$acnum.'</div>
            </div>
            </div>
            </td>
            <td style="text-align:right;">
            <div class = "repos">
            <div style="">Report No. : '.$_POST["txtRCIReportNum"] .'</div>
            <div style="">Sheet No. : {PAGENO}</div>
            </div>
            </td>
      </tr>
</table>
');

// Now collect the output buffer into a variable
$html = ob_get_contents();
ob_end_clean();
$mpdf->AddPage('L');
// send the captured HTML from the output buffer to the mPDF class for processing

// <link href="{{ asset('packages/packages.css') }}" rel="stylesheet">

// <script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
// <script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>


$mpdf->WriteHTML($html);
$mpdf->SetHTMLFooter('
<div class = "row" style = "padding-bottom: 3rem;">
  <div class = "column" style = "text-align: center;">
    <strong>C E R T I F I C A T I O N</strong>
    <div>I hereby certify on my official oath that this Report of Checks Issued in {PAGENO} sheet(s) is a full,true and correct</div>
    <div>statement of all checks issued by me during the period above for which Check Nos. '. $first_check .' to Check Nos. '. $last_check .' inclusive,</div>
    <div> were actually issued by me in payment for obligations shown in the attached disbursement vouchers/payroll.</div>
  </div>
</div>
<br>

<table class="tbl_footer">
  <tr>
    <td style="width:402px;"></td>
    <td style="width:402px; text-align:center;">
    <div>
      <strong>' . $name . '</strong>
      <div>Administrative Officer V</div>
      <div>Cash Unit</div>
    </div>
    </td>
    <td style="width:402px; text-align:center;">
      <div>
        1-{DATE M-y}
      </div>
    </td>
</table>
');

// send the captured HTML from the output buffer to the mPDF class for processing

// footer

$mpdf->Output();
exit;
?>
