<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Official Receipt #{{ $collection->or_number }}</title>

        <!-- Styles -->
        <link href="{{ asset('packages/packages.css') }}" rel="stylesheet">
        <style>
            @media print and (width: 10cm) and (height: 20.5cm) {
                @page {
                    margin: 3cm;
                }
            }
        </style>
    </head>

    <body style = "font-family: monospace; font-size: 10px;">
    	<div style="position: absolute;">
            <div style="padding-top: 12.75rem; padding-left: 3.25rem;">{{ $collection->payor }}</div>
            <div style="padding-top: 1rem; padding-bottom: 3.25rem; padding-left: 3.25rem;">{{ $collection->agency }}</div>
        </div>

        <div style="position: absolute; padding-top: 20rem;">
            <table cellspacing="0" cellpadding="4" width="100%">
                @if(count($particulars) > 0)
                    @foreach($particulars as $particular)
                        <tr>
                            <td align="left" style="word-wrap: break-word; width: 19rem;">- {{ $particular->desc }}</td>
                            <td align="right">{{ number_format($particular->amount,2,'.',',') }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>No particulars found</td>
                    </tr>
                @endif
            </table>

            {{-- @if(count($particulars) > 0)
                @foreach($particulars as $particular)
                    <div>
                        <div style="padding-left: 3.25rem; display: inline; word-wrap: break-word; width: 1px;">{{ $particular->desc }}</div>
                        <div style="padding-left: 15rem; display: inline;">{{ number_format($particular->amount,2,'.',',') }}</div>
                    </div>
                @endforeach
            @else
                <div>
                    <div style="padding-left: 3.25rem; display: inline;">No particulars found</div>
                </div>
            @endif --}}
        </div>

        <div style="position: absolute; padding-top: 33.5rem; padding-left: 19.5rem;">{{ $collection->total }}</div>

        <div style="position: absolute; padding-top: 35.5rem; padding-left: 8.5rem;">{{ $total_in_words }}</div>

        <!-- Scripts -->
        <script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
        <script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>
        <script type = "text/javascript">
            window.print();
        </script>
    </body>
</html>
