@section('page-name')
	Agency Advice of Commercial Checks, Check #{{ $check->check_number }} | {{ config('app.name','Cannot display app name') }}
@endsection

@extends('layouts.advice')

@section('content')
	<div style = "text-align: center; padding-top: 2rem; padding-bottom: 0rem;">
		<div>Republic of the Philippines</div>
		<div><strong>DEPARTMENT OF EDUCATION</strong></div>
		<div>Regional Office No. 1</div>
		<div>San Fernando City, La Union</div>
		<div>Tel. No. 242-78-69 / Fax No.</div>
	</div>

	<div style = "text-align: center; padding-left: 10rem; padding-top: 1rem; padding-bottom: 0rem;">
		<div style = "font-weight: bold; text-decoration: underline;">DEPED ROI, SFC</div>
		<div>(Issuing Agency & Address)</div>
	</div>

	<div style = "text-align: center; padding-top: 0rem; padding-bottom: 0rem;">
		<div><strong>AGENCY ADVICE OF COMMERCIAL CHECKS</strong></div>
		<div><strong>RELEASED AND CHECKS CANCELLED</strong></div>
		<div style = "font-weight: bold; text-decoration: underline;">{{ date('d-M-Y') }}</div>
	</div>

	<div style = "padding-left: 30rem; padding-top: 1rem; padding-bottom: 0rem;">
		<div style = "font-weight: bold; text-decoration: underline;">RELC FUNDS</div>
		<div><strong>Bank Acct. No.{{ $check->account_number }}</strong></div>
	</div>

	<div style = "text-align: center; padding-top: 1rem; padding-bottom: 0rem;">
		<div><strong>CHECKS RELEASED</strong></div>
		<div>
			<table cellspacing="0" cellpadding="0" width="100%" border="1">
				<thead>
					<tr>
						<th>CHECK NO.</th>
						<th>DATE</th>
						<th>P A Y E E</th>
						<th>AMOUNT</th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>{{ $check->check_number }}</td>
						<td>{{ date('m/d/Y',strtotime($check->created_at)) }}</td>
						<td align="left">{{ $check->payee }}</td>
						<td align="right">{{ $check->total }}</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td align="center">***</td>
						<td>&nbsp;</td>
					</tr>

					@for($i = 0 ; $i < 8 ; $i++)
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					@endfor
				</tbody>

				<tfoot>
					<tr>
						<td>No. of checks:</td>
						<td>One (1)</td>
						<td align="right"><strong>TOTAL</strong></td>
						<td align="right"><strong>{{ number_format($check->total,2,'.',',') }}</strong></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

	<div style="padding-top: 1rem; padding-left: 5rem; padding-bottom: 0rem;">
		<div>AMOUNT IN WORDS: {{ ucwords($total_in_words) }}</div>
	</div>

	<div style="padding-top: 1rem;">
		<div style="display: inline;">
			Delivered by:
		</div>

		<div style="display: inline; text-align: center;">
			<div><strong>{{ strtoupper(trim($signatoryCashUnit->fname)) }} {{ strtoupper(trim($signatoryCashUnit->mname)) }} {{ strtoupper(trim($signatoryCashUnit->lname)) }}</strong></div>
			<div>Administrative Officer V</div>
			<div>Cash Unit</div>
		</div>
	</div>

	<div style="padding-top: 1rem;">
		<div style="display: inline;">
			Received by:
		</div>

		<div style="display: inline; text-align: center;">
			<div><strong>{{ strtoupper(trim($signatoryRegionalDir->fname)) }} {{ strtoupper(trim($signatoryRegionalDir->mname)) }} {{ strtoupper(trim($signatoryRegionalDir->lname)) }}</strong></div>
			<div>Regional Director</div>
		</div>
	</div>
@endsection
