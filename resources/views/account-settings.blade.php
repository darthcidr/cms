@section('page-name')
    Account Settings | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "ten wide column">
            <div class = "ui small text segment">
                <h5 class = "ui header">Account settings</h5>

                <form class = "ui small equal width form" method = "POST" action = "{{ route('account.update',Auth::guard('web')->user()->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class = "fields">
                        <div class = "field">
                            <label>First name:</label>
                            <input id="fname" type="text" name="fname" value="{{ Auth::guard('web')->user()->fname }}" required autofocus>
                        </div>

                        <div class = "field">
                            <label>Middle name:</label>
                            <input id="mname" type="text" name="mname" value="{{ Auth::guard('web')->user()->mname }}" required>
                        </div>

                        <div class = "field">
                            <label>Last name:</label>
                            <input id="lname" type="text" name="lname" value="{{ Auth::guard('web')->user()->lname }}" required>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Profile picture:</label>
                        <input type = "file" accept = "image/jpeg,image/jpg,image/png" id = "picture" name = "picture" value = "/images/profile_picture/{{ (Auth::guard('web')->user()->image) }}">
                    </div>

                    <div class = "field">
                        <label>E-mail address:</label>
                        <input id="email" type="email" name="email" value="{{ Auth::guard('web')->user()->email }}" required>
                    </div>

                    <div class = "field">
                        <label>Username:</label>
                        <input id="username" type="text" name="username" value="{{ Auth::guard('web')->user()->username }}" required>
                    </div>

                    <button type = "submit" class = "ui small positive button">
                        <i class = "send icon"></i>
                        Update
                    </button>
                    <button type = "reset" class = "ui small negative button">
                        <i class = "refresh icon"></i>
                        Reset fields
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.success('{{ session('status') }}');
        </script>
    @endif
@endsection