@section('page-name')
    System Settings - Signatories | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "eight wide column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Add signatory</h4>

                <form id = "frmAddSignatory" class = "ui small equal width form" method = "POST" action = "{{ route('signatories.store') }}">
                    {{ csrf_field() }}

                    <div class = "fields">
                        <div class = "field">
                            <label>First name</label>
                            <input type = "text" id = "txtAddSignatoryFName" name = "txtAddSignatoryFName" required>
                        </div>

                        <div class = "field">
                            <label>Middle name</label>
                            <input type = "text" id = "txtAddSignatoryMName" name = "txtAddSignatoryMName">
                        </div>

                        <div class = "field">
                            <label>Last name</label>
                            <input type = "text" id = "txtAddSignatoryLName" name = "txtAddSignatoryLName" required>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Position</label>
                        <input type = "text" id = "txtAddSignatoryPosition" name = "txtAddSignatoryPosition" required>
                    </div>

                    <button type = "submit" id = "btnAddSignatory" class = "ui small positive button">
                        <i class = "send icon"></i>
                        Submit
                    </button>
                </form>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small text segment">
                <h4 class = "ui header">List of signatories</h4>

                <table id = "tblListOfSignatories" class = "ui small celled striped table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgSettingsSignatories').toggleClass('active',true);
    </script>
@endsection
