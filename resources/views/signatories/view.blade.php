@section('page-name')
    System Settings - Signatories | {{ config('app.name', 'Laravel') }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "eight wide column">
            <div class = "ui small text segment">
                <h4 class = "ui header">Edit signatory</h4>

                <form id = "frmEditSignatory" class = "ui small equal width form" method = "POST" action = "{{ route('signatories.update',$signatory->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class = "fields">
                        <div class = "field">
                            <label>First name</label>
                            <input type = "text" id = "txtEditSignatoryFName" name = "txtEditSignatoryFName" value = "{{ $signatory->fname }}" required autofocus>
                        </div>

                        <div class = "field">
                            <label>Middle name</label>
                            <input type = "text" id = "txtEditSignatoryMName" name = "txtEditSignatoryMName" value = "{{ $signatory->mname }}">
                        </div>

                        <div class = "field">
                            <label>Last name</label>
                            <input type = "text" id = "txtEditSignatoryLName" name = "txtEditSignatoryLName" value = "{{ $signatory->lname }}" required>
                        </div>
                    </div>

                    <div class = "field">
                        <label>Position</label>
                        <input type = "text" id = "txtEditSignatoryPosition" name = "txtEditSignatoryPosition" value = "{{ $signatory->position }}" required>
                    </div>

                    <div class = "ui three small buttons">
                        <button type = "button" class = "ui small button">
                            <i class = "left arrow icon"></i>
                            Back
                        </button>

                        <button type = "submit" id = "btnEditSignatory" class = "ui small positive button">
                            <i class = "send icon"></i>
                            Submit
                        </button>

                        <button type = "button" id = "btnDeleteSignatory" class = "ui small negative button">
                            <i class = "trash icon"></i>
                            Delete
                        </button>
                    </div>
                </form>

                <form id = "frmDeleteSignatory" method = "POST" action = "{{ route('signatories.destroy',$signatory->id) }}" hidden>
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>
            </div>
        </div>
@endsection

@section('scripts')
    @if(session()->has('status'))
        <script type = "text/javascript">
            toastr.{{ session('type') }}('{{ session('status') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgSettingsSignatories').toggleClass('active',true);

        $(document).on('click', '#btnDeleteSignatory', function(event){
            event.preventDefault();
            if (confirm('Are you sure to delete signatory: {{ $signatory->fname }} {{ trim($signatory->mname) }} {{ $signatory->lname }}?')) {
                $('#frmDeleteSignatory').submit();
            }
        });
    </script>
@endsection
