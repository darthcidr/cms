<nav class = "ui large top fixed inverted borderless stackable menu" style = "background-color: #04508c; z-index: 1000;">
	@if(Auth::guard('web')->check())
		<a class = "header item" href = "{{ url('/home') }}">
			<img src = "{{ asset('favicon.ico') }}">
			&nbsp;
			{{ config('app.name','Cannot display app name') }}
			{{-- <img src = "{{ asset('images/deped-logo-with-text.png') }}" style = "width: 100%; height: auto;"> --}}
		</a>
	@elseif(Auth::guard('admin')->check())
		<a class = "header item" href = "{{ url('/admin/home') }}">
			<img src = "{{ asset('favicon.ico') }}">
			&nbsp;
			{{ config('app.name','Cannot display app name') }}
			{{-- <img src = "{{ asset('images/deped-logo-with-text.png') }}" style = "width: 100%; height: auto;"> --}}
		</a>
	@else
		<a class = "header item" href = "{{ url('/') }}">
			<img src = "{{ asset('favicon.ico') }}">
			&nbsp;
			{{ config('app.name','Cannot display app name') }}
			{{-- <img src = "{{ asset('images/deped-logo-with-text.png') }}" style = "width: 100%; height: auto;"> --}}
		</a>
	@endif

	@if(Auth::guard('web')->check())
		<div class = "right menu">
			<div class = "ui dropdown item">
				Welcome, {{ Auth::guard('web')->user()->fname }}
				<i class = "dropdown icon"></i>
				<div class = "menu">
					<a href = "/account/settings" class = "item">
						{{-- <i class = "setting icon"></i>
						Account Settings --}}
						<div class = "ui basic small center aligned segment">
							@if(Auth::guard('web')->user()->image !== null)
								<div><img src = "{{ asset('images/profile_picture/'.Auth::guard('web')->user()->image) }}" style = "height: 100px; width: 100px; border-radius: 50px"></div>
							@else
								<div><img src = "{{ asset('images/profile_picture/default.jpg') }}" style = "height: 100px; width: 100px; border-radius: 50px"></div>
							@endif
							<h5 class = "ui header">
								{{ ucwords(Auth::guard('web')->user()->fname) }} {{ ucwords(Auth::guard('web')->user()->lname) }}
								<div class = "sub header">Cash Unit</div>
							</h5>
						</div>
					</a>

					<a href = "{{ route('logout') }}" id = "btnLogout" class = "item">
						<i class = "power icon"></i>
						Logout
					</a>
				</div>
			</div>
		</div>
	@elseif(Auth::guard('admin')->check())
		<div class = "right menu">
			<div class = "ui dropdown item">
				Welcome, {{ Auth::guard('admin')->user()->fname }}
				<i class = "dropdown icon"></i>
				<div class = "menu">
					{{-- <a href = "/admin/account/settings" class = "item">
						<i class = "setting icon"></i>
						Account Settings
					</a> --}}
					<div class = "ui basic small center aligned segment">
						@if(Auth::guard('admin')->user()->image !== null)
							<div><img src = "{{ asset('images/profile_picture/'.Auth::guard('admin')->user()->image) }}" style = "height: 100px; width: 100px; border-radius: 50px"></div>
						@else
							<div><img src = "{{ asset('images/profile_picture/default.jpg') }}" style = "height: 100px; width: 100px; border-radius: 50px"></div>
						@endif
						<h5 class = "ui header">
							{{ ucwords(Auth::guard('admin')->user()->fname) }} {{ ucwords(Auth::guard('admin')->user()->lname) }}
							<div class = "sub header">Cash Unit</div>
						</h5>
					</div>					

					<a href = "{{ route('admin.logout') }}" id = "btnLogout" class = "item">
						<i class = "power icon"></i>
						Logout
					</a>
				</div>
			</div>
		</div>

	@endif
</nav>

@section('scripts')
	
@endsection