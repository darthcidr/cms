<nav class = "ui visible inverted vertical sidebar menu" style = "padding-top: 5rem; font-size: 12px;">
	<div class = "header item">
		<h3 class = "ui inverted center aligned header">
			<div id = "server-time"></div>
			<div class = "sub header">
				{{ date('F d, Y') }}
			</div>
		</h3>
	</div>

	@if(Auth::guard('web')->check())
		<a id = "pgHome" class = "item" href = "/home">
			<i class = "home icon"></i>
			Home
		</a>
	@elseif(Auth::guard('admin')->check())
		<a id = "pgHome" class = "item" href = "/admin/home">
			<i class = "home icon"></i>
			Home
		</a>
	@endif

	@if(Auth::guard('admin')->check())
		<a id = "pgUsers" class = "item" href = "{{ route('admin.cash-unit-users') }}">
			<i class = "user icon"></i>
			Users
		</a>
	@endif

	<a id = "pgCollections" class = "item" href = "{{ route('collections.index') }}">
		<i class = "money icon"></i>
		Collections
	</a>

	<div id = "pgAdvances" class = "item">
		<i class = "money icon"></i>
		<div class = "ui accordion">
			<a class = "title" style = "color: #fff;">
				<i class = "dropdown icon"></i>
				Cash advances
			</a>

			<div class = "content">
				<ul style = "list-style: none;">
					<a id = "pgAllAdvances" href = "/advances/all" class = "item">List all</a>
				</ul>
			</div>
		</div>
	</div>

	<div id = "pgVouchers" class = "item">
		<i class = "money icon"></i>
		<div class = "ui accordion">
			<a class = "title" style = "color: #fff;">
				<i class = "dropdown icon"></i>
				Vouchers
			</a>

			<div class = "content">
				<ul style = "list-style: none;">
					<a id = "pgAddVoucher" href = "/vouchers" class = "item">Add</a>
					<a id = "pgAllVouchers" href = "/vouchers/all" class = "item">List all</a>
				</ul>
			</div>
		</div>
	</div>

	<div id = "pgChecks" class = "item">
		<i class = "money icon"></i>
		<div class = "notifStaleChecks ui hidden mini red label"></div>
		<div class = "ui accordion">
			<a class = "title" style = "color: #fff;">
				<i class = "dropdown icon"></i>
				Checks
			</a>

			<div class = "content">
				<ul style = "list-style: none;">
					<a id = "pgAllCheck" href = "/checks/all" class = "item">All</a>
					<a id = "pgStaleCheck" href = "/checks/stale" class = "item">
						Stale
						<div class = "notifStaleChecks ui hidden mini red label"></div>
					</a>
				</ul>
			</div>
		</div>
	</div>

	<div id = "pgLDDAPs" class = "item">
		<i class = "money icon"></i>
		<div class = "ui accordion">
			<a class = "title" style = "color: #fff;">
				<i class = "dropdown icon"></i>
				LDDAP
			</a>

			<div class = "content">
				<ul style = "list-style: none;">
					<a id = "pgAllLDDAP" href = "/lddap/all" class = "item">List all</a>
				</ul>
			</div>
		</div>
	</div>

	<div class = "item">
		<i class = "newspaper icon"></i>
		<div class = "ui accordion">
			<a class = "title" style = "color: #fff;">
				<i class = "dropdown icon"></i>
				Reports
			</a>

			<div class = "content">
				<ul style = "list-style: none;">
					<a id = "pgRCI" class = "item">Report of Checks Issued (RCI)</a>
					<a id = "pgLDDAP" class = "item">Report of LDDAP</a>
					<a id = "pgSLIIAE" class = "item">Summary of LDDAP-ADAs Issued and Invalidated ADA Entries (SLIIAE)</a>
					<a id = "pgReportCollection" class = "item">Summary Report of Collections and Deposits</a>
					<a id = "pgLiquidation" class = "item">Liquidation Report</a>
				</ul>
			</div>
		</div>
	</div>

	<div id = "pgSettings" class = "item">
		<i class = "settings icon"></i>
		<div class = "ui accordion">
			<a class = "title" style = "color: #fff;">
				<i class = "dropdown icon"></i>
				System settings
			</a>

			<div class = "content">
				<ul style = "list-style: none;">
					<a href = "/settings/funds" id = "pgSettingsFunds" class = "item">Funds</a>
					<a href = "/settings/signatories" id = "pgSettingsSignatories" class = "item">Signatories</a>
					<a href = "/settings/activities" id = "pgSettingsActivities" class = "item">Activity Log</a>
					@if(Auth::guard('admin')->check()) <a href = "/settings/database" id = "pgSettingsDatabase" class = "item">Database</a> @endif
				</ul>
			</div>
		</div>
	</div>
</nav>

{{-- this is the modal of rci --}}
<div class="remodal" data-remodal-id="modalRCI">
	<button data-remodal-action="close" class="remodal-close"></button>

	<h4 class = "ui header">
		Generate Report of Checks Issued (RCI)
	</h4>

	<form id = "frmRCIFundCluster" class = "ui small equal width form" method = "POST" action = "/reports/rci" target="_new">
		{{ csrf_field() }}

		<div class = "field">
			<label>Report number</label>
			<input type = "text" id = "txtRCIReportNum" name = "txtRCIReportNum" value = "{{ date('y-m') }}-001" data-mask = "99-99-999" required>
		</div>

		<div class = "field">
			<label>Fund cluster</label>
			<select id = "cmbRCIFundCluster" name = "cmbRCIFundCluster" class = "ui small fluid search dropdown" required>

			</select>
		</div>

		<button type = "submit" class = "ui small positive button"><i class = "send icon"></i>Launch report</button>
	</form>
</div>

{{-- this is the modal of lddap --}}
<div class="remodal" data-remodal-id="modalLDDAP">
	<button data-remodal-action="close" class="remodal-close"></button>

	<h4 class = "ui header">
		Generate Report of List of Due and Demandable Accounts Payable - Advice to Debit Accounts (LDDAP-ADA)
	</h4>

	<form id = "frmLDDAPFundCluster" class = "ui small equal width form" method = "POST" action = "/reports/lddap" target="_new">
		{{ csrf_field() }}

		<div class = "field">
			<label>Report number</label>
			{{-- <input type = "text" id = "txtRCIReportNum" name = "txtRCIReportNum" value = "{{ date('y-m') }}" data-mask = "99-99-999" required> --}}
			<select id = "txtLDDAPReportNum" name = "txtLDDAPReportNum" class = "ui small fluid search dropdown" required>

			</select>
		</div>

		<div class = "field">
			<label>Fund cluster</label>
			<select id = "cmbLDDAPFundCluster" name = "cmbLDDAPFundCluster" class = "ui small fluid search dropdown" required>

			</select>
		</div>

		<button type = "submit" class = "ui small positive button"><i class = "send icon"></i>Launch report</button>
	</form>
</div>

{{-- this is the modal of sliiae --}}
<div class="remodal" data-remodal-id="modalSLIIAE">
	<button data-remodal-action="close" class="remodal-close"></button>

	<h4 class = "ui header">
		Generate Summary of LDDAP-ADAs Issued and Invalidated ADA Entries (SLIIAE)
	</h4>

	<form id = "frmGenerateSLIIAAE" class = "ui small equal width form" method = "POST" action = "/reports/sliiae" target="_new">
		{{ csrf_field() }}

		<div class = "field">
			<label>Fund cluster</label>
			<select id = "cmbGenerateSLIIAEFundCluster" name = "cmbGenerateSLIIAEFundCluster" class = "ui small fluid search dropdown" required>

			</select>
		</div>

		<div class = "field">
			<label>SLIIAE No.</label>
			<input type = "text" id = "txtGenerateSLIIAESLIIAENo" name = "txtGenerateSLIIAESLIIAENo" data-mask = "999" required>
		</div>

		<div class = "field">
			<label>Date</label>
			<input type = "date" id = "dtpGenerateSLIIAEDate" name = "dtpGenerateSLIIAEDate" required>
		</div>

		<div class = "field">
			<label>LDDAP-ADA No</label>
			<input type = "text" id = "txtGenerateSLIIAELDDAPADANo" name = "txtGenerateSLIIAELDDAPADANo" data-mask = "9 99999999-99-999-9999" required>
		</div>

		<div class = "field">
			<label>Total</label>
			<input type = "number" id = "nudGenerateSLIIAETotal" name = "nudGenerateSLIIAETotal" min = "0.00" step = "0.01" required>
		</div>

		<button type = "submit" class = "ui small positive button">
			<i class = "send icon"></i>
			Launch report
		</button>
		<button type = "reset" class = "ui small negative button">
			<i class = "refresh icon"></i>
			Reset fields
		</button>
	</form>
</div>

{{-- this is the modal of collections --}}
<div class="remodal" data-remodal-id="modalReportCollection">
	<button data-remodal-action="close" class="remodal-close"></button>

	<h4 class = "ui header">
		Generate Report of Collections
	</h4>

	<form id = "frmReportCollection" class = "ui small equal width form" method = "POST" action = "/reports/collections/all" target="_new">
		{{ csrf_field() }}

		<div class = "field">
			<label>Report number</label>
			<input type = "text" id = "txtReportCollectionReportNum" name = "txtReportCollectionReportNum" value = "{{ date('Y-m') }}-001" data-mask = "9999-99-999" required>
		</div>

		<div class = "field">
			<label>Fund cluster</label>
			<select id = "cmbReportCollectionFundCluster" name = "cmbReportCollectionFundCluster" class = "ui small fluid search dropdown" required>

			</select>
		</div>

		<button type = "submit" class = "ui small positive button"><i class = "send icon"></i>Launch report</button>
	</form>
</div>

{{-- this is the modal of cash advance --}}
<div class="remodal" data-remodal-id="modalLiquidation">
	<button data-remodal-action="close" class="remodal-close"></button>

	<h4 class = "ui header">
		Generate Liquidation Report
	</h4>

	<div class = "ui stackable equal width grid">
		<div class = "row">
			<div class = "column">
				<h5 class = "ui header">
					Existing Liquidation
				</h5>
				<form id = "frmExistingLiquidation" class = "ui small equal width form" action = "{{ route('advances.liquidation.existing') }}" method = "post" target="_new">
					{{ csrf_field() }}

					<div class = "field">
						<label>Disbursement voucher #</label>
						<select id = "cmbExistingLiquidationDVNum" name = "cmbExistingLiquidationDVNum" class = "ui small fluid search dropdown" required>

						</select>
					</div>

					<div class = "field">
						<button type = "submit" class = "ui small positive button">
							<i class = "send icon"></i>
							Generate report
						</button>
					</div>
				</form>
			</div>

			<div class = "column">
				<h5 class = "ui header">
					New Liquidation
				</h5>
				<form id = "frmLiquidation" class = "ui small equal width form" method = "POST" action = "/reports/liquidation" target="_new">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
					<input type = "text" id = "token" value = "{{ csrf_token() }}" hidden>

					<div class = "field">
						<label>Cash advance under Disbursement Voucher #</label>
						<select id = "cmbLiquidationDVNum" name = "cmbLiquidationDVNum" class = "ui small fluid search dropdown" required>

						</select>
					</div>

					<div class = "fields">
						<div class = "field">
							<label>Cash advanced</label>
							<input type = "number" id = "nudToAdvanceAdvancedCash" name = "nudToAdvanceAdvancedCash" min = "0" step = "0.01" readonly required>
						</div>

						<div class = "field">
							<label>Total amount spent</label>
							<input type = "number" id = "nudToAdvanceTotalAmountSpent" name = "nudToAdvanceTotalAmountSpent" min = "0" step = "0.01" readonly required>
						</div>

						<div class = "field">
							<label>Amount to be reimbursed</label>
							<input type = "number" id = "nudToAdvanceToReimburse" name = "nudToAdvanceToReimburse" min = "0" step = "0.01" readonly required>
						</div>
					</div>

					<div class = "field">
						<label>Breakdown of items</label>
						<table id = "tblListOfToAdvance" class = "ui small table" cellspacing="0" width="100%">
			                <thead>
			                    <tr>
			                        <th>#</th>
			                        <th>Item</th>
			                        <th>Amount</th>
			                        <th>Action</th>
			                    </tr>
			                </thead>

			                <tbody>

			                </tbody>

			                <tfoot>
			                    <td></td>
			                    <td>
			                        <input type = "text" id = "txtToAdvanceDescription" placeholder="Description...">
			                    </td>
			                    <td>
			                        <input type = "number" id = "txtToAdvanceAmount" min = "0" step="0.01" value = "0" placeholder="Amount...">
			                    </td>
			                    <td>
			                        <button type = "button" id = "btnToAdvanceAdd" class = "ui mini icon button">
			                            <i class = "plus icon"></i>
			                        </button>
			                    </td>
			                </tfoot>
			            </table>
					</div>

					<div class = "fields">
						<div class = "field">
							<label>Receiver's name</label>
							<input id = "txtToAdvanceReceiverName" name = "txtToAdvanceReceiverName" required>
						</div>

						<div class = "field">
							<label>Receiver's position</label>
							<input id = "txtToAdvanceReceiverPosition" name = "txtToAdvanceReceiverPosition" required>
						</div>
					</div>

					<div class = "field">
						<label>Remarks</label>
						<textarea id = "txtToAdvanceRemarks" name = "txtToAdvanceRemarks" rows = "2" required></textarea>
					</div>

					<button type = "submit" id = "btnGenerateLiquidationReport" name = "btnGenerateLiquidationReport" class = "ui small positive button">
						<i class = "send icon"></i>
						Generate report
					</button>
				</form>
			</div>
		</div>
	</div>
</div>

@section('scripts')

@endsection
