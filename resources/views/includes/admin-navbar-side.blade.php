<nav class = "ui visible inverted vertical borderless sidebar menu" style = "padding-top: 5rem; font-size: 12px;">
	<div class = "header item">
		<h3 class = "ui inverted center aligned header">
			<div id = "server-time"></div>
			<div class = "sub header">
				{{ date('F d, Y') }}
			</div>
		</h3>
	</div>

	{{-- <div class = "header item">
		<div class = "ui basic small center aligned segment">
			@if(Auth::guard('admin')->user()->image !== null)
				<div><img src = "{{ asset('images/profile_picture/'.Auth::guard('admin')->user()->image) }}" style = "height: 100px; width: 100px; border-radius: 50px"></div>
			@else
				<div><img src = "{{ asset('images/profile_picture/default.jpg') }}" style = "height: 100px; width: 100px; border-radius: 50px"></div>
			@endif
			<h5 class = "ui inverted header">
				{{ ucwords(Auth::guard('admin')->user()->fname) }} {{ ucwords(Auth::guard('admin')->user()->lname) }}
				<div class = "sub header">Administrator</div>
			</h5>
		</div>
	</div> --}}

	<a id = "pgHome" class = "item" href = "{{ route('admin.home') }}">
		<i class = "home icon"></i>
		Home
	</a>
	<a id = "pgUsers" class = "item" href = "{{ route('admin.cash-unit-users') }}">
		<i class = "user icon"></i>
		Users
	</a>
	<div class = "item">
		<i class = "settings icon"></i>
		<div class = "ui accordion">
			<a class = "title" style = "color: #fff;">
				<i class = "dropdown icon"></i>
				System settings
			</a>

			<div class = "content">
				<ul style = "list-style: none;">
					<a href = "/settings/database" id = "pgSettingsDatabase" class = "item">Database</a>
				</ul>
			</div>
		</div>
	</div>
</nav>

@section('scripts')

@endsection
